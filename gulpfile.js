'use strict';

const gulp = require('gulp'),
  path = require("path"),
  sass = require('gulp-sass'),
  cleanCss = require('gulp-clean-css'),
  base64 = require('gulp-base64-v2'),
  rename = require('gulp-rename'),
  fs = require("fs"),
  header = require('gulp-header'),
  footer = require('gulp-footer'),
  replace = require('gulp-replace'),
  zip = require('gulp-zip'),
  file = require('gulp-file'),
  del = require('del'),
  filter = require('gulp-filter'),
  markdown = require('gulp-markdown'),
  merge = require('merge2'),
  moment = require('moment'),
  log = require('fancy-log'),
  colors = require('ansi-colors'),
  tapCsv = require('gulp-etl-tap-csv').tapCsv,
  sitemap = require('gulp-sitemap'),
  debug = require('gulp-debug'),
  {argv} = require('yargs'),
  fileInclude = require('gulp-file-include'),
  imageResize = require('gulp-image-resize'),
  insert = require('gulp-insert'),
  webserver = require('gulp-webserver'),
  request = require('request'),
  jeditor = require('gulp-json-editor'),
  source = require('vinyl-source-stream'),
  streamify = require('gulp-streamify'),
  chmod = require('gulp-chmod');

// Global variables
const environment = {
  production: argv.release || argv.debug !== false,
  skipGallery: !argv.skipGallery,
  sourceEditionLink: 'https://framagit.org/liberte/mayenne-liberte/edit/master/app/src/',
  defaultThumbnail: '/img/danseurs-250px.png',
  locale: 'fr',
  date: {
    formatPattern: 'DD/MM/YYYY'
  },
  gallery: {
    reverseGalleries: [] // ['2022-02-08']
  },
  videos: {
    previewCount: 4,
    ext: '.mp4',
    suffix: '.min.mp4',
    thumbnailSuffix: '.min.jpg',
    i18n: {
      video: 'vidéo',
      sante: 'santé',
      libertes: 'liberté',
      economie: 'économie'
    }
  },
  articles: {
    cardBodyMaxLength: 150,
    previewCount: 6,
    i18n: {
      events: 'reportage',
      edito: 'édito',
      agenda: 'agenda',
      other: 'divers',
      videos: 'vidéo',
      forum: 'forum'
    }
  },
  forum: {
    enable: true,
    avatarCount: 5,
    avatarSize: 32,
    url: 'https://forum.mayennelibre.fr',
    topicsUrl: 'https://forum.mayennelibre.fr/latest.json',
    topicsFilter: (topic) => topic.pinned,
    defaultThumbnail: '/img/danseurs-250px.png',
    i18n: {
      category: 'catégorie'
    }
  }
};

const cache = {
  htmlFiles: {}
}

const debugBaseOptions = {
  title: 'Processing',
  minimal: true,
  showFiles: argv.debug || !environment.production,
  showCount: argv.debug || !environment.production,
  logger: m => log(colors.grey(m))
};

const paths = {
  md: ['./app/src/**/*.md'],
  img: ['./app/src/img/**/*.*'],
  doc: ['./app/src/doc/**/*.*'],
  js: ['./app/src/**/*.js'],
  gallery_md: ['./app/src/gallery/**/*.md'],

  layout_txt: ['./app/layout/**/*.txt'],
  layout_html: ['./app/layout/**/*.html'],

  sass: ['./app/**/*.scss'],

  // Gallery
  gallery_img: ['./app/src/gallery/*/*.jpg','./app/src/gallery/*/*.jpeg', './app/src/gallery/*/*.png', './app/src/gallery/*/*.gif'],
  gallery_video: ['./app/src/gallery/*/*' + environment.videos.suffix, './app/src/gallery/*/thumbnail/*' + environment.videos.thumbnailSuffix],
  // Gallery -- DEBUG
  //gallery_img: ['./app/src/gallery/2022-02-08/*.jpg','./app/src/gallery/2022-02-08/*.jpeg', './app/src/gallery/2022-02-08/*.png', './app/src/gallery/2022-02-08/*.gif'],
  //gallery_video: ['./app/src/gallery/2022-02-08*/*' + environment.videos.suffix, './app/src/gallery/2022-02-08*/thumbnail/*' + environment.videos.thumbnailSuffix],

  // Videos
  videos: ['./app/src/videos/**/*.md'],

  // Other data
  data_csv: ['./app/data/*.csv'],

  tmp_html: ['./dist/html/**/*.html'],
  output_html: ['./public/**/*.html'],

  output_html_with_sources: ['./public/**/*.html', '!./public/gallery/**/*.*', '!./public/videos/**/*.*', '!./public/articles/*.html']
};


const REGEXP = {
  title: /<h1[^>]*>([^<]*)</,
  subtitle: /<h2[^>]*>([^<]*)</,
  description: /<p[^>]*>((?:<a|<img|[^<])+)</,
  url: /<a [^>]*href="([^"]+)"/,
  img: /<img(?:[ \t]+class="[^"]*")?[ \t]+src="([^"]+)"/i,
  gallery_filename: /^([0-9]{4})-([0-9]{2})-([0-9]{2})(?:-([a-z _-éêèêàâîôç&!ùüû]+))?(?:-[a-z0-9._]*)\.(?:mp4|jpeg|jpg|png)$/i,
  lbry: /^http[s]:\/\/(www\.|open\.)?(odysee.com|lbry.com)\/.+$/,
  rumble: /^http[s]:\/\/(www\.)?(rumble.com)\/.+$/,
  youtube: /^http[s]:\/\/(www\.)?(youtu.be|youtube.com)\/.+$/,
  includeFile: /@@include\(['"]([^'"]+)['"]\)/,

  // Content of a <a> tag
  linkContent: /<a[^>]*>([^<]+)<\/a>/,
  // Content of a <a> tag
  imgTag: /<img[^>]*>(?:<\/img>)?/,
  // File prefix (e.g. in articles)
  isoDate: /\/(20[0-9]{2}-[0-9]{2}-[0-9]{2})-[^\/]*$/
}

function isNotBlank(value) {
  return value && value.trim().length > 0;
}
function isBlank(value) {
  return !value || value.trim().length === 0;
}

function dateToStr(value) {
  if (!value) return '';
  return moment(value).format(environment.date.formatPattern);
  //return value.toLocaleDateString(environment.locale, environment.date.toStringOptions);
}
function capitalize(value) {
  if (!value || value.length === 1) return (value || '').toUpperCase();
  return value.substr(0,1).toUpperCase() + value.substr(1);
}
function appDevEnvironment(done) {

  log(colors.green('Configure environment to \'development\''));

  // Enable development mode
  environment.production = false;

  if (done) done();
}

function appWatch(opts) {

  const skipGallery = (environment.skipGallery || (opts && opts.gallery === false));
  log(colors.green('Watching source files...' + (skipGallery ? ' (no gallery)' : '')));

  const compileHtmlTask = !skipGallery ? appCompileHtml : appCompileHtmlNoGallery;
  const htmlSources = !skipGallery ?
    paths.md
      .concat(...paths.layout_html)
      .concat(...paths.gallery_md)
      .concat(...paths.gallery_img)
      .concat(...paths.gallery_video) :
    paths.md
      .concat(...paths.layout_html);

  // Html
  gulp.watch(htmlSources, (done) => {
    compileHtmlTask(done);
  });

  // Ressources (images, docs)
  gulp.watch(paths.img, appCopyImg);
  gulp.watch(paths.doc, appCopyDoc);
  // JS
  gulp.watch(paths.js, appJs);
  // CSS
  gulp.watch(paths.sass, appSass);
  // Data
  gulp.watch(paths.data_csv, appCsvData);

  // Sitemap
  gulp.watch(paths.layout_txt
    .concat(...paths.output_html), appSitemap);

}

function appWatchNoGallery() {
  appWatch({gallery: false});
}

function appServe() {
  // Serve, using light webserver
  return gulp.src('./public')
    .pipe(webserver({
      path: '/',
      port: 4201,
      livereload: true,
      directoryListing: false,
      fallback: './index.html',
      open: false
    }));
}

/**
 * Generate App CSS (using SASS)
 * @returns {*}
 */
function appSass() {
  log(colors.green('Building App Sass...'));

  return merge(

    // Global style
    gulp.src('./app/style.scss')

      .pipe(sass({
        outputStyle: 'compact',
        includePaths: [ './node_modules/'],
        importer: function importer(url, prev, done) {
          if (url[0] === '~') {
            url = path.resolve('node_modules', url.substr(1));
          }
          return { file: url };
        }
      })).on('error', sass.logError)
      .pipe(base64({
        baseDir: "./public/css",
        extensions: ['svg', 'png', 'gif', /\.jpg#datauri$/i],
        maxImageSize: 14 * 1024
      }))
      .pipe(gulp.dest('./public/css/'))
      .pipe(cleanCss({
        keepSpecialComments: 0
      }))
      .pipe(rename({ extname: '.min.css' }))
      .pipe(gulp.dest('./public/css/')),

    // Copy fonts
    gulp.src('./node_modules/bootstrap-icons/font/fonts/*.*')
      .pipe(gulp.dest('./public/css/fonts/')),

    gulp.src('./node_modules/nanogallery2/dist/css/font/*.*')
      .pipe(gulp.dest('./public/css/font/')),
  );
}


/**
 * Generate App JS
 * @returns {*}
 */
function appJs() {
  log(colors.green('Building JS files...'));

  return gulp.src(['./app/src/js/**/*.js', './app/src/js/**/*.js.map'])
    .pipe(gulp.dest('./public/js/'));
}

/**
 * Transform Markdown into HTML
 * @returns {*}
 */
function appMd2Html() {
  log(colors.green('Converting markdown into HTML...'));

  return gulp.src(paths.md)
    .pipe(markdown())
    .pipe(gulp.dest('dist/html'));
}

function appCopyImg() {
  log(colors.green('Copying images...'));

  return gulp.src(paths.img)
    .pipe(gulp.dest('public/img'));
}

function appCopyDoc() {
  log(colors.green('Copying docs...'));

  return gulp.src(paths.doc)
    .pipe(gulp.dest('public/doc'));
}

function appTimestamp() {
  log(colors.green('Creating \'timestamp.txt\' file...'));

  fs.writeFileSync('dist/timestamp.txt', Date.now().toString());

  return gulp.src('dist/timestamp.txt')
    .pipe(gulp.dest('public'));
}

function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}

function getFiles(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return !fs.statSync(path.join(dir, file)).isDirectory();
    });
}

/**
 * Create galleries from pictures
 * @returns {*}
 */
function appGalleryImg(done) {
  if (!environment.production) {

  }
  log(colors.green('Creating galleries...'));

  const resizeThumbnail = gulp.src(paths.gallery_img)
    .pipe(imageResize({
      width : 500,
      height : 500,
      upscale : true
    }))
    .pipe(chmod(0o655))
    .pipe(rename((path) => {
      path.dirname += "/thumbnail";
    }))
    .pipe(gulp.dest('public/gallery'));

  const resizePictures = gulp.src(paths.gallery_img)
    .pipe(imageResize({
      width : 1600,
      height : 1600,
      upscale : false
    }))
    .pipe(chmod(0o655))
    .pipe(gulp.dest('public/gallery'));

  const copyVideo = gulp.src(paths.gallery_video)
    .pipe(chmod(0o655))
    .pipe(gulp.dest('public/gallery'));

  const galleryRootDir = 'app/src/gallery';
  const galleryFolders = getFolders(galleryRootDir);
  const filename2Title = (filename) => {
    const matches = REGEXP.gallery_filename.exec(filename);
    if (!matches) return '';
    const date = `${matches[3]}/${matches[2]}/${matches[1]}`;
    const title = (matches[4] || '').replace(/_+/g, ' ');
    return [date, title].join(' ').trim();
  };

  const htmlTasks = galleryFolders.map(function(galleryName) {

    log(colors.green(`Creating gallery '${galleryName}'...`));

    const sources = [];
    const thumbnails = [];
    const titles = [];
    let filenames = getFiles(path.join(galleryRootDir, galleryName))
      .filter(filename => !filename.startsWith('__'))
      .sort();

    // Reverse order, if need
    if (environment.gallery && (environment.gallery.reverseGalleries || []).includes(galleryName)) {
      filenames = filenames.reverse(); 
    }
    filenames.forEach(filename => {
        const isVideo = filename.endsWith(environment.videos.suffix);
        // Compute source
        const source = path.join('gallery', galleryName, filename);
        sources.push(source);
        // Compute thumbnail
        const thumbnail = isVideo
          ? '/' + path.join('gallery', galleryName, 'thumbnail', path.basename(filename, environment.videos.suffix) + environment.videos.thumbnailSuffix)
          : '/' + path.join('gallery', galleryName, 'thumbnail', filename);
        thumbnails.push(thumbnail);

        const title = filename2Title(filename);
        titles.push(title);
      });

    return gulp.src('app/template/gallery.html')
      // Processing @include annotations
      .pipe(fileInclude({
        context: {
          basepath: 'dist/html',
          gallery: galleryName,
          sources,
          thumbnails,
          titles
        }
      }))
      .pipe(rename((path) => {
        path.basename = galleryName;
      }))
      // write to output again
      .pipe(gulp.dest(path.join('dist/html/gallery')));
  });

  return merge(resizeThumbnail, resizePictures, copyVideo, htmlTasks);
}

function parseHtmlFile(file, useCache) {

  let result = cache.htmlFiles[file]
  if (useCache && result) return result; // Use cache, if exists

  const lastSlashIndex = file.lastIndexOf('/');
  const directory = lastSlashIndex !== -1 ? file.substr(0, lastSlashIndex) : '.';

  // DEBUG
  //log(colors.grey("Parsing HTML file: " + file + "..."));

  const content = fs.readFileSync(file, {encoding:'utf8', flag:'r'});

  result = parseHtml(content);

  // Get date, from the file name
  const dateInFileMatches = REGEXP.isoDate.exec(file);
  result.date = dateInFileMatches && new Date(dateInFileMatches[1]);

  // Try to get thumbnail from included file
  let thumbnail = result.thumbnail;
  if (!thumbnail) {
    const includeFileMatches = REGEXP.includeFile.exec(content);
    const includePath = includeFileMatches && includeFileMatches[1];
    if (includePath) {
      const rootDir = file.substr(0, file.lastIndexOf('/'));
      const includeAbsolutePath = includePath.startsWith('/') ? includePath : path.join(rootDir, includePath);
      if (fs.existsSync(includeAbsolutePath)) {
        //log(colors.grey('  Trying to find a thumbnail, from included file: ' + includePath));
        thumbnail = parseHtmlFile(includeAbsolutePath).thumbnail;
      }
      else {
        log(colors.red("Missing file: @@include('" + includePath + "')"));
      }
    }
  }

  // Resolve relative path, for thumbnail
  if (thumbnail) {
    if (thumbnail.startsWith('../')) {
      thumbnail = path.relative('dist/html', path.join(directory, thumbnail));
      // DEBUG
      //log("1-" + thumbnail);

      // If comes from a gallery: use the miniature image
      if (thumbnail.startsWith('gallery/') != null) {
        const miniaturePath = path.join(path.dirname(thumbnail), 'thumbnail', path.basename(thumbnail));
        if (fs.existsSync(path.join('public', miniaturePath))) {
          thumbnail = miniaturePath;

          // DEBUG
          //log("2-" + thumbnail);
        }
      }
    }
    if (!thumbnail.startsWith('http') && !thumbnail.startsWith('/')) {
      thumbnail = '/' + thumbnail;
    }
  }
  result.thumbnail = thumbnail;

  cache.htmlFiles[file] = result;
  return result
}

function parseHtml(content) {

  const titleMatches = REGEXP.title.exec(content);
  const subtitleMatches = REGEXP.subtitle.exec(content);
  const urlMatches = REGEXP.url.exec(content);
  const url = urlMatches && urlMatches[1];

  const descriptionMatches = REGEXP.description.exec(content);
  let description = descriptionMatches && descriptionMatches[1] || '';

  const tagNamesToRemove = ['a', 'img', 'b', 'i'];
  if (description && hasOneOfTags(description, tagNamesToRemove)) {
    //log(colors.grey(" Cleaning description: " + description));
    description = removeTags(description, tagNamesToRemove);
    //log(colors.grey(" description: " + description));
  }

  const type = url && REGEXP.youtube.test(url) ? 'youtube' : (url && REGEXP.lbry.test(url) ? 'lbry' : 'other');

  const imgMatches = REGEXP.img.exec(content);
  let thumbnail = imgMatches && imgMatches[1];
  //if (thumbnail) log(colors.grey(" thumbnail: " + thumbnail));

  return {
    title: titleMatches && titleMatches[1],
    subtitle: subtitleMatches && subtitleMatches[1] || undefined,
    description,
    thumbnail,
    url,
    type
  };
}

function hasOneOfTags(value, tagNames) {
  return tagNames.findIndex(tagName => value.indexOf('<' + tagName) !== -1) !== -1;
}
function removeTags(description, tagNames) {
  //log(colors.grey(" Cleaning description: " + description));
  return tagNames.reduce((res, tagName) => {
    return removeTag(res, tagName);
  }, description);
}
function removeTag(value, tagName) {
  if (!value) return value;
  let startIndex;
  while((startIndex = value.indexOf('<' + tagName)) !== -1) {
    let cleanValue = value.substr(0, startIndex);
    let endIndex = value.indexOf('>', startIndex-1);
    if (endIndex !== -1) {
      cleanValue += value.substr(endIndex+1);
    }
    value = cleanValue;
  }
  if (!tagName.startsWith('/')) {
    return removeTag(value, '/' + tagName);
  }
  return value;
}
/**
 * Create videos pages, from markdown files
 * @returns {*}
 */
function appVideos() {
  log(colors.green('Creating video pages, from markdown...'));

  const galleryRootDir = 'dist/html/videos';
  const galleryMdFolders = getFolders(galleryRootDir);

  // For each gallery
  return merge(...galleryMdFolders.map(gallery => {

    let fileNames = getFiles(path.join(galleryRootDir, gallery))
      .filter(filename => filename.endsWith('.html'));

    log(colors.green(`Creating video gallery '${gallery}'...`));

    const items = fileNames
      .sort().reverse()
      .map(filename => path.join('dist', 'html', 'videos', gallery, filename))
      .map(filename => {
        const item = parseHtmlFile(filename);
        const i18nGallery = (environment.videos.i18n[gallery] || gallery).toUpperCase();
        item.category = environment.videos.i18n.video.toUpperCase() + (i18nGallery && (' > ' + i18nGallery) || '');
        item.dateStr = item.date && dateToStr(item.date);

        return item;
      })
      .filter(item => item && item.title && item.url); // Skip incomplete JSON

    return merge(
      gulp.src('app/template/card-videos.html')
        // Processing @include annotations
        .pipe(fileInclude({
          basepath: 'dist/html',
          context: {
            gallery,
            items,
            class: 'col-12 col-lg-6',
            showMoreLink: '',
            showMoreLinkClass: 'invisible'
          }
        }))
        .pipe(rename((path) => {
          path.basename = gallery;
        }))

        .pipe(gulp.dest(path.join('dist/html/videos'))),

      gulp.src('app/template/card-videos.html')
        // Processing @include annotations
        .pipe(fileInclude({
          basepath: 'dist/html',
          context: {
            gallery,
            items: items.slice(0,Math.min(items.length, environment.videos.previewCount)),
            class: 'col-12 col-lg-6',
            showMoreLink: '/pages/videos.html'
          }
        }))
        .pipe(rename((path) => {
          path.basename = gallery + '-preview';
        }))
        .pipe(gulp.dest(path.join('dist/html/videos')))
    );
  }));
}


function appForumTopics(done) {
  log(colors.green(`Fetching forum topics...`));
  if (!environment.forum.enable || !environment.forum.topicsUrl) {
    if (done) done();
    return;
  }

  const destDir = path.join('dist', 'forum');

  return request({
    url: environment.forum.topicsUrl,
    headers: {
      'User-Agent': 'request',
      'Accept': 'application/json;q=0.9, */*;q=0.8'
    }
  })
    .pipe(source('topics.json'))
    .pipe(streamify(jeditor(function (json) {

      // Read users
      const users = (json.users || []).map(user => {
        const avatar = environment.forum.url + user.avatar_template
          .replace('{size}', ''+environment.forum.avatarSize);
        const name = user.username === 'system' ? 'Admin' : capitalize(user.name || user.username);

        return {
          ...user,
          avatar,
          name
        }
      });

      // Read topics
      const topicsFilter = environment.forum.topicsFilter || ((_) => true);
      const topics = (json.topic_list && json.topic_list.topics || [])
        .filter(topicsFilter);

      log(colors.grey(`Processing ${topics.length} topics...`));

      // Fill topics
      return topics
        .map(function (topic) {
          const url = `${environment.forum.url}/t/${topic.slug}/${topic.id}`;
          log(colors.grey(' - ' + url));
          const posters = (topic.posters || []).map(poster => users.find(u => u.id === poster.user_id))
            .filter(u => !!u);
          const basename = `${topic.created_at.substr(0, 10)}-${topic.slug}-${topic.id}`;
          return {
            ...topic,
            basename,
            date: topic.created_at,
            url,
            posters
          };
      });
    })))
    .pipe(gulp.dest(destDir));
}


function appForumPosts() {
  log(colors.green(`Fetching forum posts...`));

  const sourceDir = path.join('dist', 'forum');
  const topics = JSON.parse(fs.readFileSync(path.join(sourceDir, 'topics.json'), 'utf8'));
  const destDir = path.join('dist', 'forum');

  return merge(
    ...topics.map(topic => {
      return request({
        url: topic.url + '.json',
        headers: {
          'User-Agent': 'request',
          'Accept': 'application/json;q=0.9, */*;q=0.8'
        }})
        .pipe(source(topic.basename + '.json'))
        .pipe(streamify(jeditor(function (json) {
          return json.post_stream && json.post_stream.posts || [];
        })))
        .pipe(gulp.dest(destDir));
    })
  );
}

function appForumArticles() {
  log(colors.green(`Creating forum articles...`));

  const sourceDir = path.join('dist', 'forum');
  const topics = JSON.parse(fs.readFileSync(path.join(sourceDir, 'topics.json'), 'utf8'));
  const destDir = path.join('app', 'src', 'articles', 'forum');

  return merge(
    ...topics.map(topic => {

      const file = path.join(sourceDir, topic.basename + '.json');
      const posts = JSON.parse(fs.readFileSync(file, 'utf8'))

      const post = (posts || []).find(p => p.cooked && p.cooked.trim().length);
      const cooked = post.cooked;
      const dateStr = topic.date && dateToStr(topic.date);

      return gulp.src('app/template/forum-article.md')
        // Processing @@ annotations
        .pipe(fileInclude({
          context: {
            title: topic.title,
            dateStr,
            posters: topic.posters,
            cooked,
            imageUrl: topic.image_url || '',
            imageClass: topic.image_url && '' || 'insivible',
            url: topic.url
          }
        }))
        .pipe(rename((path) => {
          path.basename = topic.basename;
        }))
        .pipe(gulp.dest(destDir));
    })
  )
}

/**
 * Create articles by categories
 * @returns {*}
 */
function appArticlesByCategory() {

  log(colors.green(`Creating articles categories...`));
  const rootDir = path.join('dist', 'html', 'articles');
  const categories = getFolders(rootDir);
  const today = moment(new Date()).startOf('day');

  const articlesByCategory = categories.reduce((res, category) => {
    const fileNames = getFiles(path.join(rootDir, category))
      .filter(filename => filename.endsWith('.html'))
      .sort().reverse();

    const items = fileNames
      .map(filename => {
        const file = path.join(rootDir, category, filename)
        const item = parseHtmlFile(file);
        item.file = file;
        item.path = '/' + path.join('articles', category, filename);

        item.category = (environment.articles.i18n[category] || category).toUpperCase();
        item.dateStr = item.date && dateToStr(item.date);
        item.thumbnail = item.thumbnail || environment.defaultThumbnail;

        // Truncate description
        if (item.description && item.description.length > environment.articles.cardBodyMaxLength) {
          item.description = item.description.substr(0, environment.articles.cardBodyMaxLength);
          const lastSpaceIndex = item.description.lastIndexOf(' ');
          item.description = item.description.substr(0, lastSpaceIndex) + ' (...)';
        }
        return item;
      })
    res[category] = items;
    return res;
  }, {});

  articlesByCategory['events'] = articlesByCategory['events'].filter(item => {
   if (item.date && today.isSameOrBefore(item.date)) {
     item.category = (environment.articles.i18n['agenda'] || 'agenda').toUpperCase();
     articlesByCategory['agenda'] = articlesByCategory['agenda'] || [];
     articlesByCategory['agenda'].push(item);
     return false;
   }
   return true;
  });

  const allItems = Object.values(articlesByCategory).reduce((res, articles) => res.concat(articles))
    .sort((a, b) => ((!a || !b) ? 0 : (a.date - b.date)))
    .reverse(); // Recent first

  // Last articles (all categories)
  return merge(
    gulp.src('app/template/card-articles.html')
      // Processing @@ annotations
      .pipe(fileInclude({
        context: {
          items: allItems.slice(0, Math.min(allItems.length, environment.articles.previewCount)),
          class: 'col-12 col-lg-6',
          showMoreLinkClass: 'invisible',
          titleClass: 'invisible',
        }
      }))
      .pipe(rename((path) => {
        path.basename = 'index-preview';
      }))
      .pipe(gulp.dest(rootDir)),

    // For each folder (=categories)
    ...Object.keys(articlesByCategory).map(category => {

      log(colors.green(`Creating articles category '${category}'...`));
      const items = articlesByCategory[category];
      const i18nCategory = (environment.articles.i18n[category] || category);
      const title = capitalize(i18nCategory) + 's';

      return merge(
        // Full list, by category
        gulp.src('app/template/card-articles.html')
          // Processing @@ annotations
          .pipe(fileInclude({
            context: {
              items,
              title,
              class: 'col-12 col-lg-6',
              showMoreLinkClass: 'invisible'
            }
          }))
          .pipe(rename((path) => {
            path.basename = category;
          }))
          .pipe(gulp.dest(rootDir)),

        // Preview (last X articles), by category
        gulp.src('app/template/card-articles.html')
          // Processing @@ annotations
          .pipe(fileInclude({
            context: {
              items: items.slice(0,Math.min(items.length, environment.articles.previewCount)),
              class: 'col-12 col-lg-6',
              showMoreLink: '/pages/' + category + '.html'},
            // No title
            title: '', titleClass: 'invisible'
          }))
          .pipe(rename((path) => {
            path.basename = category + '-preview';
          }))
          .pipe(gulp.dest(rootDir))
      );
    })
  );

}


/**
 * Prepare layout files (header and footer)
 * @returns {*}
 */
function appLayoutFiles() {
  log(colors.green('Preparing HTML layout...'));

  return gulp.src(paths.layout_html)
    // Processing @include annotations
    .pipe(fileInclude({
      basepath: 'dist/html'
    }))
    .pipe(gulp.dest('dist/layout'));
}

/**
 * Transform Markdown into HTML
 * @returns {*}
 */
function appFinalHtml() {
  log(colors.green('Building final HTML files...'));

  const headerContent = fs.readFileSync('./dist/layout/header.html', 'utf8');
  const menuContent = fs.readFileSync('./dist/layout/menu.html', 'utf8');
  const footerContent = fs.readFileSync('./dist/layout/footer.html', 'utf8');
  const pkgObj = JSON.parse(fs.readFileSync('./package.json', 'utf8'));
  const version = pkgObj.version;
  const title = pkgObj.description;
  const description = pkgObj.description;
  const image = '/img/logo-250px.png';
  const imageType = 'image/png';
  const url = (pkgObj.homepage || '');
  const homepage = url
    .replace('http://', '')
    .replace('https://', '');
  const date = moment().format('DD/MM/YYYY HH:m:s');
  const editLink = environment.sourceEditionLink;

  const pageTitleRegexp = /main">[^<]*<h[12] id="[^"]+">([^<]+)<\/h[12]>/ig;
  const descriptionRegexp = /main">[^<]*<h[12] id="[^"]+">[^<]+<\/h[12]>[^<]*<p>([^<]+)</ig;

  return gulp.src(paths.tmp_html)
    // Processing @@include annotations
    .pipe(fileInclude({ basepath: '@file' }))
    // Add header/footer
    .pipe(header(headerContent, {title, image, imageType, description, url, menuContent}))
    .pipe(footer(footerContent, {editLink, version, date, homepage}))
    // Replace
    .pipe(insert.transform(function(contents, file) {

      const res = parseHtmlFile(file.path);

      // Page URL
      log(colors.grey('Processing file ' + file.relative + '...'))
      let pageUrl = (url + '/' + file.relative);

      // Remove html extension (not need)
      pageUrl = pageUrl.replace(/\/index.html$/, '')
        .replace(/[.]html$/, '');
      contents = contents.replace(`property="og:url" content="${url}"`, `property="og:url" content="${pageUrl}"`);

      // Page title
      const pageTitleMatches = pageTitleRegexp.exec(contents);
      const pageTitle = pageTitleMatches && pageTitleMatches[1];
      if (pageTitle && pageTitle.trim().length) {
        //log(colors.grey('  og:title: ' + pageTitle));
        contents = contents.replace(`<title>${title}</title>`, `<title>${pageTitle} - ${title}</title>`)
        contents = contents.replace(`property="og:title" content="${title}"`, `property="og:title" content="${pageTitle} - ${title}"`)
        contents = contents.replace(`property="og:image:alt" content="${description}"`, `property="og:image:alt" content="${pageTitle}"`)
      }

      // Page description
      const pageDescriptionMatches = descriptionRegexp.exec(contents);
      const pageDescription = pageDescriptionMatches && pageDescriptionMatches[1] && (pageDescriptionMatches[1] + ' (...)');
      if (isNotBlank(pageDescription)) {
        //log(colors.grey('  og:description: ' + pageDescription));
        contents = contents.replace(`property="og:description" content="${description}"`, `property="og:description" content="${pageDescription}"`);
      }

      // Page image
      let pageImage = res && res.thumbnail;
      if (isBlank(pageImage)) {
        // Try to get from the img directory
        pageImage = '/img/' + + file.stem + '.png';
        if (!fs.existsSync('./app/src' + pageImage)) {
          pageImage = null;
          log(colors.grey('  No image found ' + file.path));
        }
      }
      if (isNotBlank(pageImage)) {
        // Replace relative and absolute URL
        pageImage = pageImage.replace(/^[.]?\//, url + '/');

        //log(colors.grey('  og:image: ' + pageImage));
        const pageImageType = pageImage.endsWith('.jpg') ? 'image/jpeg' : 'image/png';
        contents = contents.replace(`property="og:image" content="${image}"`, `property="og:image" content="${pageImage}"`);
        contents = contents.replace(`property="og:image:type" content="${imageType}"`, `property="og:image:type" content="${pageImageType}"`);
      }

      return contents;
    }))

    // Replace link (remove trailling .html)
    .pipe(replace(/href="(\.?\/[^"]+)\.md"/g, 'href="$1.html"'))
    .pipe(replace(/href="(\.?\/[^"]+)\.html"/g, environment.production ? 'href="$1"' : 'href="$1.html"'))

    .pipe(gulp.dest('public'));
}

function appCleanUnusedHtml(done) {
  log(colors.green("Cleaning unused HTML files..."));

  // Clean old generated HTML file
  return gulp.src(paths.output_html_with_sources, {
    read: true
  })
  .on("data", function(file){
    const sourceFile = './app/src/' + file.relative.replace(/\.html$/, '.md')
    // Check if source (MD file) exists
    if (!fs.existsSync(sourceFile)) {
      // Then remove if source not exists
      const distPath = './dist/html/' + file.relative;
      if (fs.existsSync(distPath)) {
        log(colors.grey("Removing old file: " + distPath));
        fs.unlinkSync(distPath);
      }

      const publicPath = './public/' + file.relative;
      if (fs.existsSync(publicPath)) {
        log(colors.grey("Removing old file: " + publicPath));
        fs.unlinkSync(publicPath);
      }
    }
  })
  .on("end", done);
}

/**
 * Genrate sitemap.xml
 * @returns {*}
 */
function appSitemap() {
  log(colors.green("Generating 'sitemap.xml'..."));

  const siteUrl = JSON.parse(fs.readFileSync('./package.json', 'utf8')).homepage;

  return merge(
    // Copy robots.txt
    gulp.src('app/layout/robots.txt')
      // Add sitemap URL
      .pipe(header('Sitemap: '+siteUrl+'/sitemap.xml\n'))
      .pipe(gulp.dest('public')),

    // Generate sitemap.xml file
    gulp.src(paths.output_html_with_sources, {
      read: true
    })
    .pipe(sitemap({
      siteUrl,
      changefreq: 'daily',
      // Compute value of the tag <priority>
      priority: function(siteUrl, loc, entry) {
        const relativePath = loc.replace(siteUrl, '').replace(/^\//, '');
        const slashCount = relativePath.split('/').length
        const priority = slashCount === 1 ? 1 : 0.5;
        log(colors.grey("Priority of '/" + relativePath + "': " + priority));
        return priority;
      },
      // Compute last modified time.
      lastmod: function(file) {
        const sourceFile = './app/src/' + file.relative.replace(/\.html$/, '.md')

        // Source not exists anymore: reused the generated file's date
        if (!fs.existsSync(sourceFile)) {
          return fs.statSync('./public/' + file.relative).mtime.toString().trim();
        }
        const dateStr = fs.statSync(sourceFile).mtime.toString().trim();
        log(colors.grey("Date of '/" + file.relative + "': " + dateStr));
        return dateStr;
      }
    }))
      // HTML not need, in production
      .pipe(replace(/\.html</g, environment.production ? '<' : '.html<'))

      .pipe(gulp.dest('public')));
}

function appCsvData() {
  log(colors.green('Transform CSV file ...'));
  // Transform CSV into tables
  return gulp.src(paths.data_csv)
    .pipe(tapCsv({ columns:true }))
    .pipe(replace(/{"type":"RECORD","stream":"[^"]+","record":/gi, "\t"))
    .pipe(replace("}}", "},"))
    .pipe(rename({ extname: ".json" }))
    .pipe(header("[\n"))
    .pipe(footer("]"))
    .pipe(replace(",]", "\n]"))
    .pipe(gulp.dest('public/data'));

}

function appClean() {
  return del([
    './dist',
    './public'
  ]);
}

function appZip() {
  const srcPath = './public';
  const distPath = './dist';
  const packageObj = JSON.parse(fs.readFileSync('./package.json', 'utf8'));
  const projectName = packageObj.name;
  const version = packageObj.version;
  const txtFilter = filter(["**/*.txt"], { restore: true });

  return gulp.src(srcPath + '/**/*.*')

    // Process TXT files: Add the UTF-8 BOM character
    .pipe(txtFilter)
    .pipe(header('\ufeff'))
    .pipe(txtFilter.restore)

    .pipe(zip(projectName + '-v'+version+'.zip'))
    .pipe(gulp.dest(distPath));
}

function buildSuccess(done) {
  const packageObj = JSON.parse(fs.readFileSync('./package.json', 'utf8'));
  const projectName = packageObj.name;
  const version = packageObj.version;
  log(colors.green("Web artifact created at: 'dist/"+ projectName +"-v" + version + ".zip'"));
  if (done) done();
}


function help() {
  log(colors.green("Usage: gulp {clean|build} OPTIONS"));
  log(colors.green(""));
  log(colors.green("NAME"));
  log(colors.green(""));
  log(colors.green("  config --env <config_name>  Configure environment (create file `www/config.js`). "));
  log(colors.green("  clean                       Clean build directory"));
  log(colors.green("  build                       Build from sources (HTML, CSS and JS)"));
  log(colors.green(""));
  log(colors.green("OPTIONS"));
  log(colors.green(""));
  log(colors.green("  --release                   Release build (with uglify and sourcemaps)"));
  log(colors.green("  --uglify                    Build using uglify plugin"));
}

/* --------------------------------------------------------------------------
   -- Combine task
   --------------------------------------------------------------------------*/
const appForum = gulp.series(appForumTopics, appForumPosts, appForumArticles)
const appCopyResources = gulp.parallel(appCopyImg, appCopyDoc);
const appCompileHtml = (opts) => gulp.series(appMd2Html, appGalleryImg, appVideos, appArticlesByCategory, appLayoutFiles, appFinalHtml)(opts);
const appCompile = gulp.series(appCsvData, appCopyResources, appCompileHtml, appCleanUnusedHtml, appSitemap, appTimestamp, appJs, appSass);

// Same, without gallery (that is long)
const appCompileHtmlNoGallery = gulp.series(appMd2Html, appVideos, appArticlesByCategory, appLayoutFiles, appFinalHtml);
const appCompileNoGallery = gulp.series(appCsvData, appCopyResources, appCompileHtmlNoGallery, appCleanUnusedHtml, appSitemap, appTimestamp, appJs, appSass);

/* --------------------------------------------------------------------------
   -- Define public tasks
   --------------------------------------------------------------------------*/

exports.help = help;
exports.clean = appClean;
exports.sass = appSass;
exports.watch = appWatch;
exports.serve = appServe;
exports.compile = appCompile;
exports.build = gulp.series(appCompile, appZip, buildSuccess);

exports.dev = gulp.series(appDevEnvironment, appCompileNoGallery, gulp.parallel(appWatchNoGallery, appServe));
exports.categories = appArticlesByCategory
exports.galleries = appGalleryImg
exports.forum = appForum;

exports.default = gulp.series(appCompile, gulp.parallel(appWatch, appServe));
