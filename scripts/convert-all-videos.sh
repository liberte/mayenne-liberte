#!/bin/bash

# Get to the root project
SCRIPT_DIR=$(dirname "$(readlink "$BASH_SOURCE" || echo "$BASH_SOURCE")")
SCRIPT_DIR=$(cd "${SCRIPT_DIR}" && pwd -P)
PROJECT_DIR=$(cd "${SCRIPT_DIR}/.." && pwd -P)
export PROJECT_DIR
cd $PROJECT_DIR

# Check if trash has been installed
echo "temp" > .local/trash.test
trash .local/trash.test
if [[ $? -ne 0 ]]; then
  echo "ERROR: 'trash' command not found. Please install trash-cli first !"
  echo " > sudo apt install trash-cli"
  exit 1
fi;

WIDTH=720
OUTPUT_SUFFIX=".min"
OUTPUT_EXTENSION=".mp4"
REMOVE_ORIGINAL=true

FILES=$(ls app/src/gallery/*/*.mp4)
#FILES=$(ls app/src/gallery/2021-02-08/*.mp4)
#FILES=$(ls app/src/gallery/2022-02-08/2022-02-12-*.mp4)

echo "Processing videos '${PROJECT_DIR}/**/*.mp4' ..."

for FILE in $FILES;
do
  dirname=$(dirname $FILE)
  basename=$(basename -s .mp4 $FILE)
  THUMBNAIL_DIR="${dirname}/thumbnail"

  if [[ $basename == !(*${OUTPUT_SUFFIX}) ]]; then

    OUTPUT="${dirname}/${basename}${OUTPUT_SUFFIX}"
    OUTPUT_MP4="${OUTPUT}.mp4"
    OUTPUT_WEBM="${OUTPUT}.webm"
    OUTPUT_THUMBNAIL="${THUMBNAIL_DIR}/${basename}${OUTPUT_SUFFIX}.jpg"
    TMP_OUTPUT_THUMBNAIL="${OUTPUT}.jpg"

    INPUT_WIDTH=$(ffprobe -v error -select_streams v -show_entries stream=width -of csv=p=0:s=x $FILE)
    INPUT_HEIGHT=$(ffprobe -v error -select_streams v -show_entries stream=height -of csv=p=0:s=x $FILE)

    if [[ $INPUT_HEIGHT -gt $INPUT_WIDTH ]]; then
      SCALE_FACTOR="height" # landscape
    else
      SCALE_FACTOR="width" # portrait
    fi

    if [[ -f "${OUTPUT_MP4}" && -f "${OUTPUT_THUMBNAIL}" ]]; then
      echo "  \--> Already done. Skipping"
      if [[ ${REMOVE_ORIGINAL} == true ]]; then
        echo "  \--> Deleting file  $FILE (to trash)"
        trash $FILE
      fi;
    else
      echo ""
      echo "-----------------------------------------------------------------"
      # Remove previous build
      rm -f ${OUTPUT_MP4} ${OUTPUT_WEBM} ${OUTPUT_THUMBNAIL} ${TMP_OUTPUT_THUMBNAIL}

      echo "- Resizing '$FILE' $SCALE_FACTOR to $WIDTH..."

      # Launch conversion
      . ${SCRIPT_DIR}/convert-one-video.sh $FILE $SCALE_FACTOR $WIDTH 1 $OUTPUT > /dev/null
      [[ $? -ne 0 ]] && exit 1

      # Move image thumbnail
      if [[ -f "${TMP_OUTPUT_THUMBNAIL}" ]]; then
        mkdir -p ${THUMBNAIL_DIR}
        mv ${TMP_OUTPUT_THUMBNAIL} ${THUMBNAIL_DIR}
        [[ $? -ne 0 ]] && exit 1
      fi;

      if [[ ${REMOVE_ORIGINAL} == true ]]; then
        echo "  \--> Deleting file  $FILE (to trash)"
        trash $FILE
      fi;

      echo "  \--> [Done]"
    fi;
  fi;
done;

echo "Processing videos '${PROJECT_DIR}/**/*.mp4' [OK]"
