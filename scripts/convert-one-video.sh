#!/bin/bash

#Scaling
#- Scale can be used as is which will set the height to 560 but keep aspect ratio for width.
#- Other options include setting both with & height
#- Watch out for sizing errors when not divisible by 2

if [[ ! "$1" || ! "$2" || ! "$3" ]] || [[ "$1" = '--help' ]]; then
	if [[ "$1" = '--help' ]]; then
		echo " "
		echo "Converts input video files for the web, with several parameters to ease the process and replicate across projects:"
		echo " "
	else
		echo " "
		echo "Please provide all the necessary parameters"
		echo " "
	fi
	echo "Usage: video-convert <input file> <scale factor> <scale amount> <#thumbnail frame> <#output name> // # means not required"
	echo "Example: video-convert video.mov width 720 // Converts video and scales it aspectly based on a width of 720"
	echo "Example: video-convert video.mov height 560 25 // Converts video and scales it aspectly based on a height of 560 and exports thumbnail at 25 seconds in"
	echo "Example: video-convert video.mov height 560 25 myvideo // Converts video and scales it aspectly based on a height of 560 and exports thumbnail at 25 seconds in, and exports videos to myvideo.extension.  Default name is the input file name with a different extension"
		echo " "
else
	input="$1"
	factor="$2"
	even=$(( $3 - ($3 % 2) )) # Makes scale amount even just incase an odd number is given
	scale="$even"
	if [[ ! "$4" ]]; then
		frame="1"
	else
		frame="$4"
	fi
	if [[ ! "$5" ]]; then
		output="${input%.*}"
	else
		output="$5"
	fi
	echo "input is $input, output is $output"
	echo "scale factor is $2, scale amount is $scale"
	echo "thumbnail frame is $frame"
	if [ "$factor" = 'height' ]; then
	  if [[ -f "$input" ]]; then
      #MP4 1st pass
      ffmpeg -i "$input" -vcodec libx264 -vprofile high -preset veryslow -b:v 225k -maxrate 300k -bufsize 1000k -vf scale="trunc(oh*a/2)*2:$scale" -threads 2 -pass 1 -y -an -f mp4 /dev/null
      #MP4 with audio 2nd pass
      ffmpeg -i "$input" -vcodec libx264 -vprofile high -preset veryslow -b:v 225k -maxrate 300k -bufsize 1000k -vf scale="trunc(oh*a/2)*2:$scale" -threads 2 -pass 2 -acodec mp3 -b:a 128k -f mp4 "$output.mp4"
      #webm 1st pass
      #ffmpeg -i "$input" -codec:v libvpx -quality good -cpu-used 0 -b:v 225k -qmin 10 -qmax 42 -maxrate 300k -bufsize 1000k -threads 2 -vf scale="trunc(oh*a/2)*2:$scale" -y -an -pass 1 -f webm /dev/null
      #webm 2st pass with audio
      #ffmpeg -i "$input" -codec:v libvpx -quality good -cpu-used 0 -b:v 225k -qmin 10 -qmax 42 -maxrate 300k -bufsize 1000k -threads 2 -vf scale="trunc(oh*a/2)*2:$scale" -codec:a libvorbis -b:a 128k -pass 2 -f webm "$output.webm"
      #Generate single image from selected frame of video (default is 1), scale and save as jpg
      ffmpeg -i "$input" -ss $frame -vframes 1 -vf scale="trunc(oh*a/2)*2:$scale" "$output.jpg"
		elif [[ -f "$output.mp4" ]]; then
      #Generate single image from selected frame of video (default is 1), scale and save as jpg
      ffmpeg -i "$output.mp4" -ss $frame -vframes 1 -vf scale="trunc(oh*a/2)*2:$scale" "$output.jpg"
    fi
	elif [ "$factor" = 'width' ]; then
	  if [ -f "$input" ]; then
      #MP4 1st pass
      ffmpeg -i "$input" -vcodec libx264 -vprofile high -preset veryslow -b:v 225k -maxrate 300k -bufsize 1000k -vf scale="$scale:trunc(ow/a/2)*2" -threads 2 -pass 1 -y -an -f mp4 /dev/null
      #MP4 with audio 2nd pass
      ffmpeg -i "$input" -vcodec libx264 -vprofile high -preset veryslow -b:v 225k -maxrate 300k -bufsize 1000k -vf scale="$scale:trunc(ow/a/2)*2" -threads 2 -pass 2 -acodec mp3 -b:a 128k -f mp4 "$output.mp4"
      #webm 1st pass
      #ffmpeg -i "$input" -codec:v libvpx -quality good -cpu-used 0 -b:v 225k -qmin 10 -qmax 42 -maxrate 300k -bufsize 1000k -threads 2 -vf scale="$scale:trunc(ow/a/2)*2" -y -an -pass 1 -f webm /dev/null
      #webm 2st pass with audio
      #ffmpeg -i "$input" -codec:v libvpx -quality good -cpu-used 0 -b:v 225k -qmin 10 -qmax 42 -maxrate 300k -bufsize 1000k -threads 2 -vf scale="$scale:trunc(ow/a/2)*2" -codec:a libvorbis -b:a 128k -pass 2 -f webm "$output.webm"
      #Generate single image from selected frame of video (default is 1), scale and save as jpg
      ffmpeg -i "$input" -ss $frame -vframes 1 -vf scale="$scale:trunc(ow/a/2)*2" "$output.jpg"
    elif [ -f "$output.mp4" ]; then
      #Generate single image from selected frame of video (default is 1), scale and save as jpg
      ffmpeg -i "$output.mp4" -ss $frame -vframes 1 -vf scale="$scale:trunc(ow/a/2)*2" "$output.jpg"
    fi
	else
		echo "Scale factor not recognized. Permitted options are <height> or <width>"
	fi
fi
