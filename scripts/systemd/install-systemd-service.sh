#!/bin/bash

sudo systemctl stop mayenne-liberte.timer
sudo systemctl disable mayenne-liberte.timer
sudo systemctl disable mayenne-liberte.service

sudo cp -f mayenne-liberte.* /etc/systemd/system/
sudo chown root:root /etc/systemd/system/mayenne-liberte.*
sudo chmod 0644 /etc/systemd/system/mayenne-liberte.*

# Reload systemd

sudo systemctl daemon-reload

sudo systemctl enable mayenne-liberte.timer
sudo systemctl start mayenne-liberte.timer

sudo systemctl status mayenne-liberte.timer

sudo touch /var/log/mayenne-liberte.timer.log
