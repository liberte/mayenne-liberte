#!/bin/bash

# Get to the root project
SCRIPT_DIR=$(dirname "$(readlink "$BASH_SOURCE" || echo "$BASH_SOURCE")")
SCRIPT_DIR=$(cd "${SCRIPT_DIR}" && pwd -P)
PROJECT_DIR=$(cd "${SCRIPT_DIR}/../.." && pwd -P)
export PROJECT_DIR
cd $PROJECT_DIR

LOG_FILE=/var/log/mayenne-liberte.timer.log
export LOG_FILE
[[ ! -f ${LOG_FILE} ]] && touch ${LOG_FILE}

DATE_STR=$(date +%F-%T)

# Update the project
echo "${DATE_STR} - Update projet '${PROJECT_DIR}' ..." >> $LOG_FILE
echo "${DATE_STR} - Update projet '${PROJECT_DIR}' ..."

git fetch
GIT_LOG=$(git pull)
echo "${GIT_LOG}" >> $LOG_FILE
echo "${GIT_LOG}"

# No changes: exit
if [[ "${GIT_LOG}" == "Déjà à jour." || "${GIT_LOG}" == "Already up to date." ]]; then
  exit 1
fi

echo "${DATE_STR} - Starting to deploy '${PROJECT_DIR}' ..." >> $LOG_FILE
echo "${DATE_STR} - Starting to deploy '${PROJECT_DIR}' ..."
cd ${PROJECT_DIR}/scripts
./deploy.sh >> $LOG_FILE
if [[ $? -ne 0 ]]; then
  echo "${DATE_STR} - ERROR during deployment" >> $LOG_FILE
  echo "${DATE_STR} - ERROR during deployment"

  # Send email
  #sendmail benoit.lavenier@e-is.pro < ${PROJECT_DIR}/scripts/error-email.txt
  exit 1
fi

echo "${DATE_STR} - Successfully deployed" >> $LOG_FILE
echo "${DATE_STR} - Successfully deployed"


