# En direct du Convoi de la Liberté !   

<img src="/gallery/2022-02-08/thumbnail/2022-02-08-4_5767294055142656885.min.jpg" class="invisible">

Le Convoi de la Liberté est déjà en route, au départ de plusieurs villes de France (et d'Europe).
Rien n'arrêtera ce vent de liberté ! Les médias s'affolent et commencent l'inévitable campagne de dénigrement contre le mouvement.
Mais qu'importe : les choses bougent ! 

> Cette page **sera mise à jour** régulièrement. <br/>
> Pour nous envoyer vos photos/vidéos, contactez **Benoit** - 06.62.86.37.82

Reportage en images, **à partager pour réinformer** autour de vous : 

<p>
@@include('../../gallery/2022-02-08.html')
</p>


## La presse en parle

- France Soir : [À Paris et Fontainebleau, le "Convoi de la liberté" se heurte à la sévérité des "convois policiers"](https://www.francesoir.fr/politique-france/paris-le-convoi-de-la-liberte-lordre-et-la-securite) ;
- Le Parisien : [Reportage vidéo](https://fb.watch/babtO9dRu_/) (avec des Mayennais !) ;
- Ouest-France : [« Convoi de la liberté » en Mayenne : 300 véhicules de passage près de Laval](https://www.ouest-france.fr/sante/virus/coronavirus/convoi-de-la-liberte-en-mayenne-des-dizaines-de-vehicules-de-passage-a-louverne-ebb4a378-8b24-11ec-88ed-2ead809b0816) ;
- Courrier de la Mayenne : [Le convoi de la liberté à Laval : environ 300 véhicules sur la rocade](https://actu.fr/pays-de-la-loire/laval_53130/le-convoi-de-la-liberte-a-laval-environ-300-vehicules-sur-la-rocade_48631451.html) ;
- France Bleu Mayenne : [#ConvoidelaLiberte est applaudi par des piétons au bord de la route](https://twitter.com/bleumayenne/status/1492089003104944128).

## Sur les réseaux

Sur <i class="bi-twitter"></i> Twitter :
- [#ConvoyFrance](https://twitter.com/hashtag/ConvoyFrance)
- [#ConvoiFrance2022](https://twitter.com/hashtag/ConvoiFrance2022)
- [#ConvoiDeLaLiberté](https://twitter.com/hashtag/ConvoiDeLaLiberté)

Sur <i class="bi-facebook"></i> Facebook :
- [Convoy France Officiel](https://www.facebook.com/Convoy-France-Officiel-Affili%C3%A9-Convoy-Europe-110034208259203)

