# Rassemblement du 07/08 à Laval

Organisée par Les bagnard de la république, cette manifestation avait pour objectif :
- de dire NON au passe sanitaire;
- de préserver la liberté vaccinale, et exiger la sauvegarde des libertés individuelles fondamentales;

<p>
@@include('../../gallery/2021-08-07.html')
</p>

## Articles de presse

- Ouest-France : ["Quatrième manifestation contre le passe sanitaire"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-une-quatrieme-manifestation-contre-le-passe-sanitaire-ce-samedi-616af978-fb6a-11eb-aafc-574538136874)
- France Bleu : ["1.000 opposants dans la rue à Laval, prêts à "faire une croix" sur les lieux concernés"](https://www.francebleu.fr/infos/societe/troisieme-samedi-manifestation-pass-sanitaire-laval-1628337821)

## Sur les réseaux

- Twitter : 
  * [#Manifs7aout #Laval](https://twitter.com/search?q=%23Laval%23manifs7aout)
