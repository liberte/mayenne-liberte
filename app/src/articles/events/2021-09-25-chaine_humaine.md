# Rassemblement et chaine humaine du 25/09 à Laval 

La manifestation était organisée par le collectif [Mayenne Libre](https://www.mayennelibre.fr), le collectif [Mayenne Défense des Libertés](https://www.defensedeslibertes53.fr)
et l'association [Bon Sens](http://www.bonsens.info/).

Après un cortège et un sit-in sur le cours de la résistance, 
les participants ont formé **une chaine humaine** autour de la place du 11 novembre,
pour demander l'armistice de la guerre contre les libertés !


## Articles de presse

- Ouest-France : ["Une nouvelle manifestation contre le passe sanitaire"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-un-peu-moins-de-200-personnes-au-depart-de-la-manifestation-contre-le-passe-sanitaire-4cd10640-1df4-11ec-8e5d-e7ddd3d74444)
- Reportage photos du [blog de Joël Douillet](http://nouvellesducoin.free.fr/index.php/2021/09/28/bonjour-tout-le-monde/)

## Suivez-nous sur les réseaux

Facebook : https://www.facebook.com/groups/1853356128159103/

Twitter:
- [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)
