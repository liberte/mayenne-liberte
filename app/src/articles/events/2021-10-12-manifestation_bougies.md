# Vidéos et photos de la veillée aux bougies du 12/10 à Laval 

La manifestation été organisée par des bénévoles du collectif, en lien avec RéinfoCovid 53.
Merci au 250 personnes (et autant de bougies alumées !) qui nous ont rejoints !

<img src="/gallery/2021-10-12/20211012_194318.jpg">

L'enjeu maintenant, comme nous l'a proposé le Caitaine Alexandre Juving-Brunet, est de passer à l'action,
pour aller à la rencontre de nos députés. Un **compte-rendu** des informations passées dans la soirée a été rédigé : **A diffuser largement !**
<p class="text-center">
<a type="button" class="btn btn-primary" href="/articles/forum/2021-10-14-compte-rendu-des-informations-diffusees-rassemblement-du-mardi-12-10-106.html">Voir le compte-rendu</a>
</p>

<p>
@@include('../../gallery/2021-10-12.html')
</p>

## Articles de presse

- Ouest-France : ["Près de 200 personnes réunies contre le passe sanitaire"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-pres-de-200-personnes-reunies-contre-le-passe-sanitaire-e138cf3a-2b86-11ec-bfa1-8289e5d95d8d)

## Suivez-nous sur les réseaux

Facebook : https://www.facebook.com/groups/1853356128159103/

Telegram : https://t.me/joinchat/MX8Xr62UxFgzNzg0

Twitter:
- [#MayenneLiberte](https://twitter.com/search?q=%23MayenneLiberte)
- [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)
