# Guinguette du 21/08 à Laval

Sous la pluie, mais dans un ambiance bon enfant, nous avons pu pique-niquer, dialoguer, et écouter quelques groupes de musiciens.
Grand merci à tous pour vos engagements respectifs !

![Guinguette de la liberté !](../../gallery/2021-08-21/__affiche_guinguette_liberte.png)

<p>
@@include('../../gallery/2021-08-21.html')
</p>

## Articles de presse

- Ouest-France : ["Les manifestants anti-passe sanitaire organisent une guinguette au square de Boston"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-les-manifestants-anti-passe-sanitaire-organisent-une-guinguette-au-square-de-boston-ff7d0da6-025c-11ec-80e2-96b81cd115fd)

## Sur les réseaux

- Twitter : 
  * [#Manifestation21aout #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)
  * [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)


## Remerciements

_Tant de mercis à vous dire pour ce rassemblement ! Une demi guinguette peut-être mais le coeur y était.
Je pense que ça aurait pu être éclatant avec le soleil !_

_Merci à vous tous pour votre présence et les mots que vous êtes venus nous confier malgré cette météo !<br/>
Merci aux différents intervenants que j'ai trouvés passionnants : électromagnétisme et vaccin, santé, parents d'élèves, routier et femme de routier, engagements gilets jaunes, Catherine... la dame et son mari porteurs de tracs !<br/>
Merci à Benoit Lavenier pour sa collaboration dans l'organisation de cette journée.<br/>
Merci à Anthony Roueil pour le prêt des amplis.<br/>
Merci à Christophe le Caribouman pour ses précisions juridiques<br/>
Merci à Jean Claude de Bons sens<br/>
Merci à Olivier Pouteau pour ses précisions de réinfocovid<br/>
Merci à Irène Roy pour la Magnifique banderolle<br/>
Merci à Valfar Zerstörung et son groupe pour votre animation<br/>
Merci à Alban Hubert pour l'ouverture à la flûte traversière !<br/>
Merci aux jeunes femmes juste à côté de moi pour leurs chants sublimes pendant le pique nique.<br/>
Merci à Mireille pour ses idées en Coëvrons<br/>
Merci aux intervenants des autres collectifs<br/>
Merci à un magasin de bricolage en Coëvron Pour le don des banderoles publicitaires réformées.<br/>
Merci au Monsieur, joueur de tennis, pour le partage de son expérience personnelle en consultation.<br/>
Merci aux petites mains pour les petits noeuds des ballons et aux enfants pour l'éclatage !<br/>
Pensée à Cédric Jung des Bagnards pour son engagement et soutien<br/>
A tous ceux qui ont bravé la pluie, qui sont venus après....<br/>
Si jamais j'ai oublié de mentionner un nom, merci de me le faire rajouter._

_Le 28, Christophe le Caribouman prendra en charge un rassemblement sur Laval car tout le monde n'a pas envie d'aller à Paris et pour le 4....je crois que j'ai envie d'être près de vous.
Votre présence est importante, nous retrouver essentiel, la mobilisation demeure intacte malgré la pluie !
Nous avons eu la visite de Namoushka Djlady Nam , artiste engagée  qui pourrait partager sa vision et apporter son soutien lors d'un prochain rassemblement.
Bonne soirée à vous, je vous embrasse tous très chaleureusement comme il y avait longtemps qu'on ne pouvait plus le faire !_

<p style="text-align: end">Laurette</p>
