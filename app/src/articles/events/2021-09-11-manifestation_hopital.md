# Rassemblement du 11/09 à l'hôpital de Laval

Rassemblement citoyen, unis et luttant contre le Passe sanitaire et les mesures discriminatoires.
La manifestation était organisée par le collectif Mayenne Défense des Libertés.

<p>
@@include('../../gallery/2021-09-11.html')
</p>

## Articles de presse

- Ouest-France : ["Contre le passe sanitaire, 400 manifestants défilent dans les rues"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-contre-le-passe-sanitaire-250-manifestants-defilent-dans-les-rues-95d3b598-1301-11ec-834f-5e1c5d33e0a7) 
