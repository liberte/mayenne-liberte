# Rassemblement du sam. 8/01/22 à Laval

<img src="/gallery/2022-01-08/thumbnail/-5967283697294096714_121.jpg" class="invisible">

<i class="bi-megaphone-fill"></i> Manifestation organisée par le collectif, pour défendre nos droits et contre toutes les mesures liberticides covidistes.

Malgré un peu pluvieux, près de 500 personnes motivées pour dire NON au totalitarisme sanitaire. 
Encore et toujours, la bonne humeur était au rendez-vous !

La vaccination des enfants ; Le passe qui devient "vaccinal" ; 
L'appel à la haine assumé par notre président, etc.
**Les sujets ne manquaient pas...**

<p>
@@include('../../gallery/2022-01-08.html')
</p>

## Articles de presse

- France-Bleu Mayenne : ["480 personnes manifestent pour "emmerder" Emmanuel Macron à leur tour"](https://www.francebleu.fr/infos/sante-sciences/a-laval-480-personnes-manifestent-pour-emmerder-emmanuel-macron-a-leur-tour-1641679222?xtmc=laval%20passe%20vaccinal&xtnp=1&xtcr=3)
- Ouest-France : ["Près de 300 personnes manifestent contre le passe vaccinal"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-pres-de-300-personnes-manifestent-contre-le-passe-vaccinal-d5e15144-7077-11ec-8bf9-7d32b0fb299d)

## Visuels

- L'affiche du rassemblement :
<img src="/gallery/2022-01-08/2022-01-08-affiche_manif_liberte.jpg">
