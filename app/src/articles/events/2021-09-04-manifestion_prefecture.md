# Rassemblement du 04/09 à Laval

Co-organisée par plusieurs collectifs, cette manifestation avait pour objectif :
- de dire NON au passe sanitaire;
- de préserver la liberté vaccinale et exiger la sauvegarde des libertés individuelles fondamentales;

Le tout dans le cadre d'une action :
- **Citoyenne et apolitique** hors des partis politiques et des syndicats,
- **Absolument non-violente** : pas de provocation, dégradation de biens publics, pas d'insulte envers les personnes (même politiques), etc. ;

> Gardons notre détermination et notre cohésion.
> Rappelons-nous que si nous nous doutons ou sommes fatigués, il y a toujours l'écho, pas très loin qui nous crie : Liberté !

<p>
@@include('../../gallery/2021-09-04.html')
</p>

## Sur les réseaux

Twitter:
- [#AntiPassSanitaire #Laval](https://twitter.com/search?q=%23AntiPassSanitaire%20%23Laval)
- [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)
