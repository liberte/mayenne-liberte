# Rassemblement du 24/07 à Laval

Co-organisée par plusieurs collectifs, cette manifestation avait pour objectif :
- de dire NON au passe sanitaire;
- de préserver la liberté vaccinale, et exiger la sauvegarde des libertés individuelles fondamentales;

Le tout dans le cadre d'une action :
- **Citoyenne et apolitique** hors des partis politiques,
- **Absolument non-violente** : pas de provocation, dégradation de biens publics, pas d'insultes envers les personnes (même politiques), etc. ;

<a 
  href="https://cdn.radiofrance.fr/s3/cruiser-production/2021/07/ebef8b1b-5987-49bb-9adb-ba1eac2616bf/870x489_manif-pass-sanitaire.jpg"
  data-toggle="lightbox" data-gallery="2021-07-24">
  <img src="https://cdn.radiofrance.fr/s3/cruiser-production/2021/07/ebef8b1b-5987-49bb-9adb-ba1eac2616bf/870x489_manif-pass-sanitaire.jpg" class="dejavu dejavu-loaded"
    alt="Nous avons ont marché jusqu'à la place du 11-Novembre."
    class="img-fluid">
</a>

<p>
@@include('../../gallery/2021-07-24.html')
</p>

## Articles de presse

- Ouest-France (24/07/21): ["Environ 500 anti-passe sanitaire manifestent devant le théâtre"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-environ-200-anti-passe-sanitaire-manifestent-devant-le-theatre-2f376bda-ebcf-11eb-86fd-b30370d18cf3)
- France Bleu (24/07/21): ["C'est carrément une perte de liberté"](https://www.francebleu.fr/infos/societe/pass-sanitaire-a-laval-500-personnes-manifestent-pour-defendre-nos-libertes-1627137849)
- Reportage photos du [blog de Joël Douillet](http://joeldweb.free.fr/index.php/blog/le-passe-sanitaire-a-laval-ca-passe-pas/)

## Sur les réseaux

- Twitter :
  * [#AntiPassSanitaire #Mayenne](https://twitter.com/search?q=%23AntiPassSanitaire%20%23Mayenne)
  * [#AntiPassSanitaire #Laval](https://twitter.com/search?q=%23AntiPasssanitaire%20%23Laval)
  * [#24juillet #laval](https://twitter.com/search?q=%2324juillet%20%23laval)


- FaceBook :
  * [Collectif "Mayenne Défenses des libertés"](https://www.facebook.com/groups/1853356128159103)
