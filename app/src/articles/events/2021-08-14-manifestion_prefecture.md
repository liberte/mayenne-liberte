# Rassemblement du 14/08 à Laval

Co-organisée par plusieurs collectifs, cette manifestation avait pour objectif :
- de dire NON au passe sanitaire;
- de préserver la liberté vaccinale, et exiger la sauvegarde des libertés individuelles fondamentales;

Le tout dans le cadre d'une action :
- **Citoyenne et apolitique** hors des partis politiques,
- **Absolument non-violente** : pas de provocation, dégradation de biens publics, pas d'insultes envers les personnes (même politiques), etc. ;


<i>
Encore un moment qui me prend au trippes tant je suis remplie d'espérance en participant à cette mobilisation...nous voir, dans cette unité, cette complémentarité, ça ramène un courant d'humanité à ce monde qu'on veut nous imposer.... Bien entendu, nous n'avons pas forcément toutes les clefs, toutes les réponses à apporter aux drames que certains vivent...Nous gagnons du terrain, la guerre est aussi judiciaire... mais qu'est ce que ça fait du bien de retrouver ce contact... Dès que nous avons le lieu pour notre ginguette, nous vous le transmettrons ! En attendant, profitez de ce we, apaisez vos coups de soleil...Je vous embrasse tous ❤
Laurette.
</i>

<p>
@@include('../../gallery/2021-08-14.html')
</p>

## Articles de presse

- Ouest-France : ["Environ 500 manifestants contre le passe sanitaire"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-environ-500-manifestants-contre-le-passe-sanitaire-ce-samedi-c9c27c94-fcdc-11eb-91da-5a16e76c1509)
- France Bleu : 
  * Article : ["Environ 800 personnes ont défilé contre le pass sanitaire"](https://www.francebleu.fr/infos/societe/pass-sanitaire-les-opposants-un-peu-moins-nombreux-mais-determines-a-continuer-a-la-rentree-1628950846) 
  * Twitter : [Tweet #1](https://twitter.com/i/status/1426541579163082756), [Tweet #2](https://twitter.com/bleumayenne/status/1426540292149334017)
- Reportage photos du [blog de Joël Douillet](http://joeldweb.free.fr/index.php/blog/laval-14-aout-2021/)

## Sur les réseaux

- Twitter : 
  * [#Manifs14aout #Laval](https://twitter.com/search?q=%23Laval%20%23Manifs14aout)
