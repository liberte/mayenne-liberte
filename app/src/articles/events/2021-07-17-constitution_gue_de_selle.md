# Constitution du collectif au Gué-de-Selle

Mayenne défenses des libertés est un collectif citoyen, fondé le 17 juillet 2021 à l'appel de Laurette Josserand.

![image collectif Ouest-France](/gallery/2021-07-17/ouest-france.jpeg)
_1ère réunion (constitutive) du collectif, au Gué-de-Selle, le 17 juillet 2021 (Photo [Ouest-France](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/en-mayenne-un-collectif-cree-pour-la-defense-de-nos-libertes-notre-libre-arbitre-b58e20d0-e7dc-11eb-91f7-b0a903f6bddd))_

## Que demandons-nous ?

Nous demandons le rétablissement des libertés individuelles constitutionnelles.
Nous **refusons le passe sanitaire**, et demandons la liberté vaccinale.

Nous demandons le libre accès aux traitements existants efficaces contre le SARS-COV-2 (curatifs ou préventifs),
plutôt que la généralisation des pseudo-vaccins expérimentaux.

<p>
@@include('../../gallery/2021-07-17.html')
</p>

## Articles de presse

- Ouest-France (24/07/21): ["En Mayenne, un collectif créé pour « la défense de nos libertés, notre libre arbitre"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/en-mayenne-un-collectif-cree-pour-la-defense-de-nos-libertes-notre-libre-arbitre-b58e20d0-e7dc-11eb-91f7-b0a903f6bddd)
