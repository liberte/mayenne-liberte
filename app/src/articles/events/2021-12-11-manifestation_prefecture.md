# Rassemblement sam. 11/12 à Laval

<img src="/gallery/2021-12-11/thumbnail/267500437_3034892576759090_6490950047767358064_n.jpg" class="invisible">


<i class="bi-megaphone-fill"></i> Manifestation organisée par le collectif, pour défendre nos droits et contre toutes les mesures liberticides covidistes.

> Belle ambiance, grâce au programme bien préparé de notre équipe de choc : 
> Cortège animé en musique, concours de dessins, troc livres, café de la liberté musical !
> 
> Avec la présence de plusieurs pompiers suspendus, qui nous ont fait des démonstrations de gestes de sécurité. 


<p>
@@include('../../gallery/2021-12-11.html')
</p>

Voir aussi [notre petite vidéo >>](https://www.youtube.com/watch?v=n-7Ez4a68cQ) sur notre passage chez Mr Garot, député de la Mayenne. 

