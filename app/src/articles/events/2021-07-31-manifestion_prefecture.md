# Rassemblement du 31/07 à Laval

Co-organisée par plusieurs collectifs, cette manifestation avait pour objectif :
- de dire NON au passe sanitaire;
- de préserver la liberté vaccinale, et exiger la sauvegarde des libertés individuelles fondamentales;

Le tout dans le cadre d'une action :
- **Citoyenne et apolitique** hors des partis politiques,
- **Absolument non-violente** : pas de provocation, dégradation de biens publics, pas d'insultes envers les personnes (même politiques), etc. ;

<p>
@@include('../../gallery/2021-07-31.html')
</p>

## Articles de presse

- Ouest-France : ["Un millier d’opposants au passe sanitaire dans le centre-ville"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-1-000-personnes-rassemblees-a-une-nouvelle-manifestation-contre-le-passe-sanitaire-ce-samedi-4dcbeef2-f1e4-11eb-895f-5d269b933794)
- France Bleu : ["1 000 manifestants dans les rues de Laval pour défendre les libertés de tous"](https://www.francebleu.fr/infos/societe/pass-sanitaire-1-000-manifestants-dans-les-rues-de-laval-pour-defendre-les-libertes-de-tous-1627742532)

## Sur les réseaux

- Twitter :
  * [#Manifestation31juillet #Laval](https://twitter.com/search?q=%23Manifestation31juillet%20%23Laval)
