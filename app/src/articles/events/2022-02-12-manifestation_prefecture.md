# Rassemblement sam. 12 février 2022, à Laval

<img src="/gallery/2022-02-12/thumbnail/273652975_1129781734233592_4884072374155065653_n.jpg" class="invisible">

<i class="bi-megaphone-fill"></i> Manifestation organisée par le collectif, pour défendre nos droits et contre toutes les mesures liberticides covidistes.

Le passe-vaccinal anti-citoyen ; La vaccination des enfants ; Les effets secondaires des injections Anti-Covid qui explosent ;  
**Les sujets ne manquaient pas !**


<p>
@@include('../../gallery/2022-02-12.html')
</p>

## Infos sur les concerts solidaires

Dates de concerts solidaires et privés : 26.02 Rennes / 05.03 Argentré du Plessis /
13.03 Gorron / 09.04 Bonchamps les Laval / 28.05 Bonchamps les Laval

* Réservations possibles pour concerts des 26.02, 05.03 et 09.04 en envoyant vos
  coordonnées à lesclessolidairs@gmail.com

## Archive

<p class="text-center">
    <a type="button" class="btn btn-primary" href="/doc/2022-02-12-Flyer_manifestation_Mayenne_liberté.pdf" target="_blank"> 
      <i class="bi-file-pdf"></i> Télécharger le flyer (PDF)
    </a>
</p>

