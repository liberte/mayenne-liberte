# <i class="bi-truck"></i> Convoi de la Liberté : grande convergence pacifique vers Paris, puis Bruxelles !   

<img src="/doc/2022-02-11-convoy_france_logo.png" class="invisible">

Un Convoi de la Liberté est organisé, avec une convergence vers Paris puis (pour ceux qui souhaitent) vers Bruxelles.

> Les citoyens entendent récupérer :
> - Leur liberté
> - Leurs droits fondamentaux
> - L’accès inconditionnel aux soins, à l’éducation et à la culture
> - Le respect des valeurs essentielles de notre constitution
> - Stop au sacrifice des enfants et de la jeunesse, cessons la maltraitance subie quotidiennement !
> 
> **Le Canada a ouvert la voie. La convergence pour NOS ENFANTS et tous les citoyens, c'est maintenant !**

Il s'agit d'une **action pacifique**, ne visant pas à nuire à la population, mais seulement à dire STOP aux dérives actuelles.

La circulation du convoi se fera sur une seule voie, pour ne pas bloquer les automobilistes, mais cependant à vitesse réduite.
Aucun blocage total n'est prévu, bien que l'on ne puisse prédire ce qui se passera. Par exemple, l'arrivée à Paris le vendredi soir 
se fera davantage en Île-de-France, plutôt que directement sur Paris.

> Un mouvement citoyen, sans porte-parole "officiel".

Il n'y a **pas de porte-parole officiel au mouvement**. L'organisation du mouvement reste décentralisée 
et organisée directement par les citoyens eux-même, sans intermédiaire.


## Depuis la Mayenne

Sur notre département, nous proposons un départ **vendredi 11 février à 9h30** (et non plus 10h30) : RDV au péage de Louverné.
Nous rejoindrons ensuite le convoi Ouest, provenant de toute la Bretagne et des Pays-de-Loire.

> **Venez nous rejoindre** avec votre véhicule, ou simplement pour encourager le convoi, avec vos pancartes ou drapeaux.


Avant de partir, nous vous recommandons **de lire ces documents** :

<p class="text-center">
  <a type="button" class="btn btn-primary" href="/doc/2022-02-11-Charte_convoy_grand_ouest.pdf" target="_blank"> 
    <i class="bi-file-pdf"></i> Charte - Convoi Ouest
  </a>

  <a type="button" class="btn btn-secondary" href="/doc/2022-02-11-convoy_ouest_feuille_route.pdf" target="_blank"> 
    <i class="bi-file-pdf"></i> Feuille de route - Convoi Ouest
  </a>
</p>

Comment rester en contact ?
- Sur <i class="bi-telegram"></i> Telegram : https://t.me/+WbetDg2wqvdjNGRk
  > Note : Demandez **avant** de partager des documents sur le groupe, sinon vous risquez d'être exclu automatiquement. L'administrateur du groupe est `gaz`.

Voici la carte détaillée (prévisionnelle) :

[![Convoi de la Liberté - Grand-Ouest](/doc/2022-02-11-convoy_grand_ouest.jpg)](/doc/2022-02-11-convoy_grand_ouest.jpg)

Agenda (récap) :

- Vendredi 11/02, 9h30 : départ des Mayennais, du péage de Louverné ;
- Vendredi 11/02 Soir : arrivé à Paris de tous les convois ;
- Samedi 12/02 : Participation libre aux manifestations existantes sur Paris ;
- Dimanche : 
  * certains parlent d'aller à Bruxelles, mais est-ce la majorité ?<br/>
  * Nombreux sont ceux qui resteront sur Paris : chacun doit se sentir libre.

> Il est évident que les gouvernements en place préfèrent que rien ne soit bloqué, pour passer ce grand mouvement sous silence.
> 
> **Restons donc libre dans vos choix !** (Exemple : décision de participer ou non à des actions sur Paris, 
> de rester sur Paris plutôt que Bruxelles, etc.).


### Hébergement en région parisienne

Vous voulez venir, mais **vous ne savez pas où dormir** ? (vendredi soir ou samedi soir)

Plusieurs groupes d'entraide ont été créé (sur l'application Telegram) pour vous mettre en relation 
avec des résistants d'Île-De-France qui pourront vous héberger : 
 - https://t.me/entraideconvoyfranceofficiel
 - https://t.me/+Nip58C8W7EBhYmQ8

## Partout en France, les convois convergeront vers Paris

Toutes les régions de France convergeront, le vendredi 11/02 au soir, vers l'île-de-France :

![Carte Convoy France](/doc/2022-02-11-convoy_france_carte.jpg)

Pour en savoir plus :

<p class="text-center">
  <a type="button" class="btn btn-primary" href="/doc/2022-02-11-convoy_france_communiqué.pdf" target="_blank"> 
    <i class="bi-file-pdf"></i> Communiqué - Convoi France
  </a>
</p>

## Dans toute l'Europe !

Il en sera de même dans toutes les capitales d'Europe : un véritable raz de marée pour défendre nos libertés :

<div class="row">
  <div class="col">
   <a href="/doc/2022-02-11-convoy_europe_page1.jpg" target="_blank"><img src="/doc/2022-02-11-convoy_europe_page1.jpg" width="100%"></a>
  </div>
  <div class="col">
   <a href="/doc/2022-02-11-convoy_europe_page2.jpg" target="_blank"><img src="/doc/2022-02-11-convoy_europe_page2.jpg" width="100%"></a>
  </div>
</div>

Bien qu'une convergence vers Bruxelles soit proposé (les 13 et 14/02), nombreux sont les français qui resteront sur Paris.

> Chacun est libre de suivre sa route, suivant son cœur !

## Aux origines du mouvement : les _Truckers_ du Canada

Le mouvement s'inspire du mouvement pacifique des _Truckers_ au Canada : près de 50 000 camions ont convergé vers la capitale Ottawa,
pour y arriver le samedi 29 janvier. À ce jour, ils y sont encore et y installent même des cabanes en bois.

Quoi qu'en dise les médias (ou Justin Trudeau - cf vidéo ci-dessous) **le mouvement est resté pacifique et bon enfant**,
ne faisait pas de blocage complet, mais réveillant surtout un immense élan d'espérance et de soutien de la part de la population.

Plus d'un million de Canadiens étaient au rendez-vous à l'arrivée à Ottawa, par un froid de... -25°C !

<p>
@@include('../../gallery/2022-01-29.html')
</p>

Voir aussi :

<p class="text-center">
  <a type="button" class="btn btn-outline-secondary" href="https://www.youtube.com/watch?v=0NCfZ7uImEk" target="_blank"> 
    <i class="bi-youtube"></i> Court reportage en immersion (Canada) 
  </a>
</p>
