# Rassemblement sam. 06/11 à Laval

<img src="/gallery/2021-11-06/thumbnail/MjAyMTExYjEyNWE2OWM4YmFmYmMxYzU1MzRmYTBkOGE5ZTJiZTE.jpeg" class="invisible">


<i class="bi-megaphone-fill"></i> Manifestation organisée par le collectif, pour défendre nos droits et contre toutes les mesures liberticides covidistes.


# Article de presse

- Ouest-France : ["Une centaine de personnes manifeste contre la prolongation du passe sanitaire](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-une-centaine-de-personnes-manifeste-contre-la-prolongation-du-passe-sanitaire-cd3a1282-3f22-11ec-9acb-9d2356bf4b6b)
