# Rassemblement et Ateliers citoyens - S'informer, échanger, créer ! Samedi 19 mars à LAVAL

<img src="/img/ateliers_citoyens.jpeg" class="invisible">

<i class="bi-megaphone-fill"></i> Rassemblement organisée par le collectif, pour défendre nos droits et contre toutes les mesures liberticides covidistes.

Plutôt qu'une manifestation "classique", nous propons une nouvelle formule, avec :
- Présentation d'ateliers citoyens sur les diverses thématiques
  relatives aux libertés et à l'autonomie citoyenne.
- un moment de convivialité et d'échange, autour de quelques douceurs;

Horaire : 14h à 17h 

Lieu : Square de Boston, à Laval

![flyer](/gallery/2022-03-19/__flyer.jpg)

<p class="text-center">
    <a type="button" class="btn btn-primary" href="/doc/2022-03-19-flyer_ateliers_citoyen.pdf" target="_blank"> 
      <i class="bi-file-pdf"></i> Télécharger le flyer (PDF)
    </a>
</p>


## Ateliers thématiques

- **Juridique** : Informations sur la plainte tribunal de La Haye
- **Santé** : Réaliser une détoxification post vaccinale
- **Informations** : Etat des lieux et impacts post-vaccination statistiques
- **Autonomie monétaire** : Présentation de la [monnaie libre G1](https://www.monnaie-libre.fr)
- **Réseaux citoyens** : Informations sur le réseau Solaris



