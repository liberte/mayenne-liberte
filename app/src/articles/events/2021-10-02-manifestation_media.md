# Rassemblement devant les médias locaux du 02/10 à Laval 

La manifestation était organisée par le collectif [Mayenne Libre](https://www.mayennelibre.fr), le collectif [Mayenne Défense des Libertés](https://www.defensedeslibertes53.fr)
et l'association [Bon Sens](http://www.bonsens.info/).

Après un cortège et des prises de paroles  devant les locaux de Radio Bleu puis Ouest-France,
les participants ont pu déposer des articles de réinformation, ou leurs témoignages, à destination de nos médias locaux.

L'objectif était de réaffirmer leur responsabilité dans la bonne information de la population, claire et loyale, concernant
les mesures sanitaires liberticides et les nombreux risques liées à la vaccination.


## Articles de presse

- Ouest-France : ["Une centaine de personnes à la manifestation contre le passe sanitaire"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/laval-une-centaine-de-personnes-a-la-manifestation-contre-le-passe-sanitaire-e3b39546-239e-11ec-9ce4-38e6bbc25a47)
- Reportage photos du [blog de Joël Douillet](http://nouvellesducoin.free.fr/index.php/2021/10/02/mourir-peut-attendre/)

## Suivez-nous sur les réseaux

Facebook : https://www.facebook.com/groups/1853356128159103/

Twitter:
- [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)
