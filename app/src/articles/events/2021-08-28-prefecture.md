# Rassemblement du 28/08 à Laval

Rassemblement citoyen, unis et luttant contre le Passe sanitaire et les mesures discriminatoires.
La manifestation était organisée par l'[Association Morpheus Ethica](http://www.morpheus-ethica.com/).

<p>
@@include('../../gallery/2021-08-28.html')
</p>

## Articles de presse

- Ouest-France : ["Plusieurs centaines de personnes contre la passe sanitaire"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/a-laval-une-septieme-manifestation-anti-passe-sanitaire-s-est-elancee-b202fbe4-07e8-11ec-a841-cbabc780533d)
- France-Bleu : ["plus de 400 manifestants anti-pass sanitaire à Laval, ce samedi"](https://www.francebleu.fr/infos/sante-sciences/covid-19-plus-de-400-manifestants-anti-pass-sanitaire-a-laval-ce-samedi-1630173534)
- Reportage photos du [blog de Joël Douillet](http://nouvellesducoin.free.fr/index.php/2021/08/28/cinquieme-vague/)

## Sur les réseaux

Twitter:
- [#AntiPassSanitaire #Laval](https://twitter.com/search?q=%23AntiPassSanitaire%20%23Laval)
- [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval)
- [#Manifs28aout #Laval](https://twitter.com/search?q=%23Manifs28aout%20%23Laval)
