# Rassemblement du 18/09 à l'hôpital de Laval

Rassemblement citoyen, unis et luttant contre le Passe sanitaire et les mesures discriminatoires.
La manifestation était organisée par le collectif Mayenne Défense des Libertés.

<p>
@@include('../../gallery/2021-09-18.html')
</p>

## Articles de presse

- Ouest-France : ["Une nouvelle manifestation contre le passe sanitaire ce samedi"](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/a-laval-une-nouvelle-manifestation-contre-le-passe-sanitaire-ce-samedi-d3f38334-17d0-11ec-9f73-6fd91ee9f0dd) 

