# Bienvenue sur le forum!

<div class="article forum">
<h2>
<a name="qui-sommes-nous-1" class="anchor" href="#qui-sommes-nous-1"></a>Qui sommes nous ?</h2>
<p>Nous sommes des simples citoyens, mayennais, organisé au sein de collectifs défendant les libertés individuelles.</p>
<p>Nous demandons la sortie de l’état d’urgence sanitaire, et le rétablissement des libertés individuelles constitutionnelles. Nous refusons la mise en œuvre d’un passe sanitaire (« passeport vaccinal », « certificat vert numérique », etc.), comme de toute autre forme de dictature sanitaire et numérique. Nous demandons la fin de l’omerta sur les traitements existants efficaces contre le SARS-COV-2 (curatifs ou préventifs), au seul profit de la propagande vaccinale.</p>
<h2>
<a name="pourquoi-ce-forum-2" class="anchor" href="#pourquoi-ce-forum-2"></a>Pourquoi ce forum ?</h2>
<p>Ce forum est destiné à mieux nous organiser, en structurant nos échanges (ce que ne permettent pas de faire les réseaux sociaux).<br>
Cet outil est à disposition des différents groupes du collectif.</p>

<p class="text-end footer"><span class="text-muted">
<a class="text-muted" href="https://forum.mayennelibre.fr/t/bienvenue-sur-le-forum/7" target="_blank">Posté le 30/08/2021</a>
<span class="text-muted">
par <img class="avatar avatar-16" src="https://forum.mayennelibre.fr/uploads/default/original/1X/da5f6f808e1395c536307a96d5d7a34409e23a47.png"> Admin
 |
</span>
<a class="text-info" href="https://forum.mayennelibre.fr/t/bienvenue-sur-le-forum/7" target="_blank">Voir la discussion >></a>
</p>

</div>
