# Compte-rendu des informations diffusées - Rassemblement du mardi 12/10

<div class="article forum">
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/66e279680283793fa9b882699ea39f33f65994bf.jpeg" data-download-href="https://forum.mayennelibre.fr/uploads/default/66e279680283793fa9b882699ea39f33f65994bf" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/66e279680283793fa9b882699ea39f33f65994bf_2_690x388.jpeg" alt="image" data-base62-sha1="eG9V5BPsIQUNTQQu6KpSHquP2gf" width="690" height="388" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/66e279680283793fa9b882699ea39f33f65994bf_2_690x388.jpeg, https://forum.mayennelibre.fr/uploads/default/optimized/1X/66e279680283793fa9b882699ea39f33f65994bf_2_1035x582.jpeg 1.5x, https://forum.mayennelibre.fr/uploads/default/optimized/1X/66e279680283793fa9b882699ea39f33f65994bf_2_1380x776.jpeg 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/66e279680283793fa9b882699ea39f33f65994bf_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1920×1080 417 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>Merci à tous d’être venus si nombreux au RDV ! Voici un compte-rendu de toutes les informations transmises mardi soir, le 12/10, à Laval.</p>
<blockquote>
<p>Pour télécharger ce compte-rendu : <a href="https://www.mayenne-liberte.fr/doc/2021-10-13-Compte-rendu-rassemblement-Laval-12-10.pdf">Télécharger (PDF) &gt;&gt;</a></p>
</blockquote>
<blockquote>
<p>/!\ NB1 : <strong>Je ne suis pas juriste, ni avocate, les éléments concernant la lecture des textes de loi communiqués ci-dessous ne sont que la lecture des informations indiquées dans les textes de lois.</strong></p>
<p><strong>Ces informations sont données en date du 13/10 : le décret évolue très fréquemment, n’hésitez pas à le consulter régulièrement pour les paragraphes qui vous impactent pour suivre les éventuels changements !!!</strong></p>
<p>/!\ NB2 : <strong>les informations communiquées ci-dessous ne sont pas garanties de résultats. Il s’agit là de pistes de travail</strong>.</p>
</blockquote>
<h1>
<a name="1-se-renseigner-sur-la-loi-et-les-dcrets-1" class="anchor" href="#1-se-renseigner-sur-la-loi-et-les-dcrets-1"></a>1. Se renseigner sur la loi et les décrets</h1>
<p>Allez jeter un petit coup d’œil sur les textes de lois (<em>oui je sais ce n’est pas divertissant</em> …mais pour se défendre il est important de connaître ses droits <img src="https://forum.mayennelibre.fr/images/emoji/twitter/wink.png?v=10" title=":wink:" class="emoji" alt=":wink:"> )</p>
<h2>
<a name="ou-trouvez-les-textes-de-lois-2" class="anchor" href="#ou-trouvez-les-textes-de-lois-2"></a>Ou trouvez les textes de lois ?</h2>
<p>Sur le site de Legifrance (site de l’état) : <a href="https://www.legifrance.gouv.fr/">https://www.legifrance.gouv.fr/</a> vous trouverez toutes les lois et décrets en accès gratuit.</p>
<ul>
<li>
<p>Pour la loi : vous tapez dans la barre de recherche : loi 2021-1040 du 5 août 2021 : <a href="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000043909676/?isSuggest=true">https://www.legifrance.gouv.fr/loda/id/JORFTEXT000043909676/?isSuggest=true</a></p>
</li>
<li>
<p>Pour le décret : le décret principal est «Décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire »<br>
<a href="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000043575238/?isSuggest=true">https://www.legifrance.gouv.fr/loda/id/JORFTEXT000043575238/?isSuggest=true</a></p>
</li>
</ul>
<p>Vous aurez alors accès au décret mis à jour à la date de votre consultation<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc.png" data-download-href="https://forum.mayennelibre.fr/uploads/default/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc_2_690x387.png" alt="image" data-base62-sha1="odjrwWSzV1ccgphq2ZCbceVRLHK" width="690" height="387" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc_2_690x387.png, https://forum.mayennelibre.fr/uploads/default/optimized/1X/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc_2_1035x580.png 1.5x, https://forum.mayennelibre.fr/uploads/default/original/1X/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc.png 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/a9b572cecbcff8af4cd0d61dab2a2f2fb1defcfc_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1366×768 129 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<p>Les articles du décret peuvent être mis à jour par d’autres décrets…</p>
<ul>
<li>Exemple ci-dessous :<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/5dfced89ac159c095d8b21d10cb8d7178c71f6a8.png" data-download-href="https://forum.mayennelibre.fr/uploads/default/5dfced89ac159c095d8b21d10cb8d7178c71f6a8" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/original/1X/5dfced89ac159c095d8b21d10cb8d7178c71f6a8.png" alt="image" data-base62-sha1="dpsiC7wdKQfcNpTwzyUj5t8Ea4E" width="690" height="120" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/5dfced89ac159c095d8b21d10cb8d7178c71f6a8_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1074×187 8.11 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div>
</li>
</ul>
<h1>
<a name="2-information-relative-au-comit-de-salut-du-peuple-csp-3" class="anchor" href="#2-information-relative-au-comit-de-salut-du-peuple-csp-3"></a>2. Information relative au « Comité de salut du peuple » (CSP)</h1>
<p>Organisation (apolitique et citoyenne) menée par le Capitaine Alexandre Juving-Brunet.<br>
Pour obtenir les informations sur les CSP <a href="https://comitedesalutpublic.fr/">https://comitedesalutpublic.fr/</a></p>
<p>L’idée est donc de monter des groupes citoyens volontaires pour maintenir le contact régulier avec nos députés et les solliciter sur leurs positions vis-à-vis de la gestion de la crise.</p>
<ul>
<li>Pour connaître vos députés : <a href="https://www.nosdeputes.fr/circonscription/departement/Mayenne" class="inline-onebox">Mayenne (53) : Liste des députés par circonscription - NosDéputés.fr</a>
</li>
</ul>
<div class="md-table">
<table>
<thead>
<tr>
<th>N° CSP</th>
<th>code insee</th>
<th>ID dept</th>
<th>Nom dept</th>
<th>Député</th>
<th>Pop.</th>
<th>Présence ou création en cours d’un CSP ?</th>
</tr>
</thead>
<tbody>
<tr>
<td>247</td>
<td>5301</td>
<td>53</td>
<td>Mayenne</td>
<td>Mr Guillaume Garot</td>
<td>106.339</td>
<td>Oui référente Lydie Bosse</td>
</tr>
<tr>
<td>248</td>
<td>5302</td>
<td>53</td>
<td>Mayenne</td>
<td>Mme Géraldine Bannier</td>
<td>110.019</td>
<td>A créer</td>
</tr>
<tr>
<td>249</td>
<td>5303</td>
<td>53</td>
<td>Mayenne</td>
<td>Mr Yannick Favennec-Becot</td>
<td>101.737</td>
<td>A créer</td>
</tr>
</tbody>
</table>
</div><p>Nous avons initié le premier CSP (n° 247) pour la première circo de Mayenne. Si des personnes souhaitent nous rejoindre pour ce CSP ou même en créer d’autres pour les 2 autres circo n’hésitez pas à me contacter par mail à <a href="mailto:bosse.lylie@gmail.com">bosse.lylie@gmail.com</a> en indiquant dans le titre de votre mail « <strong>Information CSP</strong> ». Je pourrais également essayer de me rendre disponible pour aller voir vos députés avec vous si vous avez besoin d’un argumentaire « scientifique ».</p>
<p>Liste des CSP nationaux mis à jour sur le site : <a href="https://comitedesalutpublic.fr/actions13f0db9e" class="inline-onebox">Actions</a> avec la carte de visualisation des créations de csp.</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/3236581e4039bbdebfc9d9d9581a792447eebd67.jpeg" data-download-href="https://forum.mayennelibre.fr/uploads/default/3236581e4039bbdebfc9d9d9581a792447eebd67" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/3236581e4039bbdebfc9d9d9581a792447eebd67_2_690x387.jpeg" alt="image" data-base62-sha1="7aciWbo0scKyqCtekwKMd0Qa2lp" width="690" height="387" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/3236581e4039bbdebfc9d9d9581a792447eebd67_2_690x387.jpeg, https://forum.mayennelibre.fr/uploads/default/optimized/1X/3236581e4039bbdebfc9d9d9581a792447eebd67_2_1035x580.jpeg 1.5x, https://forum.mayennelibre.fr/uploads/default/original/1X/3236581e4039bbdebfc9d9d9581a792447eebd67.jpeg 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/3236581e4039bbdebfc9d9d9581a792447eebd67_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1366×768 242 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<h1>
<a name="3informations-sur-les-protocoles-de-test-de-dpistage-4" class="anchor" href="#3informations-sur-les-protocoles-de-test-de-dpistage-4"></a>3.Informations sur les protocoles de test de dépistage.</h1>
<p><strong>Ce que dit le décret :</strong></p>
<ul>
<li>L’article 2-2 / 1° précise la méthode d’analyse mais non la méthode de prélèvement (au 13/10 à surveiller qu’il ne le change pas …)<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40.png" data-download-href="https://forum.mayennelibre.fr/uploads/default/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40_2_690x387.png" alt="image" data-base62-sha1="aYVSVCXCGmTga1O4RuepCBllwfm" width="690" height="387" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40_2_690x387.png, https://forum.mayennelibre.fr/uploads/default/optimized/1X/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40_2_1035x580.png 1.5x, https://forum.mayennelibre.fr/uploads/default/original/1X/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40.png 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/4cf90e1e89ce0b4353a0872e99ee1edd86b84e40_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1366×768 119 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div>
</li>
</ul>
<p>Pour connaître les laboratoires réalisant les tests : <a href="https://www.sante.fr/cf/centres-depistage-covid/departement-53-mayenne.html" class="inline-onebox">Santé.fr | Mayenne: centres et lieux de dépistage en RT-PCR et tests antigéniques de la Covid-19</a> (site du gouvernement)</p>
<ul>
<li>
<p>Choisissez votre département<br>
(ici j’illustre l’exemple pour la Mayenne mais cela fonctionne pour tous les départements)</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/605d3076c5e97487a9d76bd446b1b86739d336a0.png" data-download-href="https://forum.mayennelibre.fr/uploads/default/605d3076c5e97487a9d76bd446b1b86739d336a0" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/605d3076c5e97487a9d76bd446b1b86739d336a0_2_690x387.png" alt="image" data-base62-sha1="dKtuEg0FynJfkQ5KPjN3C0gNec8" width="690" height="387" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/605d3076c5e97487a9d76bd446b1b86739d336a0_2_690x387.png, https://forum.mayennelibre.fr/uploads/default/optimized/1X/605d3076c5e97487a9d76bd446b1b86739d336a0_2_1035x580.png 1.5x, https://forum.mayennelibre.fr/uploads/default/original/1X/605d3076c5e97487a9d76bd446b1b86739d336a0.png 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/605d3076c5e97487a9d76bd446b1b86739d336a0_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1366×768 112 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
<li>
<p>Contactez les laboratoires pour savoir s’ils pratiquent les prélèvements salivaires (ou oropharyngés)</p>
</li>
<li>
<p>Demandez les conditions d’accès (est-ce uniquement pour les enfants ?)</p>
</li>
<li>
<p>Leur communiquer que vous souhaitez un test salivaire</p>
<ul>
<li>
<p>On vous l’accorde : Bingo !</p>
</li>
<li>
<p>On vous le refuse : envoyer le modèle de courrier (transmis en pièce-jointe) en lettre avec accusé de réception et bien stipuler dans votre courrier que vous souhaitez une réponse écrite et que l’on vous argumente avec les textes de loi en quoi le test salivaire n’est réservé qu’aux enfants (pour avoir une trace)</p>
</li>
</ul>
</li>
</ul>
<p>(Attention cela est indépendant du projet de loi de rendre les tests payants …)</p>
<h1>
<a name="4-information-relative-aux-bibliothquesmdiathques-5" class="anchor" href="#4-information-relative-aux-bibliothquesmdiathques-5"></a>4. Information relative aux bibliothèques/médiathèques</h1>
<p><strong>Ce que dit le décret :</strong></p>
<ul>
<li>
<p>Décret 2021-699 version en vigueur – chapitre 7 – article 47-1 II / k</p>
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/84fdd166d9ad20e6e3df07932a3c025b4108c824.png" data-download-href="https://forum.mayennelibre.fr/uploads/default/84fdd166d9ad20e6e3df07932a3c025b4108c824" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/84fdd166d9ad20e6e3df07932a3c025b4108c824_2_690x387.png" alt="image" data-base62-sha1="iYuPn9xTrkM3y6npg74aRZiBc9K" width="690" height="387" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/84fdd166d9ad20e6e3df07932a3c025b4108c824_2_690x387.png, https://forum.mayennelibre.fr/uploads/default/optimized/1X/84fdd166d9ad20e6e3df07932a3c025b4108c824_2_1035x580.png 1.5x, https://forum.mayennelibre.fr/uploads/default/original/1X/84fdd166d9ad20e6e3df07932a3c025b4108c824.png 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/84fdd166d9ad20e6e3df07932a3c025b4108c824_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1366×768 118 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
</li>
</ul>
<p>Le décret indique qu’il est possible d’aller en bibliothèque à des fins professionnelles <strong>OU</strong> pour des recherches. Vous pouvez donc tenter de vous rendre en bibliothèque pour vous ou vos enfants en indiquant avoir besoin de faire une recherche (pour une association, pour un exposé, pour votre travail …).</p>
<p>Allez en discuter avec les personnes de votre bibliothèque en expliquant que vous avez lu le décret et que vous vous interrogez sur les conditions d’application (avec courtoisie et le sourire) :</p>
<ul>
<li>
<p>On vous laisse rentrer : Re - Bingo !</p>
</li>
<li>
<p>On vous le refuse : envoyez un courrier transmis en lettre avec accusé de réception à la mairie de votre commune et bien stipuler dans votre courrier que vous souhaitez une réponse écrite et que l’on vous argumente avec les textes de loi en quoi on vous refuse l’accès car vous avez besoin d’accéder pour faire des recherches et ajouter l’extrait du texte de loi (pour avoir une trace)</p>
</li>
</ul>
<h1>
<a name="5-information-relative-aux-pratiques-sportives-6" class="anchor" href="#5-information-relative-aux-pratiques-sportives-6"></a>5. Information relative aux pratiques sportives</h1>
<p><strong>Ce que dit le décret :</strong></p>
<ul>
<li>Décret 2021-699 version en vigueur – chapitre 7 – article 47-1 II / g ou h<br>
<div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/416078980f9c6cc46a5b68153f0f4b741b417994.png" data-download-href="https://forum.mayennelibre.fr/uploads/default/416078980f9c6cc46a5b68153f0f4b741b417994" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/optimized/1X/416078980f9c6cc46a5b68153f0f4b741b417994_2_690x387.png" alt="image" data-base62-sha1="9klISEcWr3U9wc2Jk6q1ZFYwRus" width="690" height="387" srcset="https://forum.mayennelibre.fr/uploads/default/optimized/1X/416078980f9c6cc46a5b68153f0f4b741b417994_2_690x387.png, https://forum.mayennelibre.fr/uploads/default/optimized/1X/416078980f9c6cc46a5b68153f0f4b741b417994_2_1035x580.png 1.5x, https://forum.mayennelibre.fr/uploads/default/original/1X/416078980f9c6cc46a5b68153f0f4b741b417994.png 2x" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/416078980f9c6cc46a5b68153f0f4b741b417994_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">1366×768 118 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div>
</li>
</ul>
<p>Le texte de loi indique donc que le pass sanitaire s’applique aux établissements <strong>faisant habituellement l’objet d’un contrôle</strong> (carte d’accès, accès avec réservation … )<br>
Exemple :</p>
<ul>
<li>Parc des princes, théâtre, cinéma, parc d’attraction, musée … → habituellement contrôle d’accès → pass sanitaire</li>
<li>Salle de sport de « trifouilli les oies » pour l’entraînement de basket  pas de contrôle d’accès habituellement → pas de pass</li>
</ul>
<p>Pour connaître les établissements en fonction du type : <a href="https://www.service-public.fr/professionnels-entreprises/vosdroits/F32351" class="inline-onebox">Définition d'un établissement recevant du public (ERP) - professionnels | service-public.fr</a></p>
<p>Pour le type X = « Établissement sportif clos et couvert, salle omnisports, patinoire, manège, piscine couverte, transformable ou mixte. Salle polyvalente sportive de moins de 1 200 m² ou d’une hauteur sous plafond de plus de 6,50 m »</p>
<blockquote>
<p><strong>Pour rappel :</strong> <strong>Le pass sanitaire n’est applicable que pour les enfants ayant 12 ans et</strong> <strong>2 mois</strong></p>
</blockquote>
<h1>
<a name="6-solidarit-pour-le-sport-7" class="anchor" href="#6-solidarit-pour-le-sport-7"></a>6. Solidarité pour le sport</h1>
<p>Si faute de pass vous ne pouvez plus pratiquer votre sport, faites appel à la force des groupes locaux.</p>
<p>Pour exemple un groupe sport a été créé sur facebook pour Laval, Louverné, Changé (lien <a href="https://www.facebook.com/groups/4125028927592814" class="inline-onebox">Groupes Facebook</a> ) pour permettre aux gens qui le souhaitent de se rejoindre pour organiser des séances de sport en groupe. Si vous êtes d’une autre zone géographique vous pouvez vous aussi créer votre propre groupe.</p>
<p>Vous pouvez organiser des sorties vélo, marche nordique, renforcement cardio, fitness, basket, foot …</p>
<p>Si vous êtes coach sportif, entraîneur, passionné de sport et que vous voulez aidez vos concitoyens vous pouvez me contacter par mail à <a href="mailto:bosse.lylie@gmail.com">bosse.lylie@gmail.com</a> en indiquant dans votre titre de mail « <strong>Le sport pour tous</strong> ».</p>
<p>Pour exemple dimanche dernier à Louverné, 20 personnes (adultes et enfants) ont pu participer à une séance de sport collective (renfo – cardio), basket et foot.</p>
<p>Rejoignez-nous ! Ne restez pas seul.</p>
<h1>
<a name="7-information-relative-aux-vaccins-8" class="anchor" href="#7-information-relative-aux-vaccins-8"></a>7. Information relative aux vaccins</h1>
<p>Pour ceux qui souhaitent une information sur les vaccins, je vais organiser des réunions en visio pour expliquer les notions générales à connaître (ex quels sont les différents types de vaccins, inactivé, atténué, ARN …quels sont les modes de fonctionnement ? …)</p>
<p>Si vous êtes intéressés vous pouvez :</p>
<ul>
<li>m’envoyer un mail à <a href="mailto:bosse.lylie@gmail.com">bosse.lylie@gmail.com</a> en indiquant dans le titre « Information vaccin ».</li>
</ul>
<h1>
<a name="8-les-rseaux-dentraide-9" class="anchor" href="#8-les-rseaux-dentraide-9"></a>8. Les réseaux d’entraide</h1>
<p>Ne restez pas seul : si vous avez besoin d’aide publier un message sur ces réseaux :</p>
<blockquote>
<p>Il y en a plein de groupes n’hésitez pas à trouver celui qui vous aider. Par exemple :</p>
<p><strong>« Je suis la commune de … et je recherche des personnes volontaires pour venir m’aider à …   Pourriez-vous m’aider ? »</strong><br>
(ex: aller voir le maire de ma commune, le responsable des associations, le responsable de la bibliothèque …)</p>
</blockquote>
<ul>
<li>
<p>Sur notre site : <a href="https://www.mayenne-liberte.fr/pages/agir" class="inline-onebox">Collectif Mayenne Liberté</a> <strong>vous trouverez tous les liens</strong> vers les différents groupes (Telegram) vous y trouverez également les agendas des évènements.</p>
</li>
<li>
<p>Sur Facebook : « Collectif Mayenne Défense des libertés » : <a href="https://www.facebook.com/groups/1853356128159103" class="inline-onebox">Groupes Facebook</a></p>
</li>
<li>
<p>Sur Télégram (application gratuite, installable sur téléphone ou ordinateur)</p>
</li>
</ul>
<h2>
<a name="groupes-tlgram-10" class="anchor" href="#groupes-tlgram-10"></a>Groupes Télégram</h2>
<ul>
<li>
<p>Groupe d’accueil : <a href="https://t.me/joinchat/MX8Xr62UxFgzNzg0" class="inline-onebox">Telegram: Join Group Chat</a></p>
</li>
<li>
<p>Groupes locaux :</p>
</li>
</ul>
<div class="md-table">
<table>
<thead>
<tr>
<th><strong>Communauté de communes</strong></th>
<th><strong>Groupe Telegram</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Avaloir</td>
<td><a href="https://t.me/joinchat/zrdZcsxh-CY4NTZk" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Bocage Mayennais</td>
<td><a href="https://t.me/joinchat/P6H5SfnswnRmNjc8" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Coëvron</td>
<td><a href="https://t.me/joinchat/zG35BZuwoMRiMDBk" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Ernée</td>
<td><a href="https://t.me/joinchat/PsWzf4dU-hAyMjA0" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Laval Agglo:</td>
<td>(« RSA Laval » - groupe historique) : <a href="https://t.me/joinchat/LerguxhXPyNLSgz24Qc_cw" class="inline-onebox">Telegram: Join Group Chat</a>
</td>
</tr>
<tr>
<td>Lassay-Le Horps</td>
<td><a href="https://t.me/joinchat/oL_pYl8EP3AyYzdk" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Pays de Chateau-Gontier</td>
<td><a href="https://t.me/joinchat/ghQDmdKUsdszMTE0" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Pays de Craon</td>
<td><a href="https://t.me/joinchat/rDZu51dfvMo3OGE8" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Pays de Loiron</td>
<td><a href="https://t.me/joinchat/VfncMISI5CQwMzJk" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Pays de Mayenne</td>
<td><a href="https://t.me/joinchat/Mcc_bVWmVCZkYzU0" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Pays de Meslay-Grez</td>
<td><a href="https://t.me/joinchat/fYR9E-iW7l03NWVk" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
</tbody>
</table>
</div><ul>
<li>Groupes par profession :</li>
</ul>
<div class="md-table">
<table>
<thead>
<tr>
<th><strong>Profession (salariée)</strong></th>
<th><strong>Groupe Telegram</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Médical, Santé</td>
<td><a href="https://t.me/joinchat/Z7Zcu02CcFQyMTQ0" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Education nationale</td>
<td><a href="https://t.me/joinchat/4q51Xj2vSVs5MWM0" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Autres salariés</td>
<td><a href="https://t.me/joinchat/QLz3ylScHw9jYzU0" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
</tbody>
</table>
</div><div class="md-table">
<table>
<thead>
<tr>
<th><strong>Profession (libérale)</strong></th>
<th><strong>Groupe Telegram</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Artisans, auto-entrepreneurs, exploitants agricoles, autres situations</td>
<td><a href="https://t.me/joinchat/1lCJEXSANIk2YWE8" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
</tbody>
</table>
</div><ul>
<li>
<p>Groupe thématique :</p>
<blockquote>
<p><strong>Les groupes thématiques sont des espaces de réflexion</strong> sur un domaine précis. Ils ont pour mission d’aider les groupes locaux dans leurs actions, en leur <strong>proposant des outils concrets</strong> (modèles de documents, procédures, affiches, tracts, etc.). Ils sont en lien avec les collectifs nationaux, pour valoriser les ressources déjà existantes. Ces groupes peuvent aussi <strong>proposer des actions communes</strong> à l’échelle du département.</p>
</blockquote>
</li>
</ul>
<div class="md-table">
<table>
<thead>
<tr>
<th><strong>Thématique</strong></th>
<th><strong>Description</strong></th>
<th><strong>Groupe Telegam</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Parents d’élèves</td>
<td>Vous avez des enfants scolarisés ? Vous avez des questions concernant les vaccins ? <a href="https://www.mayenne-liberte.fr/pages/parents">En savoir plus &gt;&gt;</a>
</td>
<td><a href="https://t.me/joinchat/9lrEkXUDHk5kNjI8" class="inline-onebox">Telegram: Join Group Chat</a></td>
</tr>
<tr>
<td>Juridique</td>
<td>Vous avez des questions juridiques, ou souhaitez participer à une plainte collective ? <a href="https://www.mayenne-liberte.fr/pages/juridique">En savoir plus &gt;&gt;</a>
</td>
<td></td>
</tr>
<tr>
<td>Santé</td>
<td>Vous êtes malade de la Covid-19 ? Vous avez reçu un vaccin « génétique » et voulez en limiter les effets négatifs ? <a href="https://www.mayenne-liberte.fr/pages/sante">En savoir plus &gt;&gt;</a>
</td>
<td></td>
</tr>
<tr>
<td>Victimes des vaccins</td>
<td>Vous êtes victime d’effet indésirable, suite à une injection de vaccin Covid-19 ? Un de proche est décédé brusquement suite à une injection ? <a href="https://www.mayenne-liberte.fr/pages/juridique">En savoir plus &gt;&gt;</a>
</td>
<td></td>
</tr>
</tbody>
</table>
</div><h2>
<a name="me-joindre-11" class="anchor" href="#me-joindre-11"></a>Me joindre</h2>
<p>Pour ma part (Lydie) je publie sur les réseaux télégram :</p>
<ul>
<li>Parents d’élèves : <a href="https://t.me/joinchat/9lrEkXUDHk5kNjI8" class="inline-onebox">Telegram: Join Group Chat</a>
</li>
<li>Groupe RSA Laval : <a href="https://t.me/joinchat/LerguxhXPyNLSgz24Qc_cw" class="inline-onebox">Telegram: Join Group Chat</a>
</li>
<li>Le groupe facebook collectif Mayenne Défense des libertés : <a href="https://www.facebook.com/groups/1853356128159103" class="inline-onebox">Groupes Facebook</a>
</li>
</ul>
<p>Mon email : <a href="mailto:bosse.lylie@gmail.com">bosse.lylie@gmail.com</a></p>
<p>A bientôt !<br>
Lydie.</p>

<p class="text-end footer"><span class="text-muted">
<a class="text-muted" href="https://forum.mayennelibre.fr/t/compte-rendu-des-informations-diffusees-rassemblement-du-mardi-12-10/106" target="_blank">Posté le 14/10/2021</a>
<span class="text-muted">
par <img class="avatar avatar-16" src="https://forum.mayennelibre.fr/letter_avatar_proxy/v4/letter/l/b9e5f3/32.png"> Bosse
 |
</span>
<a class="text-info" href="https://forum.mayennelibre.fr/t/compte-rendu-des-informations-diffusees-rassemblement-du-mardi-12-10/106" target="_blank">Voir la discussion >></a>
</p>

</div>
