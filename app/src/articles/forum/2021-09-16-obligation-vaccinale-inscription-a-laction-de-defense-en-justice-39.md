# Obligation vaccinale : Inscription à l'action de défense en justice

<div class="article forum">
<p><div class="lightbox-wrapper"><a class="lightbox" href="https://forum.mayennelibre.fr/uploads/default/original/1X/b8c1dfa8750a1c28b6d84a40677ccf3a29eafb3f.jpeg" data-download-href="https://forum.mayennelibre.fr/uploads/default/b8c1dfa8750a1c28b6d84a40677ccf3a29eafb3f" title="image"><img src="https://forum.mayennelibre.fr/uploads/default/original/1X/b8c1dfa8750a1c28b6d84a40677ccf3a29eafb3f.jpeg" alt="image" data-base62-sha1="qmre4tTXTtfuCAMu1uMDMmVobTN" width="690" height="379" data-small-upload="https://forum.mayennelibre.fr/uploads/default/optimized/1X/b8c1dfa8750a1c28b6d84a40677ccf3a29eafb3f_2_10x10.png"><div class="meta">
<svg class="fa d-icon d-icon-far-image svg-icon" aria-hidden="true"><use xlink:href="#far-image"></use></svg><span class="filename">image</span><span class="informations">880×484 22.3 KB</span><svg class="fa d-icon d-icon-discourse-expand svg-icon" aria-hidden="true"><use xlink:href="#discourse-expand"></use></svg>
</div></a></div></p>
<h2>
<a name="qui-est-concern-1" class="anchor" href="#qui-est-concern-1"></a>Qui est concerné ?</h2>
<p>Vous êtes concerné par l’obligation vaccinale et souhaitez vous défendre ?</p>
<p>Vous pouvez rejoindre notre action, pour partager les frais d’avocat entre tous.<br>
Même si, suivant les cas, les juridictions à saisir, les procédures et le type de défense ne seront pas toujours les mêmes, il reste possible de mutualiser certains coûts, suivant les types de professions :</p>
<ul>
<li>salariés de la fonction publique (santé, etc.);</li>
<li>salariés du privé;</li>
<li>professions libérales;</li>
<li>contractuels, etc.</li>
</ul>
<p>Les procédures de défenses seront <strong>bien individuelles</strong> (exemple : plainte individuelle contre l’employeur, etc.), mais à partir d’une base commune.</p>
<h2>
<a name="o-en-sommes-nous-qui-coordonne-2" class="anchor" href="#o-en-sommes-nous-qui-coordonne-2"></a>Où en sommes nous ? Qui coordonne ?</h2>
<p>Pour le moment, nous avons rencontrer plusieurs avocats. Nous sommes en train d’affiner le choix de ceux-ci (il en faudra probablement plusieurs). Les rendez-vous s’enchaînent… et les premiers devis arrivent.</p>
<p>Cette action est coordonnée par :</p>
<blockquote>
<ul>
<li>
<strong>Hervé Le Nouvel</strong> <a class="mention" href="/u/herveln">@HerveLN</a>  : recueil des dossiers de chaque participant ;<br>
Tel : 06.30.93.14.88 - Email : <a href="mailto:mayennelibertes53@gmail.com">mayennelibertes53@gmail.com</a>
</li>
<li>
<strong>Christophe Fiamma</strong> : conseils juridique et autres urgences liés à votre situation ;<br>
Tel : 06.71.23.07.21 - Facebook : <a href="https://www.facebook.com/christophe.lecaribouman">Christophe le Caribouman</a>
</li>
</ul>
<p>En cas de problème informatique (problème avec le formulaire d’inscription, le forum, site Internet, Telegram, etc) :</p>
<ul>
<li>
<strong>Benoit Lavenier</strong> <a class="mention" href="/u/benoit">@benoit</a> : assistance informatique<br>
Tel : 06.62.86.37.82 - Email : benoit.lavenier (arobase) e-is (point) pro</li>
</ul>
</blockquote>
<p>Nous étofferons l’équipe au fur et à mesure de l’arrivée des dossiers, en les répartissant par type de profession.</p>
<h1>
<a name="comment-sinscrire-3" class="anchor" href="#comment-sinscrire-3"></a>Comment s’inscrire ?</h1>
<p>Voici la démarche (sécurisée et confidentielle) pour rejoindre l’action de défense.</p>
<p>NOTE IMPORTANTE : votre inscription ne vous engage à rien, car aucun frais n’a encore été engagé. L’inscription permet simplement de communiquer avec vous, sur les avancés de l’action, les RDV avec les avocats, etc. Si vous ne souhaitez pas poursuivre cette démarche, il vous suffira de nous le faire savoir.</p>
<ol>
<li>Inscrivez-vous à ce forum :</li>
</ol>
<ul>
<li>
<p>Cliquer sur « Se connecter » (en haut de cette page) puis « Créer un compte »</p>
</li>
<li>
<p><strong>Notez bien le pseudonyme</strong> que vous aurez choisi, il sera utile dans les étapes suivantes.</p>
<p>Cela nous permettra de communiquer plus simplement avec vous, en évitant trop d’échange par email.</p>
</li>
</ul>
<ol start="2">
<li>Préparez les informations suivantes, dont vous aurez besoin pour vous inscrire à l’étape 3:</li>
</ol>
<ul>
<li>Etat civil :
<ul>
<li>Prénom,</li>
<li>Nom, nom de jeune fille,</li>
<li>Date et lieu de naissance</li>
<li>Adresse</li>
</ul>
</li>
<li>Situation professionnel :
<ul>
<li>type de profession,</li>
<li>nom et adresse de l’employeur.</li>
</ul>
</li>
<li>Documents reçus (scannés en fichier PDF ou image)
<ul>
<li>de l’employeur, pour les salariés;</li>
<li>de l’autorité de tutelle, pour les libéraux (ARS, Ordre des médecins, etc).</li>
</ul>
</li>
<li>Votre pseudonyme sur le forum.</li>
</ul>
<ol start="3">
<li>
<p>Remplissez le formulaire d’inscription (prévoir au moins 10min)</p>
<p><a href="https://framaforms.org/inscription-a-laction-juridique-collective-mayenne-1631802700"><img src="https://forum.mayennelibre.fr/images/emoji/twitter/arrow_forward.png?v=10" title=":arrow_forward:" class="emoji" alt=":arrow_forward:"> Formulaire d’inscription en ligne &gt;&gt;</a></p>
<p>Seule notre équipe restreinte y aura accès, pour des raisons évidentes de confidentialité.</p>
</li>
<li>
<p>Une fois inscrit, nous vous donnerons accès à une rubrique privée du forum : <a href="https://forum.mayennelibre.fr/c/thematique/juridique/6">« Action juridique collective »</a>. Elle est réservée exclusivement aux participants de l’action en justice.</p>
<blockquote>
<p>Au préalable, nous aurons vérifier que votre inscription est valide (pour éviter les faux comptes). Donc merci de votre patience : <strong>cela peut prendre quelques heures</strong> !</p>
</blockquote>
</li>
</ol>
<p>Merci de votre attention.</p>
<p>Et surtout : gardez confiance ! Nous sommes face à un déferlement totalitaire, c’est certain. Mais nous sommes ensemble. <strong>Nous tiendrons. Ensemble.</strong></p>

<p class="text-end footer"><span class="text-muted">
<a class="text-muted" href="https://forum.mayennelibre.fr/t/obligation-vaccinale-inscription-a-laction-de-defense-en-justice/39" target="_blank">Posté le 16/09/2021</a>
<span class="text-muted">
par <img class="avatar avatar-16" src="https://forum.mayennelibre.fr/user_avatar/forum.mayennelibre.fr/benoit/32/5_2.png"> Benoit Lavenier
 |
</span>
<a class="text-info" href="https://forum.mayennelibre.fr/t/obligation-vaccinale-inscription-a-laction-de-defense-en-justice/39" target="_blank">Voir la discussion >></a>
</p>

</div>
