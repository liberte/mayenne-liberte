# Compte-rendu - Réunion collectif du 20/10/2021

<div class="article forum">
<p>Vous trouverez ci-joint copie du compte rendu de réunion d’organisation du collectif Mayenne défense des libertés qui s’est tenu mercredi dernier.<br>
Y étaient présents des référents des collectifs de réinfoCovid /Bon sens/Collectif Pompier/CSP/ membres actifs groupe parents d’élèves et membres actifs citoyens.</p>
<blockquote>
<p><strong>Accès au compte-rendu</strong> : <a href="https://www.mayenne-liberte.fr/doc/2021-10-20-CR_reunion_collectif.pdf">Télécharger (PDF) &gt;&gt;</a></p>
</blockquote>
<p>En préambule de la réunion : Proposition de charte</p>
<h2>
<a name="ordre-du-jour-1" class="anchor" href="#ordre-du-jour-1"></a>Ordre du jour</h2>
<p>(défini par un tour de table)</p>
<ol>
<li>Retour tour de tableau : Point suite utilisation autotest Réaction 19</li>
<li>Organisation des flux de communication</li>
<li>Organisation des rassemblements et manifestations à venir</li>
<li>Organisation des CSP</li>
<li>Boite à idées pour les manifs</li>
<li>Ordre réunion prochaine réunion</li>
</ol>
<hr>
<h3>
<a name="1-retour-dexprience-2" class="anchor" href="#1-retour-dexprience-2"></a>1. Retour d’expérience</h3>
<ul>
<li>Utilisation autotest</li>
</ul>
<p>Refus d’entrée au CHU Ponchaillou pour accompagner son enfant de 4 ans sans présentation du pass sanitaire (malgré la présentation de l’autotest).</p>
<p>Prochain rdv au CHU le 1er dec, voir pour planifier une action du collectif sur place pour soutenir la membre pour accompagner son fils dans la structure hospitalière.</p>
<ul>
<li>RDV individuel avec G.Banier</li>
</ul>
<h3>
<a name="2-organisation-des-flux-de-communication-3" class="anchor" href="#2-organisation-des-flux-de-communication-3"></a>2. Organisation des flux de communication</h3>
<p>Contexte :</p>
<ul>
<li>
<p>Contrainte avec le Groupe Facebook : difficulté de partage, surveillance…</p>
</li>
<li>
<p>Multitude de fils telegram</p>
</li>
</ul>
<p>Propositions :</p>
<ul>
<li>
<p>Favoriser l’utilisation du site <a href="https://www.mayenne-liberte.fr/">https://www.mayenne-liberte.fr/</a> : favoriser cet outil pour s’échanger des docs, des infos + forums</p>
</li>
<li>
<p>Développer le forum sur le site : <a href="https://www.mayenne-liberte.fr/">https://www.mayenne-liberte.fr/</a></p>
</li>
<li>
<p>Fusionner certains fils télégram pour arriver à 4 : 1 fil accueil (dédié à la com générale) + 1 fil par CSP (Laval, Mayenne, Château-Gontier, dédié pour le suivi des actions) à communication pour faire migrer les utilisateurs / action pour enlever les liens fils telegram sur le site Mayenne-Liberté</p>
</li>
<li>
<p>Etudier d’une possibilité d’implémenter un formulaire de contact sur le site <a href="http://Mayenne-Liberte.fr">Mayenne-Liberte.fr</a> afin de recueillir les adresses mails + pouvoir envoyer des news letters + comptes-rendus Voir possibilité de mettre en ligne les documents : réclamation laboratoire analyse / réclamation salle de sport / réclamation port du masque à l’école</p>
</li>
</ul>
<h3>
<a name="3-organisation-des-rassemblements-et-manifestations-venir-4" class="anchor" href="#3-organisation-des-rassemblements-et-manifestations-venir-4"></a>3. Organisation des rassemblements et manifestations à venir</h3>
<ul>
<li>Pour les manifestations :</li>
</ul>
<p>Une date par mois pour manifester, date connue longtemps à l’avance pour permettre de déclarer en toute sérénité.</p>
<p>Fréquence des manifs : tous les 2eme samedi du mois, (exception pour novembre) : prochaine <strong>manif le 6 nov</strong> (car deuxième samedi de novembre – pont du 11 novembre).</p>
<ul>
<li>« Les rencontres du kiosque » :</li>
</ul>
<p>Développer l’esprit de rencontre, type « les rencontres du Kiosque » à Laval, à Château et Mayenne en maintenant une rencontre à un point central connu <strong>les samedis après-midi où il n’y a pas de manifestation</strong>.</p>
<ul>
<li>« Les thèmes de manifestation » :</li>
</ul>
<p>Proposition créer un thème de manifestation sur un sujet donné et que chaque manifestant se « déguise selon le thème » : exemple de thèmes</p>
<p>accès au sport ou aux activités ? : et que chacun viennent en tenue de sport ou avec un accessoire de sport ou de musique…</p>
<p>les soignants ? : tout le monde avec une blouse blanche (même si pas soignants)</p>
<ul>
<li>« Les manifestations des autres départements » :</li>
</ul>
<p>Communiquer sur les dates de manifestation des départements limitrophes et vice versa (via nos référents bon sens et réinfocovid ?)</p>
<ul>
<li>Calendrier</li>
</ul>
<div class="md-table">
<table>
<thead>
<tr>
<th>Date</th>
<th>Evènement</th>
<th>Thème de manifestation</th>
<th>Manifestations limitrophes ?</th>
</tr>
</thead>
<tbody>
<tr>
<td>Samedi 23/10</td>
<td>RDV kiosques 3 communes(1)</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Samedi 30/10</td>
<td>RDV kiosques 3 communes</td>
<td></td>
<td></td>
</tr>
<tr>
<td><strong>Samedi 06/11</strong></td>
<td><strong>MANIFESTATION</strong></td>
<td>Thème « ce que je ne peux plus faire » (2)</td>
<td></td>
</tr>
<tr>
<td>Samedi 13/11</td>
<td>RDV kiosques 3 communes</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Samedi 20/11</td>
<td>RDV kiosques 3 communes</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Samedi 27/11</td>
<td>RDV kiosques 3 communes</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Samedi 04/12</td>
<td>RDV kiosques 3 communes</td>
<td></td>
<td></td>
</tr>
<tr>
<td><strong>Samedi 11/12</strong></td>
<td><strong>MANIFESTATION</strong></td>
<td>Thème : « soutien aux suspendus » (3)</td>
<td></td>
</tr>
</tbody>
</table>
</div><blockquote>
<p>Notes :<br>
(1) Lieu à définir pour les communes de CHG et Mayenne.<br>
(2) Venir avec un accessoire qui représente ce que vous ne pouvez plus faire sans pass sanitaire  ex venir habiller en tenue de sport ou avec accessoire de sport (raquette, ballon …) , livres (plus d’accès à la bibliothèque), chacun vient avec une pancarte avec écrit " je ne peux plus faire…" etc<br>
(3) Tous le monde porte une blouse blanche + collectif de pompiers</p>
</blockquote>
<p>L’hiver approchant voir suggestion de lieux un peu abrités.</p>
<ul>
<li>
<p>Idées d’organisation</p>
<ul>
<li>
<p>Lâcher de ballons ? à prévoir un mois avant</p>
</li>
<li>
<p>Faire intervenir des personnalités pour les manifs comme ce qui a été fait avec AJB: activation de réseau</p>
</li>
<li>
<p>Musique pour les prochaines manifestations - prise de contact avec musicien</p>
</li>
</ul>
</li>
</ul>
<h3>
<a name="4-organisation-des-csp-5" class="anchor" href="#4-organisation-des-csp-5"></a>4. Organisation des CSP</h3>
<p>Actions menées cette semaine :</p>
<ul>
<li>
<p>Rassemblements simultanés aux 3 permanences des députés (environ 50 personnes mobilisées)</p>
</li>
<li>
<p>Point à la mairie de Laval mardi après-midi</p>
</li>
<li>
<p>Regroupement au café de la députée Bannier vendredi 22/10 (Meslay du Maine CSP 248)</p>
</li>
<li>
<p>Nomination des 2 référents des CSP 248 et 249</p>
</li>
</ul>
<p>Actions à venir :</p>
<ul>
<li>
<p>Rencontrer nos sénateurs (Chevrolier, Doisneau ) – action Lydie pour prise de RDV</p>
</li>
<li>
<p>Accompagnement des personnes qui auront des RDV individuels avec les députés</p>
</li>
</ul>
<h3>
<a name="5-botes-ides-6" class="anchor" href="#5-botes-ides-6"></a>5. Boîtes à idées</h3>
<p>Etudier la possibilité d’implémenter la boite à idées sur le site Internet</p>
<h3>
<a name="6-ides-ordre-du-jour-runion-suivante-7" class="anchor" href="#6-ides-ordre-du-jour-runion-suivante-7"></a>6. Idées ordre du jour réunion suivante :</h3>
<ul>
<li>
<p>Action contre le port du masque à l’école</p>
</li>
<li>
<p>Voir nos maires pour avoir un soutien, un engagement de leur part : transmettre le courrier des élus</p>
</li>
</ul>

<p class="text-end footer"><span class="text-muted">
<a class="text-muted" href="https://forum.mayennelibre.fr/t/compte-rendu-reunion-collectif-du-20-10-2021/111" target="_blank">Posté le 22/10/2021</a>
<span class="text-muted">
par <img class="avatar avatar-16" src="https://forum.mayennelibre.fr/letter_avatar_proxy/v4/letter/l/b9e5f3/32.png"> Bosse
 |
</span>
<a class="text-info" href="https://forum.mayennelibre.fr/t/compte-rendu-reunion-collectif-du-20-10-2021/111" target="_blank">Voir la discussion >></a>
</p>

</div>
