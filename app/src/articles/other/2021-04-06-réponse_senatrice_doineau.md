
# Réponse de la sénatrice Elisabeth Doineau

Monsieur,

J'accuse bonne réception de votre courriel que nous avons lu avec la plus grande attention. Nous vous remercions de nous avoir fait partager vos réflexions sur des sujets particulièrement importants et sensibles.
Je tiens à vous informer de l'engagement du Sénat et de Madame Doineau en faveur de la défense des libertés et de la protection des données personnelles, notamment lorsqu'elles traitent de la santé des individus. L'avis de la CNIL est un repère majeur pour le Parlement sur ces problématiques.
Enfin, de nombreux sénateurs, à l'instar de la sénatrice Catherine Morin-Desailly ou du sénateur Loïc Hervé, travaillent à la "déGAFAMisation" de notre société et au développement de notre souveraineté numérique.

Je vous prie de croire, Monsieur, en l'expression de mes sincères salutations.

Jonathan Laban-Bounayre
Collaborateur parlementaire 
-----
Elisabeth Doineau
Sénatrice de la Mayenne
