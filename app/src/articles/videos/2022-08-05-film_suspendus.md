# "Suspendus... Entre deux mondes"

<img src="/gallery/2022-08-05/thumbnail/__2022-08-05-film_suspendus-vignette.png" class="invisible">

Le film-documentaire “Suspendus… Des soignants entre deux mondes” témoigne de la réalité des soignants suspendus en portant un regard critique et constructif sur notre système de santé actuelle et d’un autre possiblement émergent.
Le tout porté avec esthétisme en musiques avec des chorégraphies filmées dans des lieux insolites !

[![thumbnail](/gallery/2022-08-05/__2022-08-05-film_suspendus-vignette.png)](https://vimeo.com/707679502)

Voici les sept soignants qui témoignent :

<p>
@@include('../../gallery/2022-08-05.html')
</p>
