# Évaluation des modélisations Covid-19 en France

Depuis le début de la pandémie, de nombreuses modélisations ont servi à éclairer la décision politique. 

Maxime Langevin et Thomas Strack, tous deux Polytechniciens, ont comparé ces modélisations
avec ce qu’il s’est réellement passé lors des moments forts de la pandémie (confinement, couvre-feu, instauration du pass sanitaire…).

Plusieurs rapports provenant de différents instituts (Institut Pasteur, l'INSERM ou l'Imperial College)
ont été analysé.

Dans les graphiques ci-dessous, les courbes grises représentant les scénarios, les rouges la réalité.

Leur conclusion est simple : **les risques ont été largement sur-estimés !**
Elle a été présenté à [la commission d’enquête sénatoriale sur le pass vaccinal](https://www.publicsenat.fr/article/parlementaire/passe-vaccinal-le-senat-lance-officiellement-une-commission-d-enquete-191977),
avant son adoption.

## Mars 2020: les modélisations qui ont conduit le monde à se confiner

![1er confinement 1/2](/doc/2022-02-14-evaluation_modelisation_imperial_college.png)

![1er confinement 2/2](/doc/2022-02-14-evaluation_modelisation_imperial_college_2.png)

## Avril 2020: déconfinement de l’Ile-de-France

![Avril 2020](/doc/2022-02-14-evaluation_modelisation_echos_avril_2020.png)

## Oct 2020: Modélisations de la 2ème vague, et 2ième confinement

![Oct 2020](/doc/2022-02-14-evaluation_modelisation_pasteur_oct_2020.png)


## Pour aller plus loin

Pour consulter toute l'évaluation :

<p class="text-center">
<a type="button" class="btn btn-primary" href="https://evaluation-modelisation-covid.github.io/france/" target="_blank"> 
  https://evaluation-modelisation-covid.github.io/france
</a>
</p>

À voir également : 
- Suivez les publications de l'évaluations : [@ModelisationC](https://twitter.com/ModelisationC) (Twitter) ;
- [Interview du docteur Alice Desbiolles](https://youtu.be/wujkLnn-e-0?t=1176) présentant l'évaluation sur Sud Radio;
- [Interview du mathématicien Vincent Pavan](https://odysee.com/@nowunmondemeilleur:4/vincentpavan:d)
  (fondateur de l'association [Réinfo Liberté](https://reinfoliberte.fr)) qui dénonce l'escroquerie scientifique via les modélisations;
