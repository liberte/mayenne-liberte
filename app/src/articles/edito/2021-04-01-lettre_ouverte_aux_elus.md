# Lettre ouverte aux élus Mayennais 

> _"En colère contre le passeport vaccinal : nous demandons des actions à nos élus mayennais."_

<p class="text-right pt-5">Le 1er avril 2021,
</p>

Mesdames et messieurs les députés de la Mayenne,
Mesdames et messieurs les sénateurs de la Mayenne,
Mesdames et messieurs les élus de la Mayenne,

Nous, citoyens mayennais, apprenons que :
- Depuis le 17 mars que l'[UE a décidé la création d'un passeport vaccinal](https://fr.euronews.com/2021/03/02/le-passeport-covid-europeen-sera-presente-le-17-mars) ;
- Dès avril 2021, l’[UE a commandé 1,8 milliard de doses](https://www.huffingtonpost.fr/entry/vaccin-ue-negocie-pfizer-biontech-milliard-doses-variants_fr_6076dd02e4b089e3a2c5308d) chez Pfizer-BioNTech,
  pour une population de seulement 450 millions d’européens.
- Dans son [discours du 12 juillet 2021](./events/2021-07-21-discours_emmanuel_macron.html), Emmanuel Macron
  dit vouloir : généraliser d'avantage le passe sanitaire, imposer la vaccination
  à certains et [rendre impossible la vie aux non-vaccinés](https://www.gouvernement.fr/vaccination-et-pass-sanitaire-ce-qui-change);
  Il indique même avoir l'objectif de "vacciner le monde", dans le cadre du [programme ACT-A](https://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/actualites-du-ministere/informations-coronavirus-covid-19/coronavirus-declarations-et-communiques/article/sante-mondiale-participation-de-clement-beaune-a-la-premiere-reunion-du-conseil).

Par ailleurs, nous découvrons que :
- [La France a passé un contrat pour stocker les données médicales](https://www.msn.com/fr-fr/actualite/france/donn%C3%A9es-de-sant%C3%A9-des-fran%C3%A7ais-le-choix-contest%C3%A9-de-microsoft/ar-BB19DYSF) Françaises (dont
  la vaccination) à... Microsoft ! Dont le fondateur annonce depuis 10 ans une pandémie mondiale, milite
  pour passeport vaccinal numérique ;
- Depuis septembre 2019, un [carnet de vaccination injecté sous la peau est en test](https://www.lemonde.fr/afrique/article/2019/12/19/le-kenya-et-le-malawi-zones-test-pour-un-carnet-de-vaccination-injecte-sous-la-peau_6023461_3212.html),
  au Kenya et au Malawi, financé par la Fondation Gates (sources : 
  [Le Monde](https://www.lemonde.fr/afrique/article/2019/12/19/le-kenya-et-le-malawi-zones-test-pour-un-carnet-de-vaccination-injecte-sous-la-peau_6023461_3212.html),
  [La Croix](https://www.la-croix.com/Sciences-et-ethique/carnet-vaccination-invisible-peau-2019-12-18-1301067381), 
  [MIT](https://news.mit.edu/2019/storing-vaccine-history-skin-1218)).

**Mesdames, Messieurs les élus, les mayennais attendent de savoir quelles sont vos actions**,
pour que nous ne tombions pas tout à fait dans une dictature sanitaire !

Est-ce cela l'avenir de notre société ? Devoir se justifier dans tous nos déplacements ?
Et cela par l'intermédiaire d'un système centralisé dépendant des [GAFAM](https://fr.wikipedia.org/wiki/GAFAM) ? Sommes-nous encore en France ?

Quid des traitements SARS-CoV-2 ? De nombreuses études indépendances confirment [l'efficacité de
l'hydroxychloroquine et de l'azithromycine](https://twitter.com/raoult_didier/status/1362347035421249549) en utilisation précoce.
Plus récemment [d'autres études indépendantes](https://www.youtube.com/watch?v=bUGRrc9mQ8M) démontrent l'efficacité de l'Ivermectine.
C'est l'_omerta_ complète dans les médias sur ces traitements, pourtant peu coûteux, au profit d'une propagande
pro-vaccin. Alors même que **les études sur les vaccins ne seront achevées qu'en 2022, voir 2023** !
Ceci est un énorme scandale ! Nous ne sommes pas des cobayes !
  
Librement,

<p class="text-center">
<a class="btn btn-primary" target="_blank" href="https://framaforms.org/je-signe-la-lettre-ouverte-aux-elus-mayennais-1617799114">
Je signe la lettre ! <small><i class="bi-box-arrow-up-right"></i></small>
</a>
&nbsp;
<button type="button" class="btn btn-dark" onclick="openSignersModal()"> 
  Voir les signataires
</button>
&nbsp;
<button type="button" class="btn btn-secondary" onclick="openElusModal()">
Coordonnées des élus
</button>
</p>

# Réponses des élus

Nous publions ici les réponses recues des élus :

- Réponse de la sénatrice [Mme Doineau - 06 avril 2021](/articles/other/2021-04-06-réponse_senatrice_doineau.html)

[comment]: <> (modal for signatures)
<div id="signers-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Signataires</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body m-0 p-0">
        <table id="signers-table" class="table-striped border-secondary">
          <thead>
            <tr>
              <th data-field="Séquentiel">
                <span class="text-secondary">
                  #
                </span>
              </th>
              <th data-field="Nom">
                <span class="text-secondary">
                  Nom
                </span>
              </th>
              <th data-field="Prénom">
                <span class="text-secondary">
                  Prénom 
                </span>
              </th>
              <th data-field="Ville">
                <span class="text-secondary"> 
                  Ville
                </span>
              </th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

[comment]: <> (modal for elus)
<div id="elus-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Coordonnées des élus mayennais</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body m-0 p-0">
        <table id="elus-table" class="table-striped border-primary">
          <thead>
            <tr>
              <th data-field="Nom">
                <span class="text-secondary">
                  Nom
                </span>
              </th>
              <th data-field="Prénom">
                <span class="text-secondary">
                  Prénom
                </span>
              </th>
              <th data-field="Fonction">
                <span class="text-secondary">
                  Fonction
                </span>
              </th>
              <th data-field="Email">
                <span class="text-secondary">
                  Email
                </span>
              </th>
            </tr>
          </thead>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
