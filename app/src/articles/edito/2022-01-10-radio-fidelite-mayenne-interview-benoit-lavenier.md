# <i class="bi-broadcast-pin"></i> Le président de la république a déclaré vouloir "emmerder" les non vaccinés jusqu’au bout

<img src="/img/radio-micro.jpg" class="invisible">

Benoit Lavenier, membre fondateur du collectif Mayenne Liberté réagit au propos du président, à l'antenne de radio Fidelité Mayenne.

Il dénonce la politique du bouc émissaire et revient sur deux ans d'escroqueries scientifique, base d'une véritable propagande d'état. 

**Pour écouter l'émission** : [<i class="bi-box-arrow-up-right"></i> Podcast de l'émission](http://fidelitemayenne.fr/emissions/invite-regional-53/10-01-2022) | [<i class="bi-file-music"></i> Fichier audio MP3](/doc/2022-01-10-radio-fidelite-invite-regional-53.mp3)

![vaccin Covid-19](/img/itw-lavenier-fidelitemayenne.jpg)

> Pour **aller plus loin** au sujet de l'escroquerie scientifique :
>
> L'association [Réinfo-Liberté](https://reinfoliberte.fr/) a déposé une **plainte contre X**, pour faux et escroquerie en bande organisée, visant notamment l'Institut Pasteur.
> - [<i class="bi-youtube"></i> Plainte contre X déposée par l'association Réinfo Liberté](https://reinfocovid.fr/articles_video/live-csi-du-jeudi-2-septembre-2021/) - Vincent Pavan et Maître Ludovic - CSI du 02/09/2021
>
> Le détail de l'escroquerie est expliqué dans ces conférences : 
> - [<i class="bi-youtube"></i> "Les confinements ont accéléré la propagation de l'épidémie"](https://odysee.com/@Reinfocovid:2/CSIdu29AVRIL2021avecVPAVAN:c) - Vincent Pavan - CSI du 29/04/2021
> - [<i class="bi-youtube"></i> "À quoi tient scientifiquement le pass sanitaire ?"](https://reinfocovid.fr/articles_video/live-csi-du-jeudi-22-juillet-2021/) - Vincent Pavan - CSI du 22/07/2021
> - [<i class="bi-youtube"></i> "Pharmacovigilance et formalisation"](https://odysee.com/@Reinfocovid:2/CSIdu29AVRIL2021avecVPAVAN:c) - Vincent Pavan - CSI du 11/11/2021
>
> Sur la faible mortalité et sur l'effet limité de la vaccination : 
> - [<i class="bi-youtube"></i> Bilan de la mortalité et de l'effet de la vaccination](https://www.youtube.com/watch?v=_x-eozrCCNM) par l'IHU de Marseille - 11/01/2022
> <br/>

