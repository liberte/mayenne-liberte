# Ce qui change au 1er août...

Le Gouvernement voulait prolonger l'état d'urgence sanitaire pour neuf mois supplémentaires. [Son projet de loi déposé le 4 juillet](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/3/nsuM_wkGADeOsceRN9fBTw/aHR0cHM6Ly93d3cuYXNzZW1ibGVlLW5hdGlvbmFsZS5mci9keW4vMTYvdGV4dGVzL2wxNmIwMDA5X3Byb2pldC1sb2k) prévoyait ainsi : “_L’article 11 de la loi n° 2020‑546 du 11 mai 2020 prorogeant l’état d’urgence sanitaire et complétant ses dispositions est ainsi modifié : 1° Au premier alinéa du I, **la date : «31 juillet 2022» est remplacée par la date : «31 mars 2023»**;_”.

Pour faire avaler la pilule, le Gouvernement misait sur les prémonitions erronées de Mme Yaël Braun-Pivet, la nouvelle Présidente de l'Assemblée Nationale :

> – _Le pass vaccinal va-t-il revenir ?_
> – _**Ce n'est pas ce qui est prévu dans le texte de loi qui va être soumis au Parlement dès cette semaine**_

[Source \[VIDÉO\] :](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/4/c_uDChirtLAspo-1XJy2Qw/aHR0cHM6Ly90d2l0dGVyLmNvbS9pL3dlYi9zdGF0dXMvMTU0MzU1Nzk2NjUxNDg1MTg0MA)

[![Vidéo ](https://ci5.googleusercontent.com/proxy/XxEh4K_C128-3jSCKWkct2MDnptTpScrZsqOICeNyiHQ10YpH6l3iiSjXrkL-y2nhggyEz-IMc-ObLWDM2QQu6wPqsQA=s0-d-e1-ft#https://t.palace.legal/img/x9v9n/b/uxyjr/6kkts.png)](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/5/vWtZ0tzhep8iDckEeQ2U1A/aHR0cHM6Ly90d2l0dGVyLmNvbS9pL3dlYi9zdGF0dXMvMTU0MzU1Nzk2NjUxNDg1MTg0MA)

Pourtant le titre-même du projet de loi était clair :

_“Projet de loi **maintenant provisoirement un dispositif de veille et de sécurité sanitaire** en matière de lutte contre la covid‑19”_ ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/6/CkYl96Kr7torc6JmgPZ0gw/aHR0cHM6Ly93d3cuYXNzZW1ibGVlLW5hdGlvbmFsZS5mci9keW4vMTYvdGV4dGVzL2wxNmIwMDA5X3Byb2pldC1sb2k)).

Mais à l'issue des débats parlementaires et notamment de la Commission Mixte Paritaire qui s'est ensuivie, le texte a été refondu et retitré à l'antithèse :

_“LOI n° 2022-1089 du 30 juillet 2022 **mettant fin aux régimes d'exception** créés pour lutter contre l'épidémie liée à la covid-19”_ ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/7/S2-uxStUDISufUqCuXAV_Q/aHR0cHM6Ly93d3cuYXNzZW1ibGVlLW5hdGlvbmFsZS5mci9keW4vMTYvcmFwcG9ydHMvOS9sMTZiMDE1OF9yYXBwb3J0LWZvbmQj#)).

Ce premier texte adopté sous la nouvelle législature montre un tournant. On revient d'un hyper-présidentielisme à un régime plus parlementaire.

**Le texte finalement adopté présente des avancées :**

 * Il acte une **sortie du régime de l'état d'urgence sanitaire au 1er août 2022** ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/8/VX7lbT9zy4JQycvHt-SVvQ/aHR0cHM6Ly93d3cubGVnaWZyYW5jZS5nb3V2LmZyL2pvcmYvaWQvSk9SRlRFWFQwMDAwNDYxMTQ2MzA)) ;  

 * Il **supprime les dispositions du Code de la santé publique ainsi que les dispositions non codifiées relatives à l'état d'urgence sanitaire** ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/9/sZHK3OyZwx4A_NLWXtJ8oA/aHR0cHM6Ly93d3cubGVnaWZyYW5jZS5nb3V2LmZyL2pvcmYvaWQvSk9SRlRFWFQwMDAwNDYxMTQ2MzA)) ;  


En simple, ce texte marque le **retour à un régime de droit commun**.

**Mais il comprend aussi des faiblesses :**

 * Les soignants (et assimilés) suspendus ne sont toujours pas réintégrés à ce stade. Le législateur botte en touche en renvoyant à un avis de la Haute Autorité de Santé (HAS), qui peut se saisir d'elle-même ou être saisie par le Ministre de la santé, le comité de contrôle et de liaison Covid-19 ou par les Commissions des affaires sociales de l’Assemblée Nationale ou du Sénat. Sachant que la HAS vient de recommander le maintien de l'obligation vaccinale ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/10/CXkVEq3jYmIsR1GONf3xNg/aHR0cHM6Ly93d3cuaGFzLXNhbnRlLmZyL2pjbXMvcF8zMzU2MjMxL2ZyL2NvdmlkLTE5LWxhLWhhcy1wcmVjb25pc2UtbGUtbWFpbnRpZW4tZGUtbC1vYmxpZ2F0aW9uLXZhY2NpbmFsZS1kZXMtcGVyc29ubmVscy1leGVyY2FudC1kYW5zLWxlcy1ldGFibGlzc2VtZW50cy1kZS1zYW50ZS1ldC1tZWRpY28tc29jaWF1eA)) ;  

 * Un certificat sanitaire de voyage est mis en place – basé sur les tests et non le statut “vaccinal” compte tenu de la faible protection contre la transmission – pour les voyages vers l'étranger. Bizarrement, l'Outre-mer français est assimilé à l'étranger, que ce soit pour les voyages à destination ou en provenance de l'Hexagone. Un recours intenté par des députés devant le Conseil constitutionnel a été rejeté le 30 juillet sous la Présidence de M. Laurent FABIUS ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/11/bOHZyT7QmHplfu1fe6gWtw/aHR0cHM6Ly93d3cuY29uc2VpbC1jb25zdGl0dXRpb25uZWwuZnIvZGVjaXNpb24vMjAyMi8yMDIyODQwREMuaHRt)) ;  

 * Le fichier “Si-DEP” (Système d'Informations de DEPistage), où sont enregistrés les résultats des tests ainsi que les cas contacts, est maintenu jusqu'au 31 janvier 2023 au moins ([source](https://t.palace.legal/lnk/AbQAAD-GQqIAAcssPw4AAATCP9YAAAAAlykAAR1OABiKZQBi5pc4SnRuyQzkSOKavM0DjyBS9wAPGyQ/12/vfT4_mazoaTXHOISdc8guA/aHR0cHM6Ly93d3cubGVnaWZyYW5jZS5nb3V2LmZyL2pvcmYvaWQvSk9SRlRFWFQwMDAwNDYxMTQ2MzA)).

**Ainsi qu'une disposition qui interpelle gravement :**

_“Dans un délai de trois mois à compter de la promulgation de la présente loi, le Gouvernement présente au Parlement une évaluation du cadre juridique en vigueur, y compris en matière de traitements de données à caractère personnel, **afin de faire le bilan des moyens à la disposition des autorités publiques pour lutter contre les pandémies et, le cas échéant, de les redéfinir sans avoir recours à un régime d'exception.**”_

Après avoir évacué par la fenêtre des dispositions attentatoires aux libertés, le Parlement propose donc de les faire ré-entrer par la grande porte en les redéfinissant, mais en les pérennisant.

C'est donc l'endurance qui sera de mise dans cette affaire.

Dès lors, il faut poursuivre les procès intentés en défense de soignants suspendus, en engageant de nouvelles procédures, ainsi que des voies de recours lorsque cela est utile ;

(...)

Top peu de gens comprennent bien ce qui se passe actuellement en matière de libertés individuelles et de droits fondamentaux. Partagez ces informations.


<p class="text-center">
<a type="button" class="btn btn-primary" href="https://dejavu.legal/" target="_blank"> 
  Signer la pétition <b>Transparence vaccins Covid-19</b>
</a>
</p>

<p class="text-end text-right">
<i>Source: <a href="https://dejavu.legal/" target="_blank">Projet “DejaVu”</a></i> 
</p>
