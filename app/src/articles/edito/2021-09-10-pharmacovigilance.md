# Pharmacovigilance des vaccins Covid-19

Vous souhaitez connaitre les risques liés aux vaccins génétiques Covid-19 ?
Vous souhaitez avoir les chiffres actualisés des effets indésirables, avec des sources officielles, documentées et vérifiables ?

Nous publions ici les informations provenant de la pharmacovigilance française et européenne.
Suivant la source, ces chiffres sont parfois compliqués à obtenir et souvent mal relayés dans les médias.

![vaccin Covid-19](/img/vaccin-covid-19.jpg)

## Données de l'Union Européenne

Le site officiel de pharmacovigilance pour l'Union Européenne est celui de l'EUDRA (European Union Drug Regulating Authorities).
Il est accessible à l'adresse suivante : https://www.adrreports.eu/en/search_subst.html

### Analyse des données de l'UE

Voici les chiffres que l'on peut retenir, tous vaccins confondus (3) [au 10/09/2021](/doc/pharmacovigilance/2021-09-10-pharmacovigilance-ue.pdf) :

- **929 128 effets indésirables déclarés** (graves ou non) (+6.1% en une semaine)

  dont **418 016 effets indésirables graves** (+4.7% en une semaine)

  Soit **≈ 4 180 160 effets indésirables graves, dans la réalité** (1)
  
- **24 518 décès déclarés** (+5.4% en une semaine)

  Soit approx. **≈ 245 180 décès dans la réalité**, en compensant la sous-déclaration (1).

> Télécharger le détail par vaccins : 
> [<i class="bi-file-pdf"></i> Fichier PDF](../../doc/pharmacovigilance/2021-09-10-pharmacovigilance-ue.pdf)
> | [<i class="bi-file-spreadsheet"></i> Fichier XLS](../../doc/pharmacovigilance/2021-09-10-pharmacovigilance-ue.xlsx)
> | [<i class="bi-file-spreadsheet"></i> Fichier ODS](../../doc/pharmacovigilance/2021-09-10-pharmacovigilance-ue.ods)<br/>
> Comment obtenir ces chiffres ? [<i class="bi-youtube"></i> Vidéo explicative (10 min)](https://odysee.com/@bibule:2/Pharmacovigilance-UE:7)


## Données françaises

En France, l'[ANSM](https://ansm.sante.fr/) (Agence National de sécurité du médicament) est responsable du suivi des effets indésirables.

### Couverture des données françaises

L'ANSM a mise en place [une enquête de pharmacovigilance et un comité de suivi](https://ansm.sante.fr/dossiers-thematiques/covid-19-suivi-hebdomadaire-des-cas-deffets-indesirables-des-vaccins).
Deux fois par mois, un rapport de situation est publié.
Les données communiquées sont agrégées, non ventilées par effet indésirable, rendant impossible toute analyse contradictoire ou complémentaire. 

Depuis avril 2021, l'ANSM a en effet retiré de ses rapports des informations pourtant essentielles, 
telles que le **nombre de cas pour chaque effet indésirable grave** (cf ce [rapport de mars 2021](https://ansm.sante.fr/actualites/point-de-situation-sur-la-surveillance-des-vaccins-contre-la-covid-19-11)
où l'on peut encore lire, notamment, le nombre de décès par vaccin : **279 décès** pour Pfizer). 

Heureusement d'autres rapports plus spécifiques, issus des centres de pharmacovigilance régionaux, sont accessibles.
Ils permettent d'accéder au nombre de cas pour chaque effet grave, notamment pour les décès.

Essayons de regarder toutes ces données de plus près...

### Analyse des données françaises

Cette analyse utilise principalement :
- [<i class="bi-file-pdf"></i> Point de situation du 27/08/2021](https://ansm.sante.fr/uploads/2021/08/27/20210824-vaccins-covid-19-fiche-de-synhte-se-vf.pdf) de l'ANSM, 
  sur toute la France, arrêté au 19/08/2021;

Mais aussi, lorsque l'information est manquante, les rapports issus des centres régionaux :
- [<i class="bi-file-pdf"></i> Rapport sur Pfizer au 01/07/2021](https://ansm.sante.fr/uploads/2021/07/16/20210716-vaccins-covid-19-rapport-pfizer-periode-28-05-2021-01-07-2021-2.pdf) remis par 4 centres (Bordeaux, Marseille, Toulouse et Strasbourg);
- [<i class="bi-file-pdf"></i> Rapport sur Moderna au 01/07/2021](https://ansm.sante.fr/uploads/2021/07/16/20210716-vaccins-covid-19-rapport-moderna-periode-28-05-2021-01-07-2021.pdf) remis par 2 centres (Lille et Besançon).
- [<i class="bi-file-pdf"></i> Rapport sur AstraZeneca au 08/07/2021](https://ansm.sante.fr/uploads/2021/08/27/20210824-vaccins-covid-19-fiche-de-synhte-se-vf-2.pdf) remis par 2 centres (Amien et Rouen).
  <span class="text-danger"><i class="bi-cone-striped "></i> Analyse en cours</span>
- [<i class="bi-file-pdf"></i> Rapport sur Janssen au 08/07/2021](https://ansm.sante.fr/uploads/2021/07/23/20210723-covid-19-rapport-janssen-2.pdf) remis par 2 centres (Grenoble et Lyon)?
  <span class="text-danger"><i class="bi-cone-striped "></i> Analyse en cours</span>

Evolution globale du nombre d'effets indésirables : 

 ![Evolution du nombre d'effets indésirables](../../img/2021-08-19-ansm-evolution-effets-indesirables.png)

 Soit **78 639 cas retenus** (graves et non graves)

#### Effets indésirables graves

Regardons de plus près les effets dits "graves". 

- Proportion effets indésirables graves/non graves : 

  ![Proportion effets indésirables graves/non graves](https://ansm.sante.fr/media/cache/block_slider/uploads/2021/08/27/20210824-vaccins-covid-19-fiche-de-synhte-se-vf-3.jpg)

  Soit **19 659 cas graves** (25%) depuis le début de la vaccination.
  
  Soit **≈ 196 590 cas graves dans la réalité** (1).

  Soit **≈ 0.45 % de risque** d'effets indésirables graves, pour 44 millions de vaccinés avec au moins une dose (2)

  Ces chiffres sont également connus, par vaccin :
  * Pfizer : **12 484 cas graves** (28%)
  * Moderna : **1 650 cas graves** (18%) 


- Répartition des effets indésirables par organe, pour chaque vaccin :
  * non graves;
  * graves;


- Répartition des effets indésirables (tout confondu, graves ET non graves) **par sexe** :
  * Pfizer : <i class="bi-gender-female"></i> **55 047 femmes** (70%) <i class="bi-gender-male"></i> **23 591 hommes** (30%);
  * Moderna : <i class="bi-gender-female"></i> **6 875 femmes** (75%) <i class="bi-gender-male"></i> **2 291 hommes** (25%);


- Répartition des effets indésirables (tout confondu, graves ET non graves) **par classe âge**

- Effets indésirables détectés (sans indiquer le nombre de cas) :

  * Hypertension artérielle : Pfizer, Moderna;
  * Myocardites et péricardites: 
    * Pfizer, Moderna (l'ANSM [alerte le 23/07/2021](https://ansm.sante.fr/informations-de-securite/vaccins-a-arnm-contre-la-covid-19-cominarty-et-spikevax-risque-de-myocardite-et-de-pericardite)
    de _"quelques rares cas de myocardites et péricardites"_ mais sans donner de chiffre)
    * AstraZeneca (sous surveillance - non confirmé)
  * Syndromes pseudo-grippaux (AstraZeneca)
  * Thromboses (AstraZeneca : 65 cas déclarés, dont 14 décès)

- Suivis spécifiques des effets indésirables :
  * Suivi des jeunes (12-18 ans). Selon l'ANSM "aucun signal n’a été identifié chez les sujets les plus jeunes"
  
    Cependant, comme la "proportion cas graves/non graves" chez les plus jeunes n'est pas communiqué, impossible de le vérifier.

    Pour pfizer, cependant, les centres régionaux indique 0.32% de cas graves. Soit ≈ 40 cas graves déclarés, 
    que l'on peut estimé à **400 cas graves dans la réalité** (1)

  * Fausses couches spontanées (en indiquant que "les données actuelles ne permettent pas de conclure que ces événements sont liés au vaccin");

- Liste des effets potentiels "sous surveillance" (sans donner le nombre de cas par effets) : 
  <details><summary>Voir la liste</summary>
  <ul>
    <li>Zona (Pfizer, Moderna, AstraZeneca)
    <li>Troubles du rythme cardiaque (Pfizer, Moderna, AstraZeneca)
    <li>Thrombopénie / thrombopénie immunologique / hématomes spontanés (Pfizer)
    <li>Déséquilibre diabétique dans des contextes de réactogénicité (Pfizer)
    <li>Echecs vaccinaux (Pfizer)
    <li>Pancréatite aigüe (Pfizer)
    <li>Syndromes de Guillain-Barré (Pfizer)
    <li>Syndrome d’activation des macrophages (Pfizer)
    <li>Réactivation à virus Epstein-Barr (Pfizer)
    <li>Méningoencéphalite zostérienne (Pfizer)
    <li>Aplasie médullaire idiopathique (Pfizer)
    <li>Hémophilie acquise (Pfizer)
    <li>Polyarthrite rhumatoïde (Pfizer, Moderna)
    <li>Néphropathies glomérulaires (Pfizer, Moderna)
    <li>Troubles menstruels (Pfizer, Moderna)
    <li>Ictus amnésique : amnésie transitoire (Moderna)
    <li>Troubles auditifs : surdité, hypoacousie et acouphènes (Moderna, AstraZeneca)
    <li>Pertes de connaissances, plus ou moins associées à des chutes (Moderna)
    <li>Saignements cutanéo-muqueux (Moderna, AstraZeneca)
    <li>Erythème polymorphe (Moderna)
    <li>Erythème noueux (AstraZeneca)
    <li>Elévation de la pression artérielle (AstraZeneca)
    <li>Dyspnées et asthme associés à des syndromes pseudo-grippaux (AstraZeneca)
    <li>Paralysie faciale (AstraZeneca)
    <li>Pathologie démyélinisante centrale (AstraZeneca)
    <li>Colite ischémique (AstraZeneca)
    <li>Vascularites (AstraZeneca)
  </ul>
</details>


- Évènements sous surveillance : 
  * Évènements thromboemboliques (Pfizer)
  * Contractions utérines douloureuses (Pfizer)
  * Morts in utero (Pfizer, Moderna)

#### Analyse des cas de décès

A partir des rapports spécifiques par vaccin, émis par les centres de pharmacovigilance régionaux, 
il est possible d'estimer le nombre de décès à l'échelle de la France :

- Pfizer (cf [rapport au 01/07/2021](https://ansm.sante.fr/uploads/2021/07/16/20210716-vaccins-covid-19-rapport-pfizer-periode-28-05-2021-01-07-2021-2.pdf)) :

  **761 décès déclarés** (2.4% tous cas confondus - 8.8% des cas graves) :

  Soit sur toute la France : (4)

  ≈ 1 093 décès déclarés (8.8% des 12 484 cas graves déclarés)

  **≈ 10 930 décès dans la réalité** (1) 

- Moderna (cf [rapport au 01/07/2021](https://ansm.sante.fr/uploads/2021/07/16/20210716-vaccins-covid-19-rapport-moderna-periode-28-05-2021-01-07-2021.pdf)) :

  **44 décès déclarés** (4.2% des cas graves)

  Soit sur toute la France : (4)

  ≈ 69 décès déclarés (4.2% des 1 650 cas graves déclarés)

  **690 décès dans la réalité** (1)

#### Conclusion, sur les données françaises

L'ANSM ne donne aucun accès aux données individuelles, pourtant utiles
pour vérifier les indicateurs, ou en recalculer de nouveaux.

Par exemple, il est impossible de calculer la répartition **des cas graves par tranche d'âge ET sexe**.
Difficile, dans ces conditions, de calculer une balance précise "bénéfices / risques", au cas par cas.

**Nous devons donc croire l'ANSM sur parole** !

Pourquoi l'ANSM ne nous communique plus le nombre de décès, à l'échelle de la France, alors que manifestement
elle dispose de ces informations au niveau régional ?

Aujourd'hui, la plupart des français ne veulent plus du vaccin AstraZeneca, dont le risque de thrombose fait peur.
Pourtant, seulement 14 décès par thrombose lui sont attribué pour toute la France.
Quand on sait que la norme pour mettre fin a des essais cliniques, aux USA et en Allemagne, est proche de 50 décès,
que dire des 761 décès déclarés chez Pfizer (qui ne concerne même pas la France entière) ?

Enfin, chez les jeunes (12-18 ans), en l'absence de données fines, **il reste à démontrer que la létalité des vaccins
n'est plus importante que celle du Covid-19**. 
Or, pour rappel, au plus 20 décès des suites du Covid-19 sont a déplorer, pour cette population, sur l'année 2020.

N'oublions pas que ces vaccins sont expérimentaux, car encore en essai clinique,
jusqu'en 2022 voir 2023.

Ne devons-nous pas restez prudents, notamment sur les plus jeunes ?


## Et pour les jeunes ?

![Devons-nous vacciner nos enfants contre le Covid-19 ?](https://www.francesoir.fr/sites/francesoir/files/clip_image001_field_mise_en_avant_principale_1_0_0_0.jpg)

### Comparaison de la létalité "Vaccins génétiques" / Covid-19

Le Conseil Scientifique Indépendant (CSI) a fait une analyse comparative ([publiée le 12/08/2021](https://odysee.com/@veronicatenerezza:a/Reunion-publique-n%C2%B018-du-CSI---CrowdBunker---AUDIOVIDEO18:0)),
entre la létalité (5) du Covid-19 et celle des vaccins génétiques (tous confondus), chez les jeunes.

En résumé, ils ont obtenus :
- Pfizer : **létalité 215 fois plus importante** que la Covid-19
- Moderna : **létalité 1250 fois plus importante** que la Covid-19

[Voir la conférence >>](https://odysee.com/@veronicatenerezza:a/Reunion-publique-n%C2%B018-du-CSI---CrowdBunker---AUDIOVIDEO18:0)


----

> **Notes explicatives :**
> 
> (1) Valeur redressée par un facteur 10. Ceci afin de compenser la sous-déclaration,
> en estimant que seulement 10% des cas réels sont remontés en pharmacovigilance.
>
> (2) Nombre de personnes vaccinées, au moins une dose :
> - Soit ce nombre est inclut dans le point de situation de l'ANSM
> - Soit il est calculé à l'aide du site [vaccintracker](https://covidtracker.fr/vaccintracker/) ou du
    >   [tableau de bord de vaccination](https://solidarites-sante.gouv.fr/grands-dossiers/vaccin-covid-19/article/le-tableau-de-bord-de-la-vaccination), en prenant la date
    >   à __15 jours AVANT__ la date de fin de période, pour tenir compte du délai de déclaration et de traitement de pharmacovigilance.
>
> (3) Comment récupérer ces chiffres ? 
> [<i class="bi-youtube"></i> Vidéo explicative (10 min)](https://odysee.com/@bibule:2/Pharmacovigilance-UE:7)
> |
> [<i class="bi-youtube"></i> Vidéo alternative (4min)](https://reaction19.fr/reaction19/etudes-statistiques/tutoriel-acces-et-elaboration-excel-des-donnees-eudravigilance/)
> <br/>
> [<i class="bi-box-arrow-up-right"></i> Accès direct au site de l'UE](https://www.adrreports.eu/en/search_subst.html)
>
> (4) Pour obtenir les chiffres des décès sur toute la France, on utilise les proportions 
> de décès donnés par les centres régionaux, en les appliquant au nombre de cas graves
> donné par l'ANSM au niveau échelle national.
> 
> (5) **Létalité** : Ensemble des conditions qui rendent nécessairement mortelles une plaie, une lésion ou une maladie. ([définition Larousse](https://www.larousse.fr/dictionnaires/francais/l%c3%a9talit%c3%a9/46785))

----

<i class="bi-alarm"></i> Historique de modifications :

| Date | Auteur | Modifications |
|--- |--- |--- |
| 01/09/2021 | B.Lavenier | Création |
| 03/09/2021 | B.Lavenier,<br/>A.Aumont | MAJ des chiffres l'UE au 03/09/2021 |
| 13/09/2021 | B.Lavenier,<br/>A.Aumont | MAJ des chiffres l'UE au 10/09/2021 |



