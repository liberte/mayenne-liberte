# Variole du singe (Monkeypox) : vous avez dit bizarre ?

<img src="/img/perrone.jpg" class="invisible">

Christian Peronne, professeur de médecine, infectiologue, ancien vice-président à l'OMS du groupe d'experts européens sur la vaccination (ETAGE)
nous livre ses interrogations sur l'actualité de la variole du singe :

![message](/doc/2022-05-22-variole_singe_peronne.jpg)

<p class="text-center">
    <a type="button" class="btn btn-primary" href="/doc/2022-05-22-variole_singe_peronne.jpg" target="_blank"> 
      <i class="bi-file"></i> Télécharger l'image
    </a>
</p>



Sur ce sujet de la variole du singe, nous vous invitons à consulter l'article sur le site de l'AIMSIB 
(Association Internationale pour une Médecine Scientifique Indépendante et Bienveillante) au lien ci-dessous :

https://www.aimsib.org/2022/05/22/les-espagnols-risquent-de-mourir-en-masse-du-monkeypox


