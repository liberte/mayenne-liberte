# Que voulons-nous ?

J'essaie de transformer ma colère en courage, et de prendre la plume plus calmement que dans [la lettre ouverte aux élus](./2021-04-01-lettre_ouverte_aux_elus.html).

Avez-vous remarqué, que quand quelqu'un prend un véhicule, il sait généralement où il veut aller ?
Éventuellement, si la destination est nouvelle, la route est choisie grâce au GPS. Si le temps presse, on passe par une autoroute, mais on peut aussi préférer les petites routes...
Mais, à un moment donné, la route et la destination sont connues et le voyage commence...

Mesdames et messieurs, **savez-vous où vous voulez aller ?**

Vous êtes montés à bord du véhicule de ce mouvement de contestation, mais savez-vous ce que vous voulez y trouver, au fond ?
Connaissez-vous les types de routes que vous voulez éviter ou prendre ? Avez-vous réfléchi à vos contraintes ? De temps,
familial, du travail, de votre santé ? Tout simplement, à vos besoins et limites ?

Peut-être croyons-nous que tous, ici, avons la même destination et les mêmes moyens pour y arriver.
Cela paraîtrait idéal. Mais peut-être que non. Peut-être que pour certains, l'im-Passe Sanitaire est le seul problème à régler;
alors que pour d'autres, c'est simplement la goutte de trop, dans une coupe déjà trop pleine.

Aussi, je vous propose de **prendre le temps de réfléchir**, personnellement ou en groupe, à cette question, qui me semble légitime :
_**"Quel est mon objectif ?"**_ 

- Ici, en venant aux manifestations locales ?
- Dans un éventuel groupe local ?
- Et pour les manifestations parisiennes à venir ? Est-ce que j'y participe ? A quoi suis-je prêt la-bas ? A quoi je m'attends. 
- Qu'est-ce que je veux, ou pas.
- Quelles sont mes limites ?

Si vous ne voulez pas être déçu par ce mouvement naissant, l'idée est d'y apporter ce que vous voulez y voir. Clarifier l'objectif.
Peut-être parmi nous, surement même, il y aura plusieurs objectifs et autant de routes pour y parvenir (même si _"tous les chemins mènent à Rome"_, parait-il !).

J'aimerai tant que chaque personne, ici, se sente bien dans son action.
Que chacun puisse trouver sa place, sans en être dévier ou manipuler, sans regret ni déception ;
sans être emporté contre son gré dans des chemins non désirés et non désirables. 
Est-ce peut-être un voeu pieux ?

J'aimerai que chacun se sente libre.

Que nous soyons libres.

Que nous devenions toujours plus libres. En conscience.

J'en termine en disant que, pour moi, [_"Le but, c'est le chemin"_](https://www.jepense.org/le-but-cest-le-chemin-interpretation/).

Je vous remercie.

<p style="text-align: end">
Laval, le 31 juillet 2021.<br/>
Benoit Lavenier 
</p>
