# Pique-nique inter-collectifs Pays de Loire

<img src="/doc/2022-06-26-miniature_pique_nique.jpg" class="invisible">

Rassemblement sous forme de pique-nique, pour faire connaissance, créer du lien, 
se soutenir, dans la bonne humeur.

Organisé par le [Collectif Santé Mauges](https://www.collectifsantemauges.fr)

Lieu : La Pouplinière, 49111 ST Remy en Mauges

![flyer](/doc/2022-06-26-flyer_pique_nique.jpg)

<p class="text-center">
    <a type="button" class="btn btn-primary" href="/doc/2022-06-26-flyer_pique_nique.jpg" target="_blank"> 
      <i class="bi-file-pdf"></i> Télécharger le flyer
    </a>
</p>



