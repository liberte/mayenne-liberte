# Conférence de Laurent Mucchielli / Cholet (49)

<img src="/doc/2022-06-13-conf_laurent_mucchielli.jpg" class="invisible">

Laurent Mucchielli est sociologue, directeur de Recherches au CNRS.
> Après deux ans de recherche et de controverses, Laurent Mucchielli publie l’essentiel d’une enquête collective sur la
gestion politico-sanitaire de la crise du Covid sous forme d’un livre en 2 tomes :
>
> **« La Doxa du Covid : Peur, Santé, Corruption et Démocratie »**
> 
> Nous aurons le plaisir de l’accueillir et d’échanger avec lui, afin d'enrichir notre réflexion et de clarifier notre compréhension.

Organisé par le [Collecif Santé Mauges](https://www.collectifsantemauges.fr/so/26O4L6vrv#/main)

Lieu : Lundi 13 juin à 20h30 Salle Paul Valéry à Cholet

![flyer](/doc/2022-06-13-conf_laurent_mucchielli.jpg)

<p class="text-center">
    <a type="button" class="btn btn-primary" href="/doc/2022-06-13-conf_laurent_mucchielli.pdf" target="_blank"> 
      <i class="bi-file-pdf"></i> Télécharger le flyer (PDF)
    </a>
</p>



