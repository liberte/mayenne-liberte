# Conseil Scientifique Indépendant (CSI) n°22
## 9 sept. 2021

"Impact des conflits d’intérêts sur les décisions politiques liées a la covid" (Carine Montaner, députée d'Andorre),
"Les leçons à tirer d'Israël" (Pierre Chaillot, statisticien)

![thumbnail](https://spee.ch/f/3638074104998164.jpg?quality=85&height=220&width=390)

https://odysee.com/@ID_1974:9/CSI22:9
