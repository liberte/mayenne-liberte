# Conseil Scientifique Indépendant (CSI) n°19
## 19 août 2021

"Analyse des données de la base de données de pharmaco-vigilance américaine (VAERS)" par Christine Cotton;
"Lien de causalité en pharmaco-vigilance et en justice entre un effet indésirable et un médicament suspect" par Amine Umlil.

![thumbnail](https://spee.ch/f/ca3345bf10626187.png?quality=85&height=220&width=390)

https://odysee.com/@MascaradeetleCoronavirus:c/10000000_1464582253919155_4525088248800661516_n:0