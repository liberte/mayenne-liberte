# Conseil Scientifique Indépendant (CSI) n°15
## 22 juillet 2021

Chaque jeudi, des scientifiques indépendants analysent la situation sanitaire, méthodiquement et calmement.
"A quoi tient scientifiquement le pass sanitaire ?", par Vincent Pavan;
"Rétractation de l'article de Walach ?", par Pierre Chaillot;
" Le point juridique sur le pass", par David Guyon.

![thumbnail](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Freinfocovid.fr%2Fwp-content%2Fuploads%2F2021%2F07%2FLive-CSI-du-22-juillet-2021.jpeg&f=1&nofb=1)

https://odysee.com/@DiscreetL:6/R%C3%A9union-publique-n%C2%B015-du-CSI-(Conseil-scientifique-ind%C3%A9pendant):1?
