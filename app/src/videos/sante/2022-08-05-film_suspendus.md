# "Suspendus... Entre deux mondes"
## 5 août 2022

Le film-documentaire “Suspendus… Des soignants entre deux mondes” témoigne de la réalité des soignants suspendus en portant un regard critique et constructif sur notre système de santé actuelle et d’un autre possiblement émergent.

![thumbnail](/gallery/2022-08-05/thumbnail/__2022-08-05-film_suspendus-vignette.png)

https://vimeo.com/707679502
