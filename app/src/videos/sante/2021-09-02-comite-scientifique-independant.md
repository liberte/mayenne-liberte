# Conseil Scientifique Indépendant (CSI) n°21
## 2 sept. 2021

"Essai du vaccin Pfizer sur les adolescents" (Hélène Banoun),
"Plainte contre X déposée contre les fausses études scientifiques" (Association Reinfo Liberté: Vincent Pavan et Maître Ludovic Heringuez)

![thumbnail](https://spee.ch/f/008fbbfb53ae01a1.png?quality=85&height=220&width=390)

https://odysee.com/@ID_1974:9/CSI21:3
