# Conseil Scientifique Indépendant (CSI) n°18
## 12 août 2021

"Pharmaco-vigilance européenne des vaccins Covid19" par Vincent Pavan, Emmanuelle Darles;
"Point d’étape sur la situation sanitaire" par Eric Menat. 

![CSI #18](https://image-processor.vanwanet.com/optimize/s:390:220/quality:85/plain/https://cdn.lbryplayer.xyz/speech/836e80c92aa64466:9.png)

https://odysee.com/@veronicatenerezza:a/Reunion-publique-n%C2%B018-du-CSI---CrowdBunker---AUDIOVIDEO18:0
