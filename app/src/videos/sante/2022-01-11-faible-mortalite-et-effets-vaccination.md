# "La vaccination à favoriser l'augmentation du nombre de cas Covid"
## 11 janv. 2022

"Analyse objective des effets limités de la vaccination sur l'épidémie" (Pr Didier Raoult),

![thumbnail](/gallery/2022-01-11/thumbnail/2022-01-11-IHU.jpg)

https://www.youtube.com/watch?v=_x-eozrCCNM
