# Conflits d'intérêts et arnaque de l'espérance de vie 
## 20 juillet 2021

Le professeur Didier Raoult, directeur de l'IHU Méditerranée Infection, explique la réalité des conflits d'intérêt 
dans la médecine. 

![thumbnail](https://i.pinimg.com/736x/44/1c/04/441c046e8c9c6ecddeda87dffa20ea76.jpg)

https://www.youtube.com/watch?v=ZeSHT87hzC4
