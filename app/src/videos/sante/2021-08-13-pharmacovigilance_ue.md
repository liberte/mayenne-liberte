# Vaccins: comment obtenir les chiffres de pharmacovigilance de l'UE 
## 13 août 2021

Voici une courte vidéo (10 min) pour comprendre comment récupérer les données,
depuis le site officiel de pharmacovigilance de l'Union Européenne. 

![Vaccins Covid-19](https://vidalactus.vidal.fr/public/images/actus/PHARMACOVIGI-1215061073.jpg)

https://odysee.com/@bibule:2/Pharmacovigilance-UE:7
