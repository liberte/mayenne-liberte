# "Attention aux enfants"
## 5 juillet 2021

Alexandra Henrion-Caude est chercheur, docteur en génétique, ancienne post-doctorant de Havard, ancienne chercheur puis directrice de recherche à l'INSERM,
actuellement directrice d'un institut de recherche, spécialiste de l'ARN non codant.

![thumbnail](https://spee.ch/2/20e6f4f06a51b8e5.png?quality=85&height=220&width=390)

https://odysee.com/@RenardBute:3/interviewalexandrahenrioncaude1:4
