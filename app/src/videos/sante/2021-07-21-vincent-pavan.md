# "Nous entrons dans une dictature scientiste"
## 21 juillet 2021

Vincent Pavan est enseignant chercheur en mathématique, il a co-fondé la Conseil Scientifique Indépendant (CSI).
Il a publié de nombreuses analyses contradictoires sur les chiffres du Covid, prouvant par exemple les effets négatifs 
du confinement sur la mortalité.

![Vincent Pavan](https://image-processor.vanwanet.com/optimize/s:390:220/quality:85/plain/https://cdn.lbryplayer.xyz/speech/9fb17730e9828a42:a.png)

https://odysee.com/@nowunmondemeilleur:4/vincentpavan:d
