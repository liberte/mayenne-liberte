# "Ce n'est plus de la science"
## 13 juillet 2021

Professeur Christian Perrone revient sur la crise du Covid, et la dérive progressive de la médecine et de l'administration
qui l'organise.

![thumbnail](/img/perrone.jpg)

https://odysee.com/@WakeUp:d/Premie%CC%80re-consultation-Professeur-Christian-Perronne:a
