# Docteur Louis Fouché: la force de la non-violence

"La non-violence, c'est un travail sur la relation. Sur la relation à soi d'abord avant même de vouloir travailler sur la
relation à l'autre. C'est se dire que ma rencontre avec la matière va être quelque chose de violent, potentiellement. 
La non-violence qui serait conçue comme une éradication complète de la violence, pour moi, c'est une illusion.
La non-violence, c'est plutôt une transformation."

![thumbnail](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2F5idRA5lHG0Y%2Fmaxresdefault.jpg&f=1&nofb=1)

https://odysee.com/@comprendresonsoietlesautres:7/rencontre-avec-louis-fouche:9
