# "Un cataclysme social et sociétal sans précédent"
## 26 juillet 2021

Stéphane Ravier, maire et sénateur, dénonce ce qu'il appelle le "Passe de la Honte", 
qui instaure le nouveau tryptique de la République : "Surveillence, Différence et Méfiance". 

![thumbnail](/img/ravier.png)

https://www.youtube.com/watch?v=rYX2y2sCbgY&list=PLucUTNESI_1tl6reVF4u0Xe3sgNMDyvl2
