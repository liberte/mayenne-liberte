# "Il n'y a pas de consensus scientifique sur la vaccination de masse"
## 13 octobre 2021

D'une manière très claire, Laurence Muller-Bronn, sénatrice au Bas-Rhin, explique son "non"
à la proposition de loi sur l'obligation vaccinale.

![thumbnail](/img/muller_bronn.png)

https://odysee.com/@francesoir:2/LaurenceMullerBronn:4?&sunset=lbrytv
