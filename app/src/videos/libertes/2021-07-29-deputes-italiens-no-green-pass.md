# Révolte des députés italiens, en pleine assemblée !
## 29 juillet 2021

Des députés italiens (groupe parlementaire centre-droit) se révoltent, refusant la "prolongation de l'état d'urgence (...), 
limitation des libertés individuelles, restrictions massacrant l'économie.

![thumbnail](/gallery/2021-07-29/thumbnail/italie-no-green-pass.png)

[Révolte des députés italien, en pleine assemblée !](/gallery/2021-07-29/italie-no-green-pass.mp4)

