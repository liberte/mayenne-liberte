# "La peur et la propagande empêchent les Français de contester !"
## 20 décembre 2021

D'une manière très claire, Nicolas Dupont-Aignan, député de l'Essonne, explique l'illogisme de la situation actuelle :
un passe sanitaire qui ne fonctionne pas, une vaccination utile seulement pour les personnes âgées et à risque

![thumbnail](/img/dupont_aignan.jpg)

https://www.youtube.com/watch?v=S1K8P5gCiHg
