# Suivez-nous

<i class="bi-facebook"></i> Facebook : [Collectif Mayenne Défense des Libertés](https://www.facebook.com/groups/1853356128159103)

<i class="bi-telegram"></i> Télégram : [Groupe principal](https://t.me/joinchat/MX8Xr62UxFgzNzg0) | [Autres groupes](/pages/groupes.html)

> [Télégram](https://telegram.org/) est une application gratuite (téléphone ou ordinateur). <img align="right" width="56" height="56" src="/img/logo-telegram.png">
> 
> Une fois l'application installée, cliquez le lien du groupe (ci-dessus) pour y accéder.

<i class="bi-twitter"></i> Twitter :
- [#MayenneLiberte](https://twitter.com/search?q=%23MayenneLiberte)
- [#AntiPassSanitaire #Mayenne](https://twitter.com/search?q=%23AntiPassSanitaire%20%23Mayenne&src=typed_query)
- [#AntiPassSanitaire #Laval](https://twitter.com/search?q=%23AntiPassSanitaire%20%23Laval&src=typed_query)
- [#NonAuPassSanitaire #Laval](https://twitter.com/search?q=%23NonAuPassSanitaire%20%23Laval&src=typed_query)
