var domainroot = "www.mayenne-liberte.fr"
var modals = {
  signers: false,
  elus: false
};
var cacheMaxAgeMs = {
  // Session duration = MAX all pages duration
  session: 2 * 24 * 60 * 60 * 1000, // 2 day

  // HOME page max age
  home: 30 * 60 * 1000, // 30 min

  // Max age any other HTML page
  default: 24 * 60 * 60 * 1000, // 1 day

  timestamp: 10 * 60 * 1000 // 10 min
};

function doSiteSearch(form){
  form.q.value="site:" + domainroot + " " + form.qfront.value;
}

function initNanoGallery2() {

  console.info('[app] Init nanogallery2...');
  $('[data-nanogallery2]').nanogallery2();
}

function initSession() {
  console.info('[app] Init session');
  try {
    // Get the session id (= a time in MS)
    var sessionId = getSessionId() || 0;

    // Check if session expires
    var sessionAge = Math.abs(Date.now() - sessionId);
    if (sessionAge > cacheMaxAgeMs.session) {
      // Create a new session
      createNewSession();
      return; // end (page will be reloaded)
    }

    var isHomePage = window.location.pathname === '/';

    // Check path age
    var pathTime = getPathTime(window.location.pathname);
    var pathAge = Math.abs(Date.now() - pathTime);
    var pathMaxAge = isHomePage
      ? cacheMaxAgeMs.home
      : cacheMaxAgeMs.default;
    if (pathAge > pathMaxAge) {
      reloadPage();
      return; // end
    }

    // Home page: check if there is an update
    if (isHomePage) {
      console.debug('[app] Checking server updates...');
      getTimestamp({
        success: (timestamp) => {
          console.debug('[app] Server timestamp: ' + timestamp);
          if (timestamp > pathTime) {
            const deltaTimestampToPath = timestamp - pathTime;
            if (deltaTimestampToPath > cacheMaxAgeMs.timestamp) {
              // Force to invalidate the session
              console.info('[app] Server has updates: reload session...');
              createNewSession();
            }
          }
        },
        error: (err) => {
          console.error('[app] Cannot load server timestamp', err);
        }
      });
    }

    // Add sessionId to all links
    $("a").attr('href', function(i, href) {
      if (!href || href === '#') return; // Skip

      const isSameOrigin = href.startsWith(window.location.origin)
        || (!href.startsWith('http://') && !href.startsWith('https://') && !href.startsWith('@@'));
      if (!isSameOrigin) return href; // Skip

      console.debug('[app] Adding sessionId to href: ' + href);
      const hashIndex = href.indexOf('#');
      const hash = (hashIndex !== -1) ? href.substring(hashIndex) : '';
      href = (hashIndex !== -1) ? href.substring(0, hashIndex) : href;
      // TODO: manage hash ?
      return href
        + (href.indexOf('?') !== -1 ? "&sessionId=" + sessionId : "?sessionId=" + sessionId)
        + hash;
    });

  } catch(e) {
    console.error(e)
  }
}

function initMenu() {
  console.info('[app] Init menu behavior');

  const mobile = window.matchMedia("(max-width: 767px)").matches;
  const url = window.location.pathname;
  // create regexp to match current url pathname and remove trailing slash if present as it could collide with the
  // link in navigation in case trailing slash wasn't present there
  const urlRegExpStr = url === '/'
    ? window.location.origin + '/?$'
    : url.replace(/\/$/,'').replace(/\.html$/,'')
    + '(\.html)?$';
  const urlRegExp = new RegExp(urlRegExpStr);
  console.info('[menu] Detecting active menu item, for url: ' + url, urlRegExpStr);

  $('.navbar-nav li.nav-item a.nav-link').each(function(){
    // and test its normalized href against the url pathname regexp
    if(this.href && this.href !== '#' && urlRegExp.test(this.href.replace(/\/$/,''))){
      $(this).addClass('active');
    }
  });
  // Same, for dropdown menu (should enable root nav-item, when a child dropdown-item is active
  $('.navbar-nav li.nav-item.dropdown').each(function(){
    const rootNavItem = this;
    $(this).children('.dropdown-menu').each(function(){
      $(this).children('a.dropdown-item').each(function(){
        if (this.href && this.href !== '#' && urlRegExp.test(this.href.replace(/\/$/,''))){
          $(this).addClass('active');
          $(rootNavItem).addClass('active');
        }
      });
    })

  });

  if (!mobile) {
    $('.dropdown-show-hover').hover(function(event) {
      console.debug('[menu] Hover dropdown item', event.target);
      $(this).addClass('show');
      $(this).children('.dropdown-menu').addClass('show');
    },
    function() {
      $(this).removeClass('show');
      $(this).children('.dropdown-menu').removeClass('show');
    });
  }
  else {
    $('.dropdown-show-hover>a').click(function(event) {
      console.debug('[menu] Click dropdown item', event.target);
      $(this).addClass('show');
      var children = $(this).children('.dropdown-menu');
      if (children.length) {
        children.addClass('show');
      }
    });
  }
}

function initTooltip() {
  console.info('[app] Init tooltip behavior');
  $('[data-toggle="tooltip"]').tooltip()
}

function getSigners(params) {
  $.ajax({
    cache: false,
    type: 'GET',
    url: '/data/signatures.json',
    dataType: 'json',
    success: function(data) {
      data = $.map(data, function (item, i) {
        item['Séquentiel'] = i+1;
        item['Nom'] = (item['Nom'] || '').toUpperCase();
        item['Ville'] = (item['Code postal'] || '') + ' ' + (item['Ville'] || '').toUpperCase();
        return item;
      });
      params.success(data);
    },
    error: function(err) {
      params.error(err);
    }
  })
}

function openSignersModal() {

  console.debug('[app] Opening signers modal...')
  // Load
  if (modals.signers === false) {
    $('#signers-table').bootstrapTable({
      loadingTemplate: () => "Veuillez patientez...",
      sortable: true,
      ajax: getSigners,
      onLoadSuccess: (_) => {
        modals.signers = true;
        $('#signers-modal').modal();
      }
    });
  }
  else {
    $('#signers-modal').modal('toggle');
  }
}

function getElus(params) {
  $.ajax({
    cache: false,
    type: 'GET',
    url: '/data/elus.json',
    dataType: 'json',
    success: function (data) {
      data = $.map(data, function (item, i) {
        item['Email'] = (item['Email'] || '').split(';').join('<br/>');
        return item;
      });
      params.success(data);
    },
    error: params.error
  })
}

function openElusModal() {
  console.debug('[app] Opening élus modal...')

  // Load
  if (modals.elus === false) {
    $('#elus-table').bootstrapTable({
      loadingTemplate: () => "Veuillez patientez...",
      sortable: true,
      ajax: getElus,
      onLoadSuccess: (_) => {
        modals.elus = true;
        $('#elus-modal').modal();
      }
    });
  }
  else {
    $('#elus-modal').modal('toggle');
  }
}

function getSessionId() {
  // Read ID
  var sessionId = getStorage().getItem("sessionId");
  if (!sessionId) {
    var searchParams = getSearchParams();
    sessionId = searchParams['sessionId'];
  }
  if (sessionId) {
    console.debug('[app] Found a session ID = ' + sessionId);
  }
  return sessionId && parseInt(sessionId) || undefined;
}

function setSessionId(sessionId) {
  console.info('[app] New sessionId = ' + sessionId);

  var storage = getStorage();

  try {
    // Clean previous session storage
    cleanStorage(storage);

    // Store session to local storage
    storage.setItem("sessionId", sessionId);
  }
  catch (e) {
    console.error(e);
  }

  // Redirect to an URL with sessionId
  var searchParams = getSearchParams();
  searchParams.sessionId = sessionId;
  delete searchParams.time;
  var newUrl = window.location.origin
    + window.location.pathname
    + getSearchString(searchParams)
    + window.location.hash;


  console.debug('[app] Force reloading session: ' + newUrl);
  window.location.replace(newUrl);

}

function createNewSession(newSessionId) {
  // Generate a new session id (=now)
  const sessionId = newSessionId || Date.now();
  // Store it (will reload)
  setSessionId(sessionId);

  return sessionId;
}

function getPathTime(path) {
  // Read path time
  path = path || window.location.pathname;
  var storage = getStorage();
  var pathTime = storage.getItem(path);
  if (pathTime) return parseInt(pathTime);

  pathTime = Date.now();
  try {
    storage.setItem(path, pathTime.toString());
  }
  catch(e) {
    console.error(e);
  }
  return pathTime;
}

function reloadPage(path, sessionId) {
  path = path || window.location.pathname;
  var pathTime = Date.now();
  sessionId = sessionId = sessionId || getSessionId();
  var storage = getStorage();

  try {
    // Store path time into the storage
    storage.setItem(path, pathTime.toString());
  }
  catch (e) {
    console.error(e);
  }

  var searchParams = getSearchParams();
  searchParams.sessionId = sessionId; // Force a server reload, and browser cache reload
  searchParams.time = pathTime;

  // Compute the new path
  var newUrl = window.location.origin
    + path
    + getSearchString(searchParams)
    + window.location.hash;

  console.debug('[app] Force reloading page: ' + newUrl);

  // Reload, with a history replacement
  window.location.replace(newUrl);
}

function getSearchParams() {
  var paramMap = {};
  var queryString = window.location.search;
  if (queryString && queryString.startsWith('?')) {
    try {
      queryString = (queryString.substr && queryString.substr(1))
        || (queryString.substring && queryString.substring(1));
      var paramArray = queryString.split('&');
      for (var i= 0 ; i<paramArray.length; i++) {
        var paramSpec = paramArray[i];
        var parmParts = paramSpec.split('=');
        var paramName = parmParts[0];
        var paramValue = parmParts[1] || true;
        paramMap[paramName] = paramValue;
      }
    }
    catch (e) {
      console.error(e);
    }
  }
  return paramMap;
}

function getStorage() {
  return localStorage || sessionStorage;
}

function cleanStorage(storage) {
  storage = storage || getStorage();

  console.info('[app] Cleaning storage...')
  try {
    // Clean every key's value'
    while (storage.length) {
      const key = storage.key(0);
      storage.removeItem(key);
    }
  }
  catch (e) {
    console.error(e)
  }
}

function getSearchString(searchParams) {
  searchParams = searchParams || getSearchParams();
  return Object.keys(searchParams).reduce((res, key, index) => {
    var value = searchParams[key] || true;
    return res
      + (index > 0 ? '&' : '?')
      + key + '=' + value;
  }, '');
}


function getTimestamp(params) {
  $.ajax({
    cache: false,
    type: 'GET',
    url: '/timestamp.txt',
    dataType: 'text',
    success: function(data) {
      try {
        if (data && data.length >= 13) {
          const timestamp = data && parseInt(data);
          if (params.success) params.success(timestamp);
        }
        else {
          if (params.error) params.error("[app] Invalid timestamp found: " + data);
        }
      }
      catch(err) {
        console.error(err);
        if (params.error) params.error(err);
      }
    },
    error: function(err) {
      if (params.error) params.error(err);
    }
  })
}

function init() {
  $(document).ready(function(event) {
    initSession();
    initMenu();
    initTooltip();
    initNanoGallery2();
  });
}
