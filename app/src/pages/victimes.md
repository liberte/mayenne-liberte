# Victimes des vaccins Covid-19

Vous êtes victime d'effet indésirable, suite à un vaccin génétiques Covid-19 ?<br/>
Un de vos proches est décédé brusquement suite à une injection ?

## Contacter notre équipe 

Notre équipe écoute et conseil les victimes des vaccins génétiques. L'objectif est de vous soutenir, 
mais aussi de pouvoir agir collectivement et d'alerter les instances départementales des risques de ces vaccins.

Nous vous accompagnerons autant que possible, en essayant de répondre à vos questions :

> Coordinateur "victimes des vaccins" :<br/>
> **Brigitte Lesage** - 06.68.87.67.65

## Appel à témoins au niveau national

Nous vous encourageons à faire remonter les effets indésirables des vaccins génétiques Covid-19, en répondant aux appels
à témoins suivants :
 - https://association-victimes-coronavirus-france.org/appel-a-temoins-pour-victimes-des-vaccins-c
ovid-19/
 - https://recensementeffetsindesirable.fr/
