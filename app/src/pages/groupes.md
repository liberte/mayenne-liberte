# Se rencontre pour agir

_"Seul, on va plus vite. Ensemble, on va plus loin"_ (Proverbe africain)

_"L'action est l'antidote du désespoir"_ (Joan Baez)

## Rejoindre un groupe local

Ne restez pas seul !! A proximité de votre commune, nous vous proposons de rencontrer d'autres personnes
impliquées dans la défense des libertés.

Pour cela, 2 étapes :

- Installer l'application [Télégram](https://telegram.org/),  

  > [Télégram](https://telegram.org/) est une application gratuite pour téléphone ou ordinateur. <img align="right" width="56" height="56" src="/img/logo-telegram.png">
  > <br/><br/>
  > Elle est moins censuré que d'autres (FaceBook, WhatApps, etc.), c'est pourquoi nous l'utilisons.

- Puis rejoignez 
  * le groupe principal : https://t.me/joinchat/MX8Xr62UxFgzNzg0
  * et le groupe le plus proche de chez vous : 

| Secteur | Groupe Telegram   |
|------------------------|-------------------|
| Laval<br/>(nommé "RSA Laval") | https://t.me/joinchat/LerguxhXPyNLSgz24Qc_cw <br/>|
| Chateau-Gontier | https://t.me/joinchat/ghQDmdKUsdszMTE0 |
| Mayenne | https://t.me/joinchat/Mcc_bVWmVCZkYzU0 |

> Vous voyez une erreur, ou un oubli ?<br/>
> Contactez **Benoit** - 06.62.86.37.82

## Rejoindre un groupe par profession

Liste des groupes par profession : 

| Profession (salariée) | Groupe Telegram   |
|------------|-------------------|
| Médical, Santé | https://t.me/joinchat/Z7Zcu02CcFQyMTQ0 | 
| Education nationale | https://t.me/joinchat/4q51Xj2vSVs5MWM0 |
| <i class="bi-music-note-beamed"></i> Musiciens, artistes, culture | https://t.me/joinchat/-8D1YHg2kF00MDI8 |
| Autres salariés | https://t.me/joinchat/QLz3ylScHw9jYzU0 |


| Profession (libérale) | Groupe Telegram   |
|----------------|-------------------|
| Artisans, auto-entrepreneurs, exploitants agricoles, autres situations | https://t.me/joinchat/1lCJEXSANIk2YWE8 |
| Médical, Santé | à venir ? |

> Vous voyez une erreur, ou un oubli ?<br/>
> Contactez **Christine** - 06.66.56.25.64

<p>
@@include('./equipes.html')
</p>
