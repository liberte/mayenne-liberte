# Équipe de communication



Photographie, vidéo, mise à jour de site web, graphisme (pour les flyers), etc.
si vous avez l'une de ces compétences : vous êtes le bienvenu !

Nous sommes dans un monde où le visuel compte beaucoup : **nous avons besoin de vous !**

> Pour nous rejoindre ?<br/>
> [Contactez-nous !](/contact.html)  

