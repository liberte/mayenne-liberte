# Juridique

Vous avez une question juridique ? Vous voulez vous défendre face à votre employeur ?
Cette page est faite pour vous

Voici un récapitulatif des actions juridiques et des modèles de lettres à votre disposition, au niveau national.
_(Mise à jour le 26/08/2021)_

## Guide juridique complet

La Ligue nationale pour la liberté des vaccinations (LNPLV) vient de publier un « Guide juridique pour la 
liberté vaccinale » ( de 40 pages) pour aider les citoyens à résister au Pass et à la vaccination forcée :

[![Guide juridique PNPLV](../img/guide_juridique.png)](https://www.infovaccin.fr/guide-juridique.html)

  📄 PDF : [Version 1.3](https://www.infovaccin.fr/csx/scripts/downloader2.php?filename=T004/fichier/b7/65/mqo5rs2ybu0p&mime=application/pdf&originalname=GUIDE_JURIDIQUE_pour_la_LIBERTE_VACCINALE_V1.3.pdf)

## ADSPE 

Les actions actuelles de l'[association ADSPE](https://www.adspe.fr), en lien avec le Cabinet Di Vizio, revêtent deux formes différentes :
  
- [Des plaintes](https://www.adspe.fr/actions/plaintes/) à l'encontre de personnalités ou organisations impliquées dans la gestion de la pandémie :

  - Contre [Jean-Michel Blanquer](https://www.adspe.fr/plainte-contre-jean-michel-blanquer/) pour discrimination et extorsion;

  - Contre [Jean Castex et Olivier Véran](https://www.adspe.fr/plainte-contre-jean-castex-et-olivier-veran/) absence de prise de mesures pour combattre un sinistre;

  - Contre [Olivier Véran](https://www.adspe.fr/plainte-contre-olivier-veran/) pour violation des règles déontologiques;

  Nous **vous encourageons à y participer**, dans une stratégie de saturation des procédures judiciaires. Vous ne risquez rien !  


- [Des recours](https://www.adspe.fr/actions/recours/) collectifs et individuels:
  - l'un contre l'obligation vaccinale à destination des personnels médicaux;
  - l'autre contre le pass sanitaire à destination de tous les français.

  Ces deux recours sont actuellement clos (plus de dépot de dossier possible).

## Maitre Virgine De Araujo Recchia

L’actualité des recours et courriers est dispo sur son fil télégram : https://t.me/CIG_VDAR_JURIS
- Plainte au CSA contre Emmanuel LECHYPRE : https://t.me/CIG_VDAR_JURIS/102
- Modèle de lettre proposé pour les actifs et les pensionnaires d'EHPAD afin de refuser l'injection forcée. https://t.me/CIG_VDAR_JURIS/128
- Modèle de lettre de réponse à l'employeur ou la hiérarchie sur une injonction ou directive obligeant à l'injection.
  
  Les formats de fichiers :

  📄 DOCX : https://t.me/CIG_VDAR_JURIS/130
 
  📄 PDF : https://t.me/CIG_VDAR_JURIS/133

  📄 ODT : https://t.me/CIG_VDAR_JURIS/134

- Rappel à la loi :

  👉 Document destiné d'une part à aider la population à argumenter sur le fait que les contrôles de pass sanitaires ne sont pas légaux, où qu'ils aient lieux, et à tenter de faire changer la peur de camp en faisant réfléchir les personnes qui se prêtent à ce contrôle, quelle que soit leur fonction.      

  👉 D'autre part à organiser éventuellement des actions concertées (exemple en page 3 du document). Les 2 premières pages sont destinées à être imprimées pour être utilisées "sur le terrain". https://t.me/CIG_VDAR_JURIS/146

Modèle de lettre de réponse aux autorités scolaires ou universitaires pour les parents incités (parfois avec menaces) à faire « vacciner / injecter » leurs enfants avant la rentrée scolaire ou universitaire. Procédure à suivre et modèle :
- 📑 https://www.legavox.fr/blog/maitre-de-araujo-recchia/modele-lettre-reponse-autorites-scolaires-31104.htm

Modèle de lettre à adresser aux « Défenseur des Droits » pour dénoncer la discrimination, l'obligation vaccinale indirecte,
- 📑 https://t.me/CIG_VDAR_JURIS/152

## ️Association Bon Sens

Représenté par Me Joseph, trois plaintes collectives judiciaires contre l'Ordre des Médécins : https://bonsens.info/plaintes-collectives/
- Vous êtes médecin 👨‍⚕️👩‍⚕️ et vous avez été interdit de prescrire des médicaments spécifiques Covid-19 par l’ordre des médecins,
- Vous n’avez pas reçu d’informations concernant les traitements susceptibles de soigner ?
- Vous considérez avoir été mal soigné de la Covid-19,

Des plaintes collectives pour complicité de délaissement contre l’ordre des médecins seront déposées dans les mains des doyens des juges d’instruction.

## Association des victimes du Coronavirus

- Le recours « porte étroite » (création doctrinale non consacrée par la loi, donc sans obligation d’examen par le CC) de DiVizio au Conseil Constitutionnel https://association-victimes-coronavirus-france.org/download/recours-devant-le-conseil-constitutionnel-concernant-le-pass-sanitaire-et-obligation-vaccinale/?wpdmdl=58207&refresh=61095663ab8ff1628001891
- Plainte contre le Remdesevir https://association-victimes-coronavirus-france.org/plainte-citoyenne-contre-remdesivir/
- Plainte contre les publicités mensongères du vaccin COVID19 : https://association-victimes-coronavirus-France.org/plainte-contre-les-publicites-mensongeres-sur-vaccins-covid19
- Plainte pour extorsion concernant le premier ministre Castex : https://association-victimes-coronavirus-France.org/plainte-contre-le-pass-sanitaire
- Plainte contre M Djebarri concernant l’obligation du pass dans les transports https://association-victimes-coronavirus-France.org/plainte-contre-le-pass-sanitaire

## Plainte individuelle à la Cour Européenne des Droits de l’Homme (CEDH)

Initié par Guillaume Zambrano, Maître de Conférences en droit privé à l’université de Montpellier, suivez la procédure qu'il propose,
pour porter lainte devant la Cour Européenne des Droits de l’Homme (CEDH).

Nous vous encourageons TOUS à déposer cette plainte individuelle.
C’est un recours qui peut être efficace s’il est exercé massivement et qui ne coûte qu’un timbre et une enveloppe.

Procédure individuelle (durée: 10 min) : https://nopass.fr

## En savoir plus ? Contactez notre équipe

- Contactez notre coordinateur :

  > Coordinateur de l'équipe juridique : **Christophe Fiamma** [06.71.23.07.21](tel:06.71.23.07.21)
  > ou sur Facebook: [Christophe le Caribouman](https://www.facebook.com/christophe.lecaribouman)

Vous pouvez également rejoindre [un groupe local](/agir.html) en Mayenne pour connaitre les actions en cours
et agir collectivement.
