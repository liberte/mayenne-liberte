# Parents d'élèves

Vous voulez en savoir plus sur les risques de vaccination chez les plus jeunes ?
Vous rechercher une information claire et loyale, facilement compréhensible, et bien documentée ?
Nous publions ici un résumé des connaissances disponibles sur le sujet, établies à partir de données officielles et sourcées.

**Sommaire :**

[Devons-nous vacciner nos enfants contre le Covid-19 ?](/pages/parents#devons-nous-vacciner-nos-enfants-contre-le-covid-19-)<br/>
[Comparaison de la létalité "Vaccins génétiques" / Covid-19](/pages/parents#comparaison-de-la-létalité-vaccins-génétiques--covid-19)<br/>
[Que dire des tests menés par les labos ?](/pages/parents#que-dire-des-tests-menés-par-les-labos-)<br/>
[Comment puis-je agir ?](/pages/parents#comment-puis-je-agir-)
- [Rejoindre le collectif de parents](/pages/parents#rejoindre-le-collectif-de-parents)
- [Modèle de lettres](/pages/parents#modèle-de-lettres)
- [Visuels à diffuser](/pages/parents#visuels-à-diffuser)

## Devons-nous vacciner nos enfants contre le Covid-19 ?

[![Devons-nous vacciner nos enfants contre le Covid-19 ?](https://www.francesoir.fr/sites/francesoir/files/clip_image001_field_mise_en_avant_principale_1_0_0_0.jpg)](https://www.francesoir.fr/politique-france/devons-nous-vacciner-nos-enfants-contre-le-covid-19)

[Lire l'article (FranceSoir - 02/06/2021) >>](https://www.francesoir.fr/politique-france/devons-nous-vacciner-nos-enfants-contre-le-covid-19)

> _Nous aimons nos enfants et adolescents ?<br/>
> &nbsp;&nbsp;Alors attendons la fin des essais cliniques._

## Comparaison de la létalité "Vaccins génétiques" / Covid-19

Le Conseil Scientifique Indépendant (CSI) a fait une analyse, tous vaccins confondus, des risques de décès suite à la
vaccination anti-Covid, chez les plus jeunes.

En voici la vidéo de présentation de cette analyse :

[![CSI #18](https://image-processor.vanwanet.com/optimize/s:390:220/quality:85/plain/https://cdn.lbryplayer.xyz/speech/836e80c92aa64466:9.png)](https://odysee.com/@veronicatenerezza:a/Reunion-publique-n%C2%B018-du-CSI---CrowdBunker---AUDIOVIDEO18:0)

[Voir la vidéo >>](https://odysee.com/@veronicatenerezza:a/Reunion-publique-n%C2%B018-du-CSI---CrowdBunker---AUDIOVIDEO18:0)

> En résumé :
> - Pfizer : létalité(*) **215 fois plus importante** que la Covid-19
> - Moderna : létalité(*) **1250 fois plus importante** que la Covid-19
>
> (*) **Létalité** : Ensemble des conditions qui rendent nécessairement mortelles une plaie, une lésion ou une maladie. ([définition Larousse](https://www.larousse.fr/dictionnaires/francais/l%c3%a9talit%c3%a9/46785))

## Que dire des tests menés par les labos ?

### Pfizer/BioNTech

Le laboratoire Pfizer/BioNTech [a publié le 22/07/2021 une première étude](https://www.ema.europa.eu/en/documents/variation-report/comirnaty-h-c-5735-ii-0030-epar-assessment-report-variation_en.pdf)
sur son vaccin, **vis-à-vis des 12-15 ans**. Cette tranche d'âge n'avait pas jusqu'ici fait l'objet d'étude particulière.

**Cette étude révèle de nombreuses zones d'ombre et incohérences**, et porte à croire que ce vaccin n'est pas du tout "sûr"
comme le prétend pourtant le laboratoire. Malgré cela, l'Agence Européenne du Médicament (EMA) a donné son accord,
sans aucune contre-analyse indépendante, ouvrant ainsi la vaccination Pfizer des 12-15 ans...

Voici une critique de cette étude, par Hélène Banoun, membre du Conseil Scientifique Indépendant (CSI) :

[![CSI #21](https://spee.ch/f/008fbbfb53ae01a1.png?quality=85&height=220&width=390)](https://odysee.com/@ID_1974:9/CSI21:3)

[Voir la vidéo >>](https://odysee.com/@ID_1974:9/CSI21:3)

> En résumé :
> - **Nombreux effets indésirables** sur les 1 131 participants vaccinés :
    >   **la sécurité du vaccin Pfizer n'est pas établie** sur les 12 à 15 ans.
>
> - **Efficacité non établie** : explosion de Covid post-vaccinales, sans doute dues à l'ADE (*);
>
> - Pas de suivi à long terme : la moitié des participants suivis sur moins de 2 mois seulement.
>
> (*) ADE : facilitation et/ou aggravation de l'infection par les anticoprs vaccinaux.

Pourquoi l'EMA a t elle autorisé un médicament expérimental qui n'a pas une efficacité prouvée sur les plus jeunes ?

### Moderna

Moderna a [publiée le 08/06/2021 une étude](https://www.ema.europa.eu/en/news/ema-evaluating-use-covid-19-vaccine-moderna-young-people-aged-12-17)
sur l'impact de son vaccin sur les 12-17 ans.
Suite à ce document, l'Agence Européenne du Médicament (EMA) a donné son accord, sans contre-analyse indépendante, pour 
l'usage du vaccin sur cette population.

_La critique objective de cette étude est cours, par le Conseil Scientifique Indépendant.
Résultats publiés ici, sous peu._

## Comment puis-je agir ?

### Rejoindre le collectif de parents

**Vous avez des questions** sur la vaccination des enfants et adolescents ?<br/>
Vous souhaitez **rencontrer d'autres parents**, près de chez vous ?

- Contactez notre coordinateur "Parents d'élèves 53" :

  > Coordinateur "Parents d'élèves 53" :<br/>
  > **Hélène** - 06.79.42.34.99

- ou rejoignez d'autres parents en Mayenne, sur Telegram: https://t.me/joinchat/9lrEkXUDHk5kNjI8 <br/>
  (l'application Telegram est gratuite).

### Modèle de lettres

Vous refusez la vaccination de vos enfants ? Voici des modèles de lettre pour signifier votre refus :

- Avenant dérogatoire à la fiche infirmerie : [Lire l'article >>](https://reinfocovid.fr/operations_speciales/tests-covid-dans-les-ecoles-avenant-a-la-fiche-infirmerie/)

  📄 [Télécharger l'avenant (PDF)](../doc/avenant_derogatoire_fiche_infirmerie.pdf)

- Pour refuser la vaccination face aux autorités scolaires ou universitaires : [Lire l'article >>](https://www.legavox.fr/blog/maitre-de-araujo-recchia/modele-lettre-reponse-autorites-scolaires-31104.htm)

  📄 [Lettre au chef d'établissement - Télécharger](https://partage-public.avocat.fr/ajax/share/05235ad205086c4f52375a35086c44f99dc4e9c7f1e33a8c/1/8/Mzg/MzgvMTA2)

  📄 [Lettre au Défenseur des droits - Télécharger](https://partage-public.avocat.fr/ajax/share/0b803ad00a425c47b8015a1a425c496cb0927d73d69a403d/1/8/Mzc/MzcvMTA5)

- Choix vaccinal pour l’enfant de parents séparés/divorcés : [Lire l'article >>](https://bonsens.info/choix-vaccinal-pour-lenfant-de-parents-separes/)

### Visuels à diffuser

Vous pouvez télécharger puis imprimer les visuels suivants (au format tract ou affiche), 
pour sensibiliser les autres parents.

_Cliquez sur une image pour télécharger le fichier en grande résolution_

[![Je l'aime, j'attends](../doc/je_laime_jattend-small.png)](../doc/je_laime_jattend-v1.png)

> Note : Le QRCode de l'affiche ci-dessus renvoie vers [cet article >>](https://www.francesoir.fr/politique-france/devons-nous-vacciner-nos-enfants-contre-le-covid-19)

[![Je l'aime, j'attends. Agissez !](../doc/je_laime_jattend_agissez-small.png)](../doc/je_laime_jattend_agissez-v1.png)

[![Affiche "Vacciner les jeunes"](../doc/vacciner_les_jeunes-small.png)](../doc/vacciner_les_jeunes-v1.png)

Un dernier visuel, à utiliser par exemple au verso d'un format tract :

[![Ratio létalité - CSI](../doc/csi-ratio_letalite-small.png)](../doc/csi-ratio_letalite-v1.png)

