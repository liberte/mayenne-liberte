# Manifestations

Chaque 2ième samedi du mois est organisée une manifestation, à 14h devant la préfecture de Laval.

Retrouvez toutes les dates [dans l'agenda du collectif](/agenda.md)

## Organisation

Chaque semaine, une équipe tournante se réunit pour organiser les manifestations.

La réunion a lieu **le mercredi soir, de 19h à 21h**.

Pour se joindre au groupe, contacter :

> [Des membres du collectif](/contact.html)
