# <i class="bi-heart"></i> Collectifs amis

### Au niveau national

- Soignants et Citoyens
    - [Réinfo-Covid](https://www.reinfocovid.fr) : Réinformation, par des médecins et personnels de santé ;
    - [Laissons les médecins prescrire](https://stopcovid19.today) : collectif de 2000 médecins ;
    - [Bon Sens](https://bonsens.info) : Préserver le bon sens pour les générations actuelles et futures ;
- Juridique
  - [Réaction 19](https://www.reaction19.fr) : Actions judiciaires ;
  - [ADSPE](https://www.adspe.fr/) : Information et défense de la santé publique et environnementale (modèle de plainte, recours);
  - [Réinfo Liberté](https://reinfoliberte.fr/) : Actions judiciaires pour dénoncer l'escroquerie scientifique Covidiste ;
- Parents :
    - [Parents21 France ](https://parents21.fr) : Collectif de parents, au niveau national ;
    - [Collectif de santé pédiatrique](https://collectifdesantepediatrique.fr) : Réinformation par des pédiatres ;
    - [Enfance et Libertés](https://enfance-libertes.fr) : Action juridique collective : 950 parents déjà inscrits ;
- Enseignants :
    - [Education pour le bien des enfants](https://www.educationpourlebiendesenfants.fr/) : collectif d'enseignants ;
- Étudiants :
    - [EMLU](https://emlu.org/) : Collectif d'Etudiants Militants Libres & Unis (+16 ans), pour faire barrageà l'obligation vaccinale ;
- Solidarités :
    - [Réseau Solidarité Active](https://reseausolidarite.wixsite.com/rsafrance) (RSA) : regroupements d’éveillés sur un rayon de 25 km leur permettant de s'entraider pour faire face a la crise du Coronacircus.
- Alternatives :
    - [ANIMAP](https://animap.fr) : portail professionnel non-discriminatoire, avec 7000 prestataires respectent la dignité humaine, sans discrimination.

### En Mayenne
- [Le Sou Mayennais](https://www.le-sou.org) : Association ayant pour but de remettre l'humain au centre de l'économie.
- les Gilets Jaunes 53
