# Santé

Vous êtes **malade du Covid** (ou l'avez eu) et vous voulez vous soigner ?
Vous avez du **recevoir le "vaccin génétique"** et vous souhaitez en limiter les effets nocifs ?

Cette page est pour vous !

## Votre guide détox

![guide detox](../img/guide_detox.png)

[<i class="bi-file-pdf"></i> Télécharger le guide >>](../doc/2021-09-Detox_vaccin.pdf)

## Notre équipe "santé"


Notre équipe santé, composé d'un coordinateur et de médecins, peuvent vous aider.

Contactez notre coordinateur santé, il répondra à vos questions, où vous mettra en contact avec des médecins :

> Coordinateur équipe santé : **Jean-Claude Pannetier**
> - Email: pannetier-jc (_arobase_) club-internet.fr


## Collectifs nationaux

- [Réinfo-Covid](https://www.reinfocovid.fr) : Réinformation, par des médecins et personnels de santé ;
- [Collectif de santé pédiatrique](https://collectifdesantepediatrique.fr) : Réinformation par des pédiatres ;
- [Laissons les medecins prescrire](https://stopcovid19.today) : collectif de 2000 médecins ;

