
## Rejoindre une thématique

**Les groupes thématiques sont des espaces de réflexion** sur un domaine précis.
Ils ont pour mission d'aider les groupes locaux dans leurs actions,
en leur **proposant des outils concrets** (modèles de documents, procédures, affiches, tracts, etc.).
Ils sont en lien avec les collectifs nationaux, pour valoriser les ressources déjà existantes.

Ces groupes peuvent aussi **proposer des actions communes** à l'échelle du département.

Liste des thématiques :

| Thématique            | Description       |
|-----------------------|-------------------|
| Parents d'élèves      | Vous avez des enfants scolarisés ? Vous avez des questions concernant les vaccins ? [En savoir plus >>](/pages/parents.html)<br/><br/>Groupe Télégram: https://t.me/joinchat/9lrEkXUDHk5kNjI8 
| Juridique             | Vous avez des questions juridiques, ou souhaitez participer à une plainte collective ? [En savoir plus >>](/pages/juridique.html)
| Santé                 | Vous êtes malade de la Covid-19 ? Vous avez reçu un vaccin "génétique" et voulez en limiter les effets négatifs ? [En savoir plus >>](/pages/sante.html)
| Victimes des vaccins  | Vous êtes victime d'effet indésirable, suite à une injection de vaccin Covid-19 ? Un de proche est décédé brusquement suite à une injection ? [En savoir plus >>](/pages/victimes.html)


> Vous voyez une erreur, ou un oubli ?<br/>
> Contactez **Benoit** - 06.62.86.37.82
