# Actualité en vidéo

Courageusement, de plus en plus d'élus, fonctionnaires et scientifiques osent prendre la parole, pour dénoncer les dérives
actuelles, tant sur le plan du droit, que de la santé ou de l'économie.

## Sur la santé

<p>
@@include('../videos/sante.html')
</p>

## Sur les libertés fondamentales
<p>
@@include('../videos/libertes.html')
</p>

## Sur l'économie

<p>
@@include('../videos/economie.html')
</p>
