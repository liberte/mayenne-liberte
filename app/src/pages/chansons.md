# <i class="bi-music-note-list"></i> Chansons & Paroles

Voici les paroles de quelques chansons, utilisées pour animer nos rassemblements : 

- [Danser Encore](/pages/chansons.html#danser-encore) (HK)
- [Le chant des partisans](/pages/chansons.html#le-chant-des-partisans)
- [Le chant des sans-parti](/pages/chansons.html#le-chant-des-sans-parti) (Ingrid Courrèges)
- [Liberté](/pages/chansons.html#liberte) (Charles Trenet)
- [Another Death in the Fall](/pages/chansons.html#another-death-in-the-fall) (parodie Pink Floyd)
- ...à compléter !

> Vous voyez une erreur ou un oubli ?<br/>
> Contacter : **Benoit** - 06.62.86.37.82

## Danser Encore

> Auteur: HK<br/>
> 
> <a type="button" class="btn btn-outline-secondary" target="_blank"
>    href="https://www.youtube.com/watch?v=SyBEMRyt6Qg">
> <i class="bi-youtube"></i> Vidéo (YouTube)
> </a>

Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords
Oh, non non non non non
Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords

Nous sommes des oiseaux de passage
Jamais dociles ni vraiment sages
Nous ne faisons pas allégeance
À l'aube en toutes circonstances
Nous venons briser le silence

Et quand le soir à la télé
Monsieur le bon roi a parlé
Venu annoncer la sentence
Nous faisons preuve d'irrévérence
Mais toujours avec élégance

Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords
Oh, non non non non non
Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords

Auto-métro-boulot-conso
Auto attestation qu'on signe
Absurdité sur ordonnance
Et malheur à celui qui pense
Et malheur à celui qui danse

Chaque mesure autoritaire
Chaque relent sécuritaire
Voit s'envoler notre confiance
Ils font preuve de tant d'insistance
Pour confiner notre conscience

Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords
Oh, non non non non non
Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords

Ne soyons pas impressionnables
Par tous ces gens déraisonnables
Vendeurs de peur en abondance
Angoissants, jusqu'à l'indécence
Sachons les tenir à distance

Pour notre santé mentale
Sociale et environnementale
Nos sourires, notre intelligence
Ne soyons pas sans résistance
Les instruments de leur démence

Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords
Oh, non non non non non
Nous on veut continuer à danser encore
Voir nos pensées enlacer nos corps
Passer nos vies sur une grille d'accords



## Le chant des partisans


<a type="button" class="btn btn-outline-secondary" target="_blank"
href="https://www.youtube.com/watch?v=g3D9M5-4tWg">
<i class="bi-youtube"></i> Vidéo (YouTube)
</a>

Ami, entends-tu le vol noir des corbeaux sur nos plaines ?
Ami, entends-tu les cris sourds du pays qu'on enchaîne ?
Ohé, partisans, ouvriers et paysans, c'est l'alarme.
Ce soir l'ennemi connaîtra le prix du sang et les larmes.

Montez de la mine, descendez des collines, camarades !
Sortez de la paille les fusils, la mitraille, les grenades.
Ohé, les tueurs à la balle et au couteau, tuez vite !
Ohé, saboteur, attention à ton fardeau : dynamite...

C'est nous qui brisons les barreaux des prisons pour nos frères.
La haine à nos trousses et la faim qui nous pousse, la misère.
Il y a des pays où les gens au creux des lits font des rêves.
Ici, nous, vois-tu, nous on marche et nous on tue, nous on crève...

Ici chacun sait ce qu'il veut, ce qu'il fait quand il passe.
Ami, si tu tombes un ami sort de l'ombre à ta place.
Demain du sang noir sèchera au grand soleil sur les routes.
Chantez, compagnons, dans la nuit la Liberté nous écoute...

Ami, entends-tu ces cris sourds du pays qu'on enchaîne ?
Ami, entends-tu le vol noir des corbeaux sur nos plaines ?
Oh oh oh oh oh oh oh oh oh oh oh oh oh oh oh oh...

## Le chant des sans-parti

> Parodie du _Chant des partisans_<br/>
> 
> "Ma version du chant des partisans. En soutien aux convois des libertés partout dans le monde.
> En attendant le clip officiel, un extrait d'un concert privés"
> 
> Paroles: Ingrid Courrèges et Florian Martinez

..

<p class="text-center">
    <a type="button" class="btn btn-primary" target="_blank"
    href="/doc/le_chant_des_sans_parti.mp3">
    <i class="bi-file-music"></i> MP3
    </a>
    <a type="button" class="btn btn-outline-secondary" target="_blank"
        href="https://www.youtube.com/watch?v=N8y0GNLHAZA">
        <i class="bi-youtube"></i> Vidéo (YouTube)
    </a>    
</p>


Ami, entends-tu le convoi de l’espoir qui prend graine
Ami, entends-tu ces poids lourds défiler par centaines
Ohé, présidents, députés et gouvernants c'est l'alarme
Ce soir ennemis connaîtrez le prix du sans plomb qui hargne

Qui tire les ficelles, a pour crainte l’étincelle, populaire
Non le grand reset ne f’ra pas grande recette, sur cette terre
Demain, l’oppression se cachera du grand réveil à Varennes
Un Crépuscule doré résistant au matin brun qui nous saigne

Matez la réprime, renaissez de vos ruines, camarades
Sortez de la brume, écumez le bitume en escouade
Ohé, citoyens par beau temps ou par -20 sur les routes
Soyons le soutien des caravelles du destin dans leur joute

Et nous briserons les barreaux des prisons sanitaires
La haine on repousse quand l’amour éclabousse, sa lumière
Ami, nous suis-tu, nous on roule on prend les rues de l’érable
Enfin unanimes, citoyens et anonymes tous capables

Ami, entends-tu le convoi de l’espoir qui prend graine
Ami entends tu ces pays qui se libèrent de leurs chaînes
Ohé citoyen ! Ohé citoyen
Ami entends tu ces pays qui se libèrent de leurs chaînes


## Liberté

> Auteur: Charles Trenet

<a type="button" class="btn btn-outline-secondary" target="_blank"
   href="https://www.youtube.com/watch?v=UsljXnwaVnQ"> 
  <i class="bi-youtube"></i> Vidéo (YouTube)
</a>

## Another Death in the Fall

> Parodie de _Pink Floyd_<br/>
> Titre alternatif: _We don't need no vaccination_

<a type="button" class="btn btn-outline-secondary" target="_blank"
    href="https://www.youtube.com/watch?v=QcytqsbZLsc">
    <i class="bi-youtube"></i> Vidéo (YouTube)
</a>
