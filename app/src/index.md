# Derniers articles
<div>
@@include('./articles/index-preview.html')
</div>

<p class="text-right text-dark @@showMoreLinkClass">
  <small>
    Voir plus d'articles : <a href="/articles/edito.html">Edito</a>
    | <a href="/articles/events.html">Reportages</a>
    | <a href="/pages/videos.html">Vidéos</a>
    | <a href="/articles/forum.html">Forum</a>
  </small>
</p>

--------------------------------------------------

<div class="invisible-md smaller-title">
@@include('./agenda.html')
</div>

<div class="smaller-title">
    <h1>Comment agir ?</h1>
</div>

Nous vous encourageons à [venir aux manifestations](./agenda.md). Elles se déroulent dans une ambiance familiale et bon enfant,
en toute sécurité avec la coopération de la police. Plusieurs collectifs participent à l'organisation.

Toutes nos actions seront plus fortes, si nous **agissons ensemble**. Alors, ne restez pas seul !

Pour créer des réseaux de soutien et solidarité face à cette crise sans précédent,
nous avons créé [des groupes locaux](/pages/groupes.html) pour chaque communauté de communes du département, et également [des groupes par profession](./agir.html#rejoindre-un-groupe-par-profession).

<p class="text-center">
<a type="button" class="btn btn-primary" href="/groupes"> 
  Trouver un groupe local
</a>

<a type="button" class="btn btn-dark" href="/groupes#rejoindre-un-groupe-par-profession"> 
  Groupes par profession
</a>

<a type="button" class="btn btn-secondary" href="/groupes#rejoindre-une-thématique"> 
  Groupes par thématiques
</a>
</p>

<div class="invisible-md smaller-title">
@@include('./social.html')
</div>

