# Actualités

## Forum

- [<i class="bi-file-earmark-font"></i> Compte-rendu réunion collectif du 15/12](https://forum.mayennelibre.fr/t/compte-rendu-reunion-collectif-du-15-12/123) ;
- [<i class="bi-file-earmark-font"></i> Compte-rendu réunion collectif du 20/10](/articles/forum/2021-10-22-compte-rendu-reunion-collectif-du-20-10-2021-111.html) ;
- [<i class="bi-file-earmark-font"></i> Compte-rendu des infos du 12/10](/articles/forum/2021-10-14-compte-rendu-des-informations-diffusees-rassemblement-du-mardi-12-10-106.html) ;

## Edito

- [<i class="bi-broadcast-pin"></i> Évaluation des modélisations Covid-19](/articles/edito/2022-02-14-evalusation-modelisation-covid.html) par 2 polytechniciens (14/02/2022)
- [<i class="bi-broadcast-pin"></i> Radio Fidelité Mayenne](/articles/edito/2022-01-10-radio-fidelite-mayenne-interview-benoit-lavenier.html) : interview sur les propos honteux du président (10/01/2022)
- [<i class="bi-eyedropper"></i> Guide détox vaccins antiCovid](/pages/sante.html), réalisé par "Pure Santé"
- [<i class="bi-shield-exclamation"></i> Pharmacovigilance des vaccins Covid-19](/pharmacovigilance) (maj 10/09/2021);
- [<i class="bi-shield-check"></i> Guide juridique complet](/pages/juridique.html) (v1.3)

## Reportages

2022 :
- [<i class="bi-image"></i> En direct du Convoi de la Liberté](/articles/events/2022-02-08-reportage_convoi_de_la_liberte.html) ;
- [<i class="bi-image"></i> Rassemblement du 12/02](/articles/events/2022-02-12-manifestation_prefecture.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 08/01](/articles/events/2022-01-08-manifestation_prefecture.html) à Laval ;

2021 :
- [<i class="bi-image"></i> Rassemblement du 11/12](/articles/events/2021-12-11-manifestation_prefecture.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 06/11](/articles/events/2021-11-06-manifestation_prefecture.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 12/10](/articles/events/2021-10-12-manifestation_bougies.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 02/10](/articles/events/2021-10-02-manifestation_media.html) à Laval ;
- [<i class="bi-image"></i> Chaine humaine du 25/09](/articles/events/2021-09-25-chaine_humaine.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 18/09](/articles/events/2021-09-18-manifestation_hopital.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement à l'hôpital du 11/09](/articles/events/2021-09-11-manifestation_hopital.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 28/08/2021](/articles/events/2021-08-28-prefecture.html) à Laval ;
- [<i class="bi-image"></i> Guinguette du 21/08](/articles/events/2021-08-21-guinguette.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 14/08](/articles/events/2021-08-14-manifestion_prefecture.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 07/08](/articles/events/2021-08-07-manifestion_prefecture.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 31/07](/articles/events/2021-07-31-manifestion_prefecture.html) à Laval ;
- [<i class="bi-image"></i> Rassemblement du 24/07](/articles/events/2021-07-24-manifestion_defenses_des_libertes.html) à Laval.
- [<i class="bi-image"></i> Constitution le 17/07](/articles/events/2021-07-17-constitution_gue_de_selle.html) à Laval.
