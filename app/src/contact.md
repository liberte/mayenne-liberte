# Contact

- Coordonnées de quelques membres actifs : 
  - Maryline (organisation) - [06.03.08.22.23](tel:0603082223)
  - Christophe Fiamma (juridique) - [06.71.23.07.21](tel:0671230721) - Facebook: [Christophe le Caribouman](https://www.facebook.com/christophe.lecaribouman)
  - Jean-Claude Pannetier (santé) - pannetier-jc (_arobase_) club-internet.fr
  - Benoit Lavenier (site web) - benoit.lavenier (_arobase_) gmail.com - [06.62.86.37.82](tel:0662863782)
  - Hélène Béquin (parents) - [06.79.42.34.99](tel:0679423499)


<div class="smaller-title">
@@include('./social.html')
</div>
