
# Licences du site
## Contenus

L'intégralité de ce site est sous licence CC-BY-SA sauf mention contraire explicite (cf paragraphe suivant).

<div>
@@include('./credits.html')
</div>
