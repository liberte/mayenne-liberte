# Qui sommes-nous ?

<a class="invisible-md logo" style="float: right;"
    href="/img/logo-800px.png"
    data-toggle="lightbox" data-gallery="qui-sommes-nous">    
</a>

Mayenne Liberté est un **collectif citoyen**, apolitique, non-violent.

Nous affirmons le droit à la liberté de conscience et au respect du corps.

Nous demandons la **sortie de l'état d'urgence sanitaire**, et le rétablissement
des libertés individuelles constitutionnelles. Nous **refusons le passe** (sanitaire comme vaccinal), comme de toute autre forme
de totalitarisme sanitaire et numérique.

Nous demandons le libre accès aux traitements existants efficaces contre le SARS-COV-2 (curatifs ou préventifs),
plutôt que la généralisation des pseudo-vaccins expérimentaux.

> Le collectif est issu de la convergence de ses deux "grands frères" :
> - Mayenne Libre ([créé le 1er avril 2021](./articles/edito/2021-04-01-lettre_ouverte_aux_elus.html))
> - Mayenne défenses des Libertés ([créé le 17 juillet 2021](./events/2021-07-17-constitution_gue_de_selle.html)).

![image collectif Ouest-France](/gallery/2021-07-17/ouest-france.jpeg)
_1ère réunion (constitutive) du collectif, au Gué-de-Selle, le 17 juillet 2021 (Photo [Ouest-France](https://www.ouest-france.fr/pays-de-la-loire/laval-53000/en-mayenne-un-collectif-cree-pour-la-defense-de-nos-libertes-notre-libre-arbitre-b58e20d0-e7dc-11eb-91f7-b0a903f6bddd))_


## Mode d'action

Toutes nos actions sont **non-violentes**, respectueuses de chacun,
mais cependant **déterminées**.

Nous agissons au nom du bon sens, de la prudence et de la protection des plus fragiles 
et en particulier : des enfants, des ainés, des malades.

Nous usons de tous les moyens légaux pour faire entendre nos voix.
